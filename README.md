# Dogeit

Very long, very badly formatted ramblings about what happened during this project & reflections on the project.

## Original Idea

The original project idea was to create a Twitter / Reddit clone
with the twist of having the length of posts and comments being
limited to the price of Dogecoin.

It was originally meant to help learn how to create a REST API using Node.js,
deployment on Vercel, authentication and authorization.

Initially the project was meant to be simple:

- RestAPI with in Next.js functions
  - keep front end and back end code in same place using same language
  - can easily deploy entire application on Vercel
  - already learned Express, and with next-connect, could make Next.js
    functions look like Express
- PostgreSQL as database
  - Originally wanted to use MongoDB with a free small MongoDB Atlas server.
    However, I realized that the data was highly relational and it made more
    logical sense to use an RDB.
  - Could use Supabase for free small PostgreSQL instance. It comes with a
    pooler, which is supposed to help manage connections coming from serverless
    functions.
- Front end in Next.js
  - After learning basic React, I liked how modular you could make everything.
  - Also, with Next.js it was easy to include statically generated pages,
    statically generated pages with client-side hydration, and dynamiclly generated
    pages using React.
- Deployment on Vercel
  - Easy to deploy by just pushing or merging onto main branch on Github/Gitlab/Bitbucket
  - Comes with CDN and caching, which I though would help minimize calls to the backend

## During the project

During the project, I realized that I'd want to test all these components - mostly the database -
locally instead of, for example, always calling on Supabase's Postgres instance. Therefore, I decided
early on I was going to use Docker containers to develop.

I also wanted something that allowed me to easily create and destroy development environments. Therefore,
I decided to use docker containers as my development environment with VSCode's remote development using Docker.
Therefore, I decided to learn about the basics of Docker and how to manage multiple docker containers using
Docker compose.

## Issues Encountered

However, during the project, I kept on encountering permissions issues with my Docker development environment.
Because I was developing on Linux, there was no VM running Linux running Docker like on MacOS and Windows.
Also, since I was bind mounting the project for easier development and was not the root user when developing,
I was trying to modify another user's files as root, when I was inside the container. Because I didn't know about
running containers as a non root user, I decided to just change all the ownership of the files to root when developing.

For Next.js, there is a convient library called Next-Auth
that provides easy authentication and authorization. The library uses JWT tokens or Cookie based sessions
to persist users; however I was concerned about revoking JWTs and CSRF protections with both these methods.

Initially I ended up using sessions and implementing my own CSRF protection, by trying to mimic the code in the express
CSRF protection code. Eventually, I decided to use a method similar to the one mentioned in a Hasura blog, and Ben Awad,
to use a short lived JWT in memory and a longer lived refresh token in cookies.

However, because I had not written any automated tests,
and only relied on manual testing with Insomnia client it was really difficult
to determine if a change in the code actaully did what I intended it to do. The lack of automated testing would eventually
be the reason I abandoned the project.

Another issue I encountered with the project was trying to make it feel more express-like. I used a library
called next-connect to make the functions feel like express Request/Response functions. However, in express
you can group the routes using routers and put common middleware across the entire app, an entire router, or
just a single function. Since I wanted to minimize the amount of times I had including the authorization middleware,
I decided to group multiple sets of routes into a "router".

However, what actaully happened was that Next.js saw that as only 1 large function. When testing a deployment on Vercel,
this made it so a simple API call to a small route would take up a large amount of memory because it was loading in
the code for all the other routes I grouped together. When profiling the API using the Next.js profiler,
it showed that it thought all the routes I grouped together were just 1 large function.
Therefore, I had to break up the functions into individual function routes.

Another issue I enxounterd with this project was BigInts in PostgreSQL. I decided to use the Prisma2 ORM to
query my database. I initialized the database using raw SQL, because I felt like it was easier to initialize the database
with raw SQL and I could make migrations by dropping the database and updating the initial SQL being loaded.
Also, it allowed me to create custom functions and triggers, when inserting or updating, which I was vaguely familiar with
from my databases course in college. Because this would not be possible in Prisma, I decided to use a custom
function based off Instagram's medium post on how they generate unique ids. This would allow me to display the user id
in the url (when it was base32 encoded) and not have the ids easily guessable. However, this turned out to be
a big mistake because Prisma could not handle big ints from Postgres.

Eventually I decided that, instead of using another ORM, switching to Django, using a UUID, or accepting that it was ok to
just a regular auto incrementing int id, I would write custom PLSQL functions
to return the big int as a string for every query on a table with a big int id.

Another challenge I encountered was in how to display comments. Because a comment tree was a tree data structure,
I looked into how to store a tree in PostgreSQL. I come up with just storing the root and parent or using the ltree extension.
However, because I was concerned about performance when updating a comment tree, storage space, and
I kept changing between the two methods. However, since PrismaORM did not support ltree, I ended up jsut storing the root and
the parent.

Another challenge I faced was with pagination. I learned that offest pagination was bad for performance
and since I was building for this to scale to the moon, I had to use the more complex cursor pagination on my
id (which increments by datetime and sequentially according to the Instagram medium post). However, since I wanted the user
to be able to sort with all the features of Reddit, I needed to create complex rules on inputs. Again, without automated testing,
performing the manual tests was extremelty slow.

After writing about a dozen functions, after refactoring the routes, and very slow manual testing on the ballooning roots,
I decided that this project was not really worth continuing.

## Lessons Learned

### Engineering / Project Management

- Automated testing will save a lot of time
  - when changing code that affects a lot of other code, unit and integration testing will help in the long term
  - (later in another project using express, realized that everything should be written so that it can be easily testable)
  - testing should not be a stretch goal (as mentioned in the unreleased project plan on Confluence)
- Planning is key
  - prevents scope creep (e.g. making one thing and then deciding that I need to add more instead of keeping it small
  and simple)
  - know how many users / audience that will look at project
  - having an OpenAPI doc helps
    - helps know what routes needed in advance and what inputs and outputs are expected
    - helps know what to test for
- Don't use the wrong technology - use what is best at the moment that I know
  - PrismaORM + non standard SQL types in Postgres was not a fun combination. The rust code that converts the data from
  postgres into js types does not support many things.
  - Next js functions, still limited if writing a traditional very serverful application (like wanting CSRF protection)
    - would probably work better if auth was using external auth server with JWT, bc hard to not write lots of repeating boilerplate code
    when each function is a seperate process

### Techinical

- Basics of Docker
  - the layered architecure (allows for caching of each command & reusing layers to reduce actual storage)
  - how to mount volumes, place containers on networks
  - how to expose ports on the docker network and to forward the port to the host
  - how docker is run on macos and windows
  - how to use docker compose to manage multiple containers on the same machine
- CSRF protection
  - why its needed on only POST/PUT/PATCH/DELETE (because its changing the state on the server and different domains cannot read
  another domain's contant inside an iframe)
  - how browsers can protect and mitigate CSRF attacks with Lax cookies. These are sent only when navagint to the website from another website
  or are navaging within the website (only on newer browsers)
