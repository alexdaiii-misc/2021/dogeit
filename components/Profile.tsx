import React from "react";
import useSWR from "swr";
import fetcher from "../lib/client/fetcher";
import ProfilePic from "./ProfilePic";

export default function Profile() {
  const { data, error } = useSWR("/api/me", fetcher);

  if (error) return <div>failed to load</div>;
  if (!data) return <div>loading...</div>;

  // render data
  return (
    <>
      Signed In <br />
      <div>Username: {data.username}</div>
      <div>Name: {data.name}</div>
      <div>Email: {data.email}</div>
      <ProfilePic image={data.image} /> <br />
    </>
  );
}
