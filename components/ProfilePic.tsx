import React from "react";
import Image from "next/image";
import utilStyles from "../styles/utils.module.css";

interface MyProps {
  image: string;
}

export default function ProfilePic({ image }: MyProps) {
  return (
    <Image
      src={image}
      height={144}
      width={144}
      alt="A profile picture of some guy"
      className={utilStyles.borderCircle}
    ></Image>
  );
}
