import axios from "axios";
import { useFormik } from "formik";
import React from "react";
import { cleanDefaultValues, getCookie } from "../lib/client/utils";

export default function UpdateUser() {
  const options = {
    headers: { "X-XSRF-TOKEN": getCookie("__Host-XSRF-TOKEN") },
  };

  const handleSubmit = async (values: typeof formik.initialValues) => {
    try {
      const cleanedVals = cleanDefaultValues(values);
      const res = await axios.patch("/api/me", cleanedVals, options);
      // TODO: Remove console log response
      console.log(res);
    } catch (error) {
      // TODO: Remove console logging error
      console.log(error.response);
    }
  };

  const formik = useFormik({
    initialValues: {
      username: "",
      name: "",
      email: "",
    },
    onSubmit: async (values) => {
      await handleSubmit(values);
    },
  });

  return (
    <>
      <h2>Update profile</h2>
      <form onSubmit={formik.handleSubmit}>
        <label htmlFor="username">Username</label>

        <input
          id="username"
          name="username"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.username}
        />

        <br />

        <label htmlFor="name">name</label>

        <input
          id="name"
          name="name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.name}
        />

        <br />

        <label htmlFor="email">Email Address</label>

        <input
          id="email"
          name="email"
          type="email"
          onChange={formik.handleChange}
          value={formik.values.email}
        />

        <button type="submit">Submit</button>
      </form>
    </>
  );
}
