import { DogeitError } from "./../errors";
import { ids } from "../../types/attachIdType";
import { NextApiRequestWithParams } from "../../types/requestTypes";

export abstract class AttachId {
  protected errorMsg: DogeitError;
  protected id: ids;

  constructor(req: NextApiRequestWithParams) {
    const id = this.sourceId(req);
    this.errorMsg = this.sourceErrorMsg();

    if (!id) {
      throw this.errorMsg;
    }

    this.id = id;
  }

  /**
   * Gets the error message to throw when sourceId cannot be found
   */
  protected abstract sourceErrorMsg(): DogeitError;

  /**
   * Gets the identifier to get the data source. Returns either the id, name, etc. - something that can uniquely identify the resource in the database.
   */
  protected abstract sourceId(
    req: NextApiRequestWithParams
  ): ids | null | undefined;

  /**
   * @returns the id
   */
  public getId(): ids {
    return this.id;
  }
}
