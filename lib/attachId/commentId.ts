import { commentIdSlug } from "../server-const/routeNames";
import { StatusCodes } from "http-status-codes";
import { DogeitError } from "../errors";
import { EncodedAttachId } from "./encodedAttachId";

/**
 * Base class for getting postId identifier
 */
export class CommentIdFromURI extends EncodedAttachId {
  protected returnIdSlug(): string {
    return commentIdSlug;
  }

  protected sourceErrorMsg(): DogeitError {
    return new DogeitError({
      msg: `Comment ${this.id} cannot be found`,
      statusCode: StatusCodes.NOT_FOUND,
    });
  }
}
