import { historicIdSlug } from "../server-const/routeNames";
import { StatusCodes } from "http-status-codes";
import { NextApiRequestWithParams } from "../../types/requestTypes";
import { DogeitError } from "../errors";
import { AttachId } from "./attachId";
import { stringToBigint } from "../encode36";
import { ENCODING_RADIX } from "../encodeConsts";

/**
 * Base class for getting cryptoId identifier
 */
export class HistoricCryptoId extends AttachId {
  protected sourceId(req: NextApiRequestWithParams) {
    const historicId: string | undefined = req.params[historicIdSlug];

    // historicId is encoded - decode it
    if (historicId) {
      return stringToBigint(historicId, ENCODING_RADIX);
    }

    return historicId as undefined;
  }

  protected sourceErrorMsg(): DogeitError {
    return new DogeitError({
      msg: `Historic price ${this.id} cannot be found`,
      statusCode: StatusCodes.NOT_FOUND,
    });
  }

  public getId() {
    return this.id as bigint;
  }
}
