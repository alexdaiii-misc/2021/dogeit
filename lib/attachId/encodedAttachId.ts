import { NextApiRequestWithParams } from "../../types/requestTypes";
import { validateInputString } from "../encode36";
import { AttachId } from "./attachId";

export abstract class EncodedAttachId extends AttachId {
  protected sourceId(req: NextApiRequestWithParams) {
    const encodedId: string | undefined = req.params[this.returnIdSlug()];

    // postId is encoded - decode it
    if (encodedId) {
      return validateInputString(encodedId);
    }

    return encodedId as undefined;
  }

  protected abstract returnIdSlug(): string;

  public getId() {
    return this.id as bigint;
  }
}
