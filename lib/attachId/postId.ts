import { StatusCodes } from "http-status-codes";
import { DogeitError } from "../errors";
import { postIdSlug } from "../server-const/routeNames";
import { EncodedAttachId } from "./encodedAttachId";

/**
 * Base class for getting postId identifier
 */
export class PostIdFromURI extends EncodedAttachId {
  protected returnIdSlug(): string {
    return postIdSlug;
  }

  protected sourceErrorMsg(): DogeitError {
    return new DogeitError({
      msg: `Post ${this.id} cannot be found`,
      statusCode: StatusCodes.NOT_FOUND,
    });
  }
}
