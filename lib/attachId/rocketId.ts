import { StatusCodes } from "http-status-codes";
import { NextApiRequestWithParams } from "../../types/requestTypes";
import { DogeitError } from "../errors";
import { rocketIdSlug } from "../server-const/routeNames";
import { AttachId } from "./attachId";

/**
 * Base class for getting rocket identifier
 */
abstract class Rocket extends AttachId {
  protected sourceErrorMsg(): DogeitError {
    return new DogeitError({
      msg: `Rocketship ${this.id} cannot be found`,
      statusCode: StatusCodes.NOT_FOUND,
    });
  }
}

abstract class RocketName extends Rocket {
  /**
   * @returns the name of the rocket in the url param
   */
  public getId(): string {
    return this.id as string;
  }
}

/**
 * Sources the RocketName from the URI
 */
export class RocketNameFromURI extends RocketName {
  protected sourceId(req: NextApiRequestWithParams) {
    const rocketName: string | undefined = req.params[rocketIdSlug];
    return rocketName;
  }
}
