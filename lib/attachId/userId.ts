import { StatusCodes } from "http-status-codes";
import { NextApiRequestWithParams } from "../../types/requestTypes";
import { DogeitError } from "../errors";
import { usernameSlug } from "../server-const/routeNames";
import { AttachId } from "./attachId";

/**
 * Sources the Username from the URI
 */
export class UsernameFromURI extends AttachId {
  /**
   * @returns the username in the url param
   */
  public getId(): string {
    return this.id as string;
  }

  protected sourceErrorMsg(): DogeitError {
    return new DogeitError({
      msg: `Username ${this.id} cannot be found`,
      statusCode: StatusCodes.NOT_FOUND,
    });
  }

  protected sourceId(req: NextApiRequestWithParams) {
    const username: string | undefined = req.params[usernameSlug];
    return username;
  }
}
