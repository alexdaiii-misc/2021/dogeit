/**
 * Fetch handler for swr
 * @param input argument needed by fetch
 * @param init argument needed by fetch
 * @param args Any optional arguments
 * @returns response from the server
 */
const fetcher = async (
  input: RequestInfo,
  init: RequestInit,
  ..._args: any[]
) => {
  const res = await fetch(input, init);
  return res.json();
};

export default fetcher;
