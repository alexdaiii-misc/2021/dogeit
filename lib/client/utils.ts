// For misc utility functions that are too small to get their own file

/**
 * Clean out default values from a Formik object. Use with formik to clean out the default values.
 * All fields are assumed to have the same default value
 * @param obj Formik value object
 * @returns Cleaned object without any default values
 */
export function cleanDefaultValues(obj: any, defaultVal: any = "") {
  let newObj: any = {};

  for (var propName in obj) {
    if (obj[propName] !== defaultVal) {
      newObj[propName] = obj[propName];
    }
  }
  return newObj;
}

/**
 * Gets the cookie from the html document
 * @param name name of the cookie to get
 * @returns The cookie value
 */
export function getCookie(name: string) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);

  if (parts.length === 2) {
    return parts.pop()?.split(";").shift();
  }
}
