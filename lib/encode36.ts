import { StatusCodes } from "http-status-codes";
import { DogeitError } from "./errors";
import {
  ENCODING_RADIX,
  MAX_LENGTH_FOR_2_64_INBASE_36,
  MAX_POSTGRES_BIGINT,
  OUR_EPOCH,
  SHARD_ID,
} from "./encodeConsts";

/**
 * Converts a given string value to bigint with given radix
 * @param value value to convert to bigint
 * @param radix the base to convert it from
 * @returns a big int in the given radix
 */

export function stringToBigint(value: string, radix: number) {
  try {
    return [...value.toString()].reduce(
      (r, v) => r * BigInt(radix) + BigInt(parseInt(v, radix)),
      0n
    );
  } catch (error) {
    if (error instanceof RangeError) {
      throw new DogeitError({
        msg: `Value ${value} is an invalid base ${radix} encoded id`,
        statusCode: StatusCodes.BAD_REQUEST,
      });
    }
    throw error;
  }
}
/**
 * Encodes in base 36 an entire arrat
 * @param data the array to encode
 * @param id_params the params on data to encode
 */

export function encode36Array(
  data: any[],
  id_params: string[],
  radix: number = ENCODING_RADIX
) {
  data.forEach((obj) => {
    encode36(obj, id_params, radix);
  });
}
/**
 * Compliment to stringToBigint, converts bigints into strings. Performs conversion IN PLACE.
 * @param data data to encode
 * @param id_params the params on data to encode
 */
export function encode36(
  data: any,
  id_params: string[],
  radix: number = ENCODING_RADIX
) {
  id_params.forEach((param) => {
    if (data[param]) {
      data[param] = data[param].toString(radix);
    }
  });
}

/**
 * Validates the input base 36 encoded string is not too long or too large
 * @returns a validated string, decoded to a bigint
 */

export function validateInputString(input: string) {
  const invalidIdError = {
    msg: `Value ${input} is an invalid base ${ENCODING_RADIX} encoded id`,
    statusCode: StatusCodes.BAD_REQUEST,
  };

  if (input.length > MAX_LENGTH_FOR_2_64_INBASE_36) {
    throw new DogeitError(invalidIdError);
  }

  const id = stringToBigint(input, ENCODING_RADIX);

  if (id > MAX_POSTGRES_BIGINT) {
    throw new DogeitError(invalidIdError);
  }

  return id;
}
/**
 * Generates the latest possible id for that millisecond
 */

export function generateLatestId() {
  const time = BigInt(Date.now());
  const top54 = ((time - OUR_EPOCH) << BigInt(23)) | (SHARD_ID << BigInt(10));
  const seqId = BigInt(1023); // each ms, there can be 1024 ids generated, 0 indexed
  return top54 | seqId;
}

export function stringArrToBigInts(input: string[]) {
  const output = input.map((value) => {
    return validateInputString(value);
  });

  return output;
}
