// db constants
// January 1st, 2011

export const OUR_EPOCH = BigInt(1314220021721);
export const SHARD_ID = BigInt(1);
const MS_IN_SECONDS = BigInt(1000 * 60);

export const REVALIDATE_TIME =
  MS_IN_SECONDS * BigInt(Number(process.env.MINUTES_TO_REVALIDATE ?? 1000));

export const ENCODING_RADIX = 36;
export const MAX_POSTGRES_BIGINT = BigInt(9223372036854775807n);
export const MAX_LENGTH_FOR_2_64_INBASE_36 = 13;

// params to base 36 encode
export const POST_PARAMS_TO_ENCODE = ["id", "crypto_prices_id", "user_id"];
export const USER_PARAMS_TO_ENCODE = ["public_id"];
export const COMMENT_PARAMS_TO_ENCODE = [
  "id",
  "post_id",
  "parent_id",
  "crypto_prices_id",
  "root_id",
  "user_id",
];
export const REACTIONS_PARAMS_TO_ENCODE = ["post_id", "comment_id", "user_id"];
