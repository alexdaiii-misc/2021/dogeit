import { StatusCodes } from "http-status-codes";
import { NextApiResponse } from "next";

/**
 * Creates a DogeitError object
 */
export class DogeitError {
  msg: string;
  statusCode: number;
  blob: any;

  constructor(error: { msg: string; statusCode: number; blob?: any }) {
    this.msg = error.msg;
    this.statusCode = error.statusCode;
    this.blob = error.blob;
  }
}

/**
 * Default error handler
 * @param error the error object
 * @param res response
 * @param options Message to override default messages
 * @returns
 */
export function errorHandler(
  error: any,
  res: NextApiResponse,
  options?: { prisma?: { msg?: string; statusCode?: number; meta?: any } }
) {
  console.error(error);

  // Errors from me
  if (error instanceof DogeitError) {
    return res.status(error.statusCode).json({ error });
  }

  // Prisma errors
  const UNIQUE_CONSTRAINT_FAILURE = "P2002";
  if (error.code === UNIQUE_CONSTRAINT_FAILURE) {
    return res
      .status(options?.prisma?.statusCode ?? StatusCodes.CONFLICT)
      .json({
        error: {
          msg:
            options?.prisma?.msg ??
            `Target already exists. Meta: ${error.meta}`,
          blob: options?.prisma?.meta,
          statusCode: options?.prisma?.statusCode ?? StatusCodes.CONFLICT,
        },
      });
  }

  const RECORD_DOESNT_EXIST = "P2025";
  if (error.code === RECORD_DOESNT_EXIST) {
    return res
      .status(options?.prisma?.statusCode ?? StatusCodes.NOT_FOUND)
      .json({
        error: {
          msg: options?.prisma?.msg ?? "Record does not exist",
          blob: options?.prisma?.meta,
          statusCode: options?.prisma?.statusCode ?? StatusCodes.NOT_FOUND,
        },
      });
  }

  // default - some unknown or known internal server error
  return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
    error: { msg: "Internal Server Error" },
  });
}
