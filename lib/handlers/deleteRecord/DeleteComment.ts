import { returnedCommentDetails } from "./../../queries/qryReturnVals";
import { CommentIdFromURI } from "./../../attachId/commentId";
import { COMMENT_PARAMS_TO_ENCODE } from "../../encodeConsts";
import prisma from "../../prisma";
import dbErrors from "../../queries/dbError/dbUserThrownErrors";
import { DeleteRecord } from "./DeleteRecord";
import {
  commentSelected,
  prismaSafeComment,
} from "../../../types/qryTypes/commentTypes";

export class DeleteComment extends DeleteRecord {
  protected record: commentSelected | prismaSafeComment | undefined = undefined;
  protected prismaSafeParamsToEncode = [
    "comment_id_astring",
    "post_id",
    "user_id",
    "crypto_prices_id",
    "parent_id",
    "root_id",
  ];
  protected paramsToEncode36 = COMMENT_PARAMS_TO_ENCODE;

  /**
   * Returns the post Id from the uri as a bigint
   */
  protected getRecordId() {
    return new CommentIdFromURI(this.req).getId();
  }

  /**
   * Returns the post record
   */
  public getRecord() {
    if (!this.record) {
      throw new Error("Must call run() before getting record.");
    }
    return this.record;
  }

  /**
   * Admin delete post from database
   */
  protected async adminDeleteRecord() {
    try {
      // must use queryraw since for prisma does not like this
      // admin_delete_comment(post_id BIGINT)
      const deleted = (await prisma.$queryRaw(
        `SELECT * FROM admin_delete_comment(${(this.id as bigint).toString()})`
      )) as prismaSafeComment[];

      if (deleted.length !== 1) {
        throw new Error("Could not delete comment");
      }

      this.record = deleted[0];

      this.encodePrismaSafeRecordFromDb();
    } catch (error) {
      throw await dbErrors(error);
    }
  }

  /**
   * Nulls the post from the database as an admin
   */
  protected async adminNullRecord() {
    this.record = await prisma.comments.update({
      select: { ...returnedCommentDetails, user_id: true },
      where: { id: this.id },
      data: { user_id: null, body: "" },
    });
  }

  /**
   * Nulls the record from the database as a post owner or moderator
   */
  protected async userNullRecord() {
    try {
      // FUNCTION nullify_comment(post_id BIGINT, usr_pid BIGINT)
      const nulled = (await prisma.$queryRaw(
        `SELECT * FROM nullify_comment(${this.id.toString()}, ${this.req.user.public_id.toString()})`
      )) as prismaSafeComment[];

      if (nulled.length !== 1) {
        throw new Error("Could not delete comment");
      }

      this.record = nulled[0];
    } catch (error) {
      throw await dbErrors(error);
    }

    this.encodePrismaSafeRecordFromDb();
  }

  /**
   * converts prisma safe post into regular selected post
   */
  protected convertToStandardParams() {
    const commentId = (this.record as prismaSafeComment).comment_id_astring;

    if (!commentId) {
      return;
    }

    delete (this.record as prismaSafeComment).comment_id_astring;

    this.record!.id = commentId;
  }
}
