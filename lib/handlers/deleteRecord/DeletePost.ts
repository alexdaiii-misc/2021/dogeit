import {
  postSelected,
  prismaSafePost,
} from "../../../types/qryTypes/postTypes";
import { PostIdFromURI } from "../../attachId/postId";
import { POST_PARAMS_TO_ENCODE } from "../../encodeConsts";
import prisma from "../../prisma";
import dbErrors from "../../queries/dbError/dbUserThrownErrors";
import { returnedPostDetails } from "../../queries/qryReturnVals";
import { DeleteRecord } from "./DeleteRecord";

export class DeletePost extends DeleteRecord {
  protected record: postSelected | prismaSafePost | undefined = undefined;
  protected prismaSafeParamsToEncode = [
    "post_id_astring",
    "crypto_prices_id",
    "user_id",
  ];
  protected paramsToEncode36 = POST_PARAMS_TO_ENCODE;

  /**
   * Returns the post Id from the uri as a bigint
   */
  protected getRecordId() {
    return new PostIdFromURI(this.req).getId();
  }

  /**
   * Returns the post record
   */
  public getRecord() {
    if (!this.record) {
      throw new Error("Must call run() before getting record.");
    }
    return this.record;
  }

  /**
   * Admin delete post from database
   */
  protected async adminDeleteRecord() {
    this.record = await prisma.posts.delete({
      select: returnedPostDetails,
      where: { id: this.id },
    });
  }

  /**
   * Nulls the post from the database as an admin
   */
  protected async adminNullRecord() {
    this.record = await prisma.posts.update({
      select: returnedPostDetails,
      where: { id: this.id },
      data: { user_id: null, title: "", body: "" },
    });
  }

  /**
   * Nulls the record from the database as a post owner or moderator
   */
  protected async userNullRecord() {
    try {
      // FUNCTION nullify_post(post_id BIGINT, usr_pid BIGINT)
      const nulled = (await prisma.$queryRaw(
        `SELECT * FROM nullify_post(${this.id.toString()}, ${this.req.user.public_id.toString()})`
      )) as prismaSafePost[];

      if (nulled.length !== 1) {
        throw new Error("Could not delete post");
      }

      this.record = nulled[0];
    } catch (error) {
      throw await dbErrors(error);
    }

    this.encodePrismaSafeRecordFromDb();
  }

  /**
   * converts prisma safe post into regular selected post
   */
  protected convertToStandardParams() {
    const postId = (this.record as prismaSafePost).post_id_astring;

    if (!postId) {
      return;
    }

    delete (this.record as prismaSafePost).post_id_astring;

    this.record!.id = postId;
  }
}
