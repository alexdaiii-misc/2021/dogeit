import { DogeitError } from "./../../errors";
import { RequestWithUser } from "../../../types/requestTypes";
import { postDeletionActions } from "../../../types/schemas/postSchemas";
import { StatusCodes } from "http-status-codes";
import { ENCODING_RADIX } from "../../encodeConsts";
import { encode36 } from "../../encode36";

export abstract class DeleteRecord {
  protected req: RequestWithUser;
  protected action: postDeletionActions;
  protected record: any;
  protected id: any;
  protected prismaSafeParamsToEncode: any[] = [];
  protected paramsToEncode36: string[] = [];

  constructor(req: RequestWithUser, action: postDeletionActions) {
    // check to make sure only admins can delete
    if (action === "delete" && !req.user.site_admin) {
      throw new DogeitError({
        msg: "Only site admins can perform this action.",
        statusCode: StatusCodes.FORBIDDEN,
      });
    }

    this.req = req;
    this.action = action;
    this.id = this.getRecordId();
  }

  /**
   * Performs the action to delete from the database
   */
  public async run() {
    switch (this.action) {
      case "delete":
        await this.adminDeleteRecord();

        encode36(this.record, this.paramsToEncode36);
        break;

      case "mark":
        if (this.req.user.site_admin) {
          await this.adminNullRecord();

          encode36(this.record, this.paramsToEncode36);
        } else {
          await this.userNullRecord();
        }
        break;

      default:
        throw new Error(
          "Invalid argument: action can only be 'delete' or 'mark'"
        );
    }
  }

  /**
   * Gets the recordId
   */
  protected abstract getRecordId(): any;

  /**
   * Function to call to return the record
   */
  public abstract getRecord(): any;

  /**
   * Deletes the record from database
   */
  protected abstract adminDeleteRecord(): any;

  /**
   * Nulls the record from the database as an admin
   */
  protected abstract adminNullRecord(): any;

  /**
   * Nulls the record as a user - owner of record or moderator
   */
  protected abstract userNullRecord(): any;

  /**
   * Function will take in a Prisma Safe record returned from db and
   * encode all the params to be the correct format
   */
  protected encodePrismaSafeRecordFromDb() {
    this.prismaSafeParamsToEncode.forEach((param) => {
      const value = this.record[param];

      try {
        if (value) {
          this.record[param] = BigInt(value).toString(ENCODING_RADIX);
        }
      } catch (error) {
        throw new Error(`Cannot encode value: ${value}. Not a proper number`);
      }
    });

    this.convertToStandardParams();
  }

  /**
   * Function will convert the prismasafe params to standard params
   */
  protected abstract convertToStandardParams(): any;
}
