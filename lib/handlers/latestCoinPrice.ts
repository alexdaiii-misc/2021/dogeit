// npm packages
import axios from "axios";
import { StatusCodes } from "http-status-codes";

// ts
import { geckoResponse } from "../../types/geckoResponseData";
import { crypto_prices } from "@prisma/client";

// db
import { addLatestPrice, getLatestPrice } from "../queries/cryptoQueries";

// lib
import { DogeitError } from "../errors";
import {
  ENCODING_RADIX,
  OUR_EPOCH,
  REVALIDATE_TIME,
  SHARD_ID,
} from "../encodeConsts";

const SUPPORTED_COINS = [
  "dogecoin",
  "shiba-inu",
  "banano",
  "bitcoin",
  "ethereum",
];

type coinInfo =
  | crypto_prices
  | (Omit<crypto_prices, "id" | "total_volume"> & {
      id: string;
      total_volume: string;
    });

/**
 * Returns the latest coin price from the database or gecko
 * @param coin coin to get the price of
 * @param encode encode the bigints into strings or not, default true - will convert
 */
export default async function latestCoinPrice(
  coin: string,
  encode: boolean = true
) {
  // check if its a supported coin
  if (!SUPPORTED_COINS.includes(coin.toLocaleLowerCase())) {
    throw new DogeitError({
      msg: `Could not find coin with the given id ${coin}`,
      statusCode: StatusCodes.NOT_FOUND,
    });
  }

  // query database for the latest price within the revalidate time
  const time = BigInt(Date.now()) - REVALIDATE_TIME;
  const result = ((time - OUR_EPOCH) << BigInt(23)) | (SHARD_ID << BigInt(10));

  // call database
  let coinInfo: coinInfo | null = (await getLatestPrice(coin, result))[0];

  if (!coinInfo) {
    // the information is stale or not in database, add to db
    coinInfo = await updateCoinPrice(coin);
  }

  // conv bigints into strings
  if (encode) {
    coinInfo.id = coinInfo.id.toString(ENCODING_RADIX);
    coinInfo.total_volume = coinInfo.total_volume.toString();
  }

  return coinInfo;
}

/**
 * Calls the coinGecko API to update the coin price because the cache is stale
 */
async function updateCoinPrice(coin: string): Promise<coinInfo> {
  const GECKO_URI = `https://api.coingecko.com/api/v3/coins/${coin}?localization=false&tickers=false&market_data=true&community_data=false&developer_data=false&sparkline=false`;

  console.log(`Stale cache detected for ${coin}. Getting updated prices`);
  const res: geckoResponse = await axios.get(GECKO_URI).catch((error) => {
    console.error(error);
    throw new DogeitError({
      msg: "Could not contact gecko service",
      statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
    });
  });

  // Destructure the values I want to save
  const values = (({
    data: {
      name,
      market_cap_rank,
      market_data: {
        current_price: { usd: usd_cp },
        total_volume: { usd: usd_tv },
        high_24h: { usd: usd_h24 },
        low_24h: { usd: usd_l24 },
        price_change_24h_in_currency: { usd: usd_pc24 },
        price_change_percentage_24h,
        circulating_supply,
      },
    },
  }: typeof res) => ({
    coin_id: coin,
    name,
    market_cap_rank,
    current_price: usd_cp,
    total_volume: usd_tv,
    high_24h: usd_h24,
    low_24h: usd_l24,
    price_change_24h_in_currency: usd_pc24,
    price_change_percentage_24h,
    circulating_supply,
  }))(res);

  console.log(`Updated prices, inserting into database values:`);
  console.log(values);

  // insert new response into database
  return await addLatestPrice(values);
}
