import { StatusCodes } from "http-status-codes";
import { NextApiResponse } from "next";
import { errorHandler } from "../errors";
import PaginatedResults from "../paginatedClass/PaginatedResults";

/**
 * Method that returns a response paginated result of the target using pointer based pagination.
 * **Assumption:** Between refreshing the page, there aren't lots & lots of new posts
 * @param req request
 * @param res response
 * @param PageResults class to initialize to get the page results
 * @returns Response
 */
export async function paginatedResult(
  req: any,
  res: NextApiResponse,
  PageResults: new (req: any) => PaginatedResults
) {
  try {
    const pageResults = new PageResults(req);

    await pageResults.init();

    return res.status(StatusCodes.OK).json(pageResults.getReturnMessage());
  } catch (error) {
    return errorHandler(error, res);
  }
}
