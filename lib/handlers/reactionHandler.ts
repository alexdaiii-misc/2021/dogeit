// npm
import { NextApiResponse } from "next";
import { StatusCodes } from "http-status-codes";

// lib
import { errorHandler } from "../errors";
import { encode36, validateInputString } from "../encode36";
import { REACTIONS_PARAMS_TO_ENCODE } from "../encodeConsts";
import {
  returnedReactToPost,
  returnedReactToComment,
} from "./../queries/qryReturnVals";
import { POST_REACTIONS } from "../server-const/schemaConsts";
import { DogeitError } from "../errors";

// ts
import { reactToRecordSchema } from "../../types/schemas/reactSchemas";
import prisma from "../prisma";
import { reactions } from "@prisma/client";
import { disconnectAndPropogateError } from "../queries/disconnect";
import { RequestWithUser } from "../../types/requestTypes";
import { getReactionSchemaType } from "./../../types/schemas/reactSchemas.d";

type allowedMethods = "GET" | "POST";
type target = "post" | "comment";
type postReactions = typeof POST_REACTIONS[number];
type reactToComment = {
  user_id: bigint;
  comment_id: bigint;
  reaction: reactions;
};
type reactToPost = {
  user_id: bigint;
  post_id: bigint;
  reaction: reactions;
};
type reactRequestBody = reactToRecordSchema | getReactionSchemaType;

/**
 * Handles dealing with reactions for posts and comments
 * @param res Response
 * @returns response
 */
export async function reactionHandler(
  method: allowedMethods,
  req: RequestWithUser,
  res: NextApiResponse,
  data: reactRequestBody
) {
  const target: target = data.post_id ? "post" : "comment";

  try {
    const id = getTargetId(data, target);

    let reaction;

    switch (method) {
      case "GET":
        reaction = await getReactMethod(req, target, id);
        break;

      case "POST":
        const usrReaction = (data as reactToRecordSchema)
          .reaction as postReactions;

        reaction = await postReactMethod(req, target, usrReaction, id);
        break;

      default:
        break;
    }

    if (reaction) {
      encode36(reaction, REACTIONS_PARAMS_TO_ENCODE);
    }

    return res.status(StatusCodes.OK).json({ reaction });
  } catch (error: any) {
    return errorHandler(error, res, {
      prisma: {
        msg: error?.meta?.cause,
        statusCode: StatusCodes.NOT_FOUND,
      },
    });
  }
}

/**
 * Returns the id of the target to make a reaction to
 * @param req The post or comment must attached here
 * @param target a post xor comment to make reaction
 */
function getTargetId(data: reactRequestBody, target: target) {
  switch (target) {
    case "comment":
      const commentId = data.comment_id;

      if (!commentId) {
        throw new DogeitError({
          msg: "Comment id cannot be null if target is comment",
          statusCode: StatusCodes.BAD_REQUEST,
        });
      }

      return validateInputString(commentId);

    case "post":
      const postId = data.post_id;

      if (!postId) {
        throw new DogeitError({
          msg: "Post id cannot be null if target is comment",
          statusCode: StatusCodes.BAD_REQUEST,
        });
      }

      return validateInputString(postId);

    default:
      throw new Error("Target must be 'post' or 'comment'");
  }
}

async function getReactMethod(
  req: RequestWithUser,
  target: target,
  id: bigint
) {
  let reaction;
  switch (target) {
    case "comment":
      reaction = await prisma.reaction.findUnique({
        where: {
          reaction_comment_id_user_id_key: {
            comment_id: id,
            user_id: req.user.public_id,
          },
        },
        select: returnedReactToComment,
      });
      break;

    case "post":
      reaction = await prisma.reaction.findUnique({
        where: {
          reaction_post_id_user_id_key: {
            post_id: id,
            user_id: req.user.public_id,
          },
        },
        select: returnedReactToPost,
      });
      break;

    default:
      throw new Error("Target must be 'post' or 'comment'");
  }

  return reaction;
}

/**
 * If method is post, then react using this method
 */
async function postReactMethod(
  req: RequestWithUser,
  target: target,
  usrReaction: postReactions,
  id: bigint
) {
  if (usrReaction === "0") {
    // delete reaction
    return await deleteReactionToRecord(target, req.user.public_id, id);
  } else {
    // update or create raction
    return await reactToRecord(
      target,
      target === "post"
        ? {
            user_id: req.user.public_id,
            post_id: id,
            reaction: usrReaction,
          }
        : {
            user_id: req.user.public_id,
            comment_id: id,
            reaction: usrReaction,
          }
    );
  }
}

/**
 * Deletes a reaction to a post or comment
 */
async function deleteReactionToRecord(
  target: target,
  user_id: bigint,
  id: bigint
) {
  switch (target) {
    case "post":
      return await prisma.reaction.delete({
        where: {
          reaction_post_id_user_id_key: {
            user_id: user_id,
            post_id: id,
          },
        },
        select: returnedReactToPost,
      });

    case "comment":
      return await prisma.reaction.delete({
        where: {
          reaction_comment_id_user_id_key: {
            user_id: user_id,
            comment_id: id,
          },
        },
        select: returnedReactToComment,
      });

    default:
      throw new Error("Target must be 'post' or 'comment'");
  }
}

/**
 * Reacts to the record and returns the reaction
 */
async function reactToRecord(
  target: target,
  data: reactToComment | reactToPost
) {
  switch (target) {
    case "post":
      data = data as reactToPost;

      if (!data.post_id) {
        throw new Error("Post id must be specified");
      }

      // react to post use the post_id and user_id as the pk
      return await prisma.reaction
        .upsert({
          select: returnedReactToPost,
          where: {
            reaction_post_id_user_id_key: {
              post_id: data.post_id,
              user_id: data.user_id,
            },
          },
          update: {
            reaction: data.reaction,
          },
          create: data,
        })
        .catch(checkIfRecordDoesntExist);

    case "comment":
      data = data as reactToComment;

      if (!data.comment_id) {
        throw new Error("Comment id must be specified");
      }

      // react to cmt use the comment_id and user_id as the pk
      return await prisma.reaction
        .upsert({
          select: returnedReactToComment,
          where: {
            reaction_comment_id_user_id_key: {
              comment_id: data.comment_id,
              user_id: data.user_id,
            },
          },
          update: {
            reaction: data.reaction,
          },
          create: data,
        })
        .catch(checkIfRecordDoesntExist);

    default:
      throw new Error("Target must be 'post' or 'comment'");
  }
}

/**
 * Checks if error is a foreign constraint error on comments or posts fk
 */
async function checkIfRecordDoesntExist(error: any) {
  if (error?.code === "P2003") {
    throw new DogeitError({
      msg: "Record does not exist",
      statusCode: StatusCodes.NOT_FOUND,
    });
  }

  throw await disconnectAndPropogateError(error);
}
