import { RequestWithUser } from "../../types/requestTypes";
import UsersComments from "./UsersComments";

export default class MyComments extends UsersComments {
  protected getApiRoute(): string {
    return `/api/me/comments`;
  }

  /**
   * @returns the username of the target
   */
  protected getPageResultIdentifier() {
    return (this.req as RequestWithUser).user.username
  }
}
