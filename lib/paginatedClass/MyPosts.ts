import { RequestWithUser } from "../../types/requestTypes";
import UsersPosts from "./UsersPosts";

/**
 * Gets user logged in post Posts
 */
export default class MyPosts extends UsersPosts {
  /**
   * @returns the username of the target
   */
  protected getPageResultIdentifier() {
    return (this.req as RequestWithUser).user.username;
  }

  protected getApiRoute(): string {
    return `/api/me/posts`;
  }
}
