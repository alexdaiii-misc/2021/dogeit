import { ids } from "../../types/attachIdType";
import prisma from "../prisma";
import { disconnectAndPropogateError } from "../queries/disconnect";
import { returnedPostDetails } from "../queries/qryReturnVals";
import PostPageResults from "./PostPageResults";

export default class NewPosts extends PostPageResults {
  /**
   * No need for id, will get the newest post from all posts.
   */
  protected getPageResultIdentifier(): ids {
    return 0;
  }
  protected async queryDb(): Promise<any[]> {
    const newPosts = this.isCursorSpecific
      ? await prisma.posts
          .findMany({
            select: {
              ...returnedPostDetails,
              users: { select: { username: true } },
            },
            take: this.refresh ? -this.count : this.count,
            skip: this.skip,
            cursor: { id: this.cursor_id },
            orderBy: { id: "desc" },
          })
          .catch(disconnectAndPropogateError)
      : await prisma.posts.findMany({
          select: {
            ...returnedPostDetails,
            users: { select: { username: true } },
          },
          where: {
            id: {
              lte: this.cursor_id,
            },
          },
          take: this.count,
          orderBy: {
            id: "desc",
          },
        });

    return newPosts;
  }
  protected getApiRoute(): string {
    return "/api/post";
  }
}
