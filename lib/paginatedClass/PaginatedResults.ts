import { ids } from "../../types/attachIdType";
import { DEFAULT_POSTS_PER_PAGE } from "../server-const/schemaConsts";
import { NextApiRequestWithParams } from "../../types/requestTypes";
import { generateLatestId, stringToBigint } from "../encode36";
import { ENCODING_RADIX } from "../encodeConsts";

/**
 * Base class for getting paginated results. Examples include: posts of a rocket, comments of a post, posts of a user, or comments of a user
 */
export default abstract class PaginatedResults {
  /**
   * The request
   */
  protected req: NextApiRequestWithParams;
  /**
   * How many results to return
   */
  protected count: number = DEFAULT_POSTS_PER_PAGE;
  /**
   * Base 36 representation of cursor if user provided
   */
  protected query_id?: string;
  /**
   * The cursor id for pagination
   */
  protected cursor_id!: bigint;
  /**
   * do we have an offset we get the paginated results
   */
  protected skip: 0 | 1 = 0;
  /**
   * is the user refreshing or not
   */
  protected refresh: boolean = false;
  /**
   * The results from getting the paginated results
   */
  protected results: any[] = [];
  /**
   * The identifier to get the page results of
   */
  protected pageResultOfIdentifier: ids;
  /**
   * Tells if the user is calling for the first time
   */
  protected isCursorSpecific: boolean = true;

  /**
   * Initializer for paginated results
   * @param req Request
   */
  constructor(req: NextApiRequestWithParams) {
    this.req = req;

    this.pageResultOfIdentifier = this.getPageResultIdentifier();

    this.count = this.getCount();

    this.initFromQuery();
  }

  /**
   * Initializes some async code
   * Throws: Error if operation fails
   */
  async init() {
    this.results = await this.queryDb();
    this.serializeResults();
  }

  /**
   * Gets the count
   */
  protected getCount() {
    const count = this.req?.query?.count as unknown as number | undefined;

    if (!count) {
      throw new Error("Cannot get the count from the request query");
    }

    return count;
  }

  /**
   * Helper method to initialize some parameters
   */
  protected initFromQuery() {
    // if its empty, the user is initially loading the page
    this.query_id = (this.req.query?.max_id ?? this.req.query.since_id) as
      | string
      | undefined;

    if (!this.query_id) {
      this.cursor_id = generateLatestId();
      this.isCursorSpecific = false;
    } else {
      this.cursor_id = stringToBigint(this.query_id, ENCODING_RADIX);
      // only skip if not refreshing the page
      // this is because refreshing will need to return at least one item
      if (this.req.query.max_id) {
        this.skip = 1;
      } else {
        // user provided the since_id so they are refreshing
        this.refresh = true;
      }
    }
  }

  /**
   * Gets the page identifier to get the page results of
   */
  protected abstract getPageResultIdentifier(): ids;

  /**
   * Calls the database to get the searalized results
   * Throws: Error if operation fails
   */
  protected abstract queryDb(): Promise<any[]>;

  /**
   * Generates and sends back the return message
   */
  getReturnMessage() {
    // get the route the user should call
    const apiRoute = this.getApiRoute();

    let max_id, since_id;

    // this can happen if run at the end of the page
    if (this.results.length === 0) {
      max_id = since_id = "";
    } else {
      let cursors = this.getNextCursors();
      max_id = cursors.max_id;
      since_id = cursors.since_id;
    }

    const basePaginatedURI = `${apiRoute}?count=${this.count}`;

    return {
      results: this.results,
      search_metadata: {
        // next url to get the paginated results
        next_results: `${basePaginatedURI}${
          max_id.length > 0 ? `?max_id=${max_id}` : ""
        }`,
        // url when user pulls up to refresh
        refresh_url: `${basePaginatedURI}${
          since_id.length > 0 ? `?since_id=${since_id}` : ""
        }`,
        /**
         * **client:** needs to store this after every request
         * **client:** keep the prev max_id since if the next max id gives [] then it is end
         * - except when no more posts, then just save the last max_id
         */
        max_id,
        /**
         * **client:** needs to store this
         * - on initial page load when sending with no params
         * - after refreshing the page
         */
        since_id,

        // the rest is what the user sent to us
        count: this.count,
      },
    };
  }

  /**
   * Returns the api route the user calls to get the next paginated results
   */
  protected abstract getApiRoute(): string;

  /**
   * The query cursors user should put in the next request
   */
  protected abstract getNextCursors(): { max_id: string; since_id: string };

  /**
   * Encode it so its json safe
   */
  protected abstract serializeResults(): void;
}
