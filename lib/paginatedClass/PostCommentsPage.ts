import { COMMENT_PARAMS_TO_ENCODE, ENCODING_RADIX } from "./../encodeConsts";
import { PostIdFromURI } from "./../attachId/postId";
import { postBaseURI } from "../server-const/routeNames";
import prisma from "../prisma";
import { prismaSafePaginatedComment } from "../../types/qryTypes/paginated/paginatedPostComments";
import { disconnectAndPropogateError } from "../queries/disconnect";
import PaginatedResults from "./PaginatedResults";

/**
 * Gets the paginated result of the comments of a post
 */
export default class PostCommentsPage extends PaginatedResults {
  results: prismaSafePaginatedComment[] = [];

  /**
   * @returns the rocket name from the uri
   */
  protected getPageResultIdentifier() {
    return new PostIdFromURI(this.req).getId();
  }

  protected async queryDb(): Promise<prismaSafePaginatedComment[]> {
    /**
     * FUNCTION get_paginated_comments(
     * post_selected_id BIGINT,
     * root_id_cursor BIGINT,
     * count_limit INTEGER,
     * is_refresh BOOLEAN = False
     * )
     */
    const postComments = (await prisma
      .$queryRaw(
        `SELECT * FROM get_paginated_comments(${this.pageResultOfIdentifier.toString()}, ${this.cursor_id.toString()}, $1, $2)`,
        this.count,
        this.refresh
      )
      .catch(disconnectAndPropogateError)) as prismaSafePaginatedComment[];

    return postComments;
  }

  protected getApiRoute() {
    return `${postBaseURI}/${this.pageResultOfIdentifier.toString(
      ENCODING_RADIX
    )}/comments`;
  }

  protected serializeResults() {
    this.results.forEach((value) => {
      COMMENT_PARAMS_TO_ENCODE.forEach((param) => {
        if ((value as any)[param]) {
          (value as any)[param] = BigInt(
            (value as any)[param] as string
          ).toString(ENCODING_RADIX);
        }
      });

      value.id = BigInt(value.comment_id_astring as string).toString(
        ENCODING_RADIX
      );

      delete value.comment_id_astring;
    });
  }

  protected getNextCursors(): { max_id: string; since_id: string } {
    // the comments have already been searalized
    // we are using root id as the cursor
    return {
      max_id: this.results[this.results.length - 1].root_id,
      since_id: this.results[0].root_id,
    };
  }
}
