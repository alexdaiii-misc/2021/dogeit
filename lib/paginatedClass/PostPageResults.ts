import { encode36Array } from "../encode36";
import { POST_PARAMS_TO_ENCODE } from "../encodeConsts";
import PaginatedResults from "./PaginatedResults";

/**
 * Abstract class for paginated results that return posts
 */
export default abstract class PostPageResults extends PaginatedResults {
  protected serializeResults() {
    encode36Array(this.results, POST_PARAMS_TO_ENCODE);
  }

  protected getNextCursors(): { max_id: string; since_id: string } {
    // the posts have already been searalized
    return {
      max_id: this.results[this.results.length - 1].id as unknown as string,
      since_id: this.results[0].id as unknown as string,
    };
  }
}
