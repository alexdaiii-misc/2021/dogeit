import { ids } from "../../types/attachIdType";
import prisma from "../prisma";
import PaginatedResults from "./PaginatedResults";

/**
 * Gets a list of rockets, starting from the oldest rockets
 */
export default class RocketList extends PaginatedResults {
  results: {
    name: string;
  }[] = [];

  /**
   * No need for id, will get the oldest rocket from all rocket.
   */
  protected getPageResultIdentifier(): ids {
    return 0;
  }

  /**
   * Helper method to initialize some parameters
   */
  protected initFromQuery() {
    // if its empty, the user is initially loading the page
    this.query_id = (this.req.query?.max_id ?? this.req.query.since_id) as
      | string
      | undefined;

    if (!this.query_id) {
      this.cursor_id = BigInt(0);
      this.isCursorSpecific = false;
    } else {
      this.cursor_id = this.query_id as any;
      // only skip if not refreshing the page
      // this is because refreshing will need to return at least one item
      if (this.req.query.max_id) {
        this.skip = 1;
      } else {
        // user provided the since_id so they are refreshing
        this.refresh = true;
      }
    }
  }

  protected async queryDb() {
    // cursor_id is actually a string
    const cursor = this.cursor_id as unknown as string;

    console.log(cursor);

    const rocketList = this.isCursorSpecific
      ? await prisma.rockets.findMany({
          select: { name: true },
          take: this.refresh ? -this.count : this.count,
          skip: this.skip,
          cursor: {
            name: cursor,
          },
          orderBy: {
            id: "asc",
          },
        })
      : // first time calling
        await prisma.rockets.findMany({
          select: {
            name: true,
          },
          where: {
            id: {
              lte: this.count,
            },
          },
          take: this.count,
          orderBy: {
            id: "asc",
          },
        });

    return rocketList;
  }

  protected getApiRoute(): string {
    return "/api/rocket";
  }

  protected getNextCursors(): { max_id: string; since_id: string } {
    return {
      max_id: this.results[this.results.length - 1].name,
      since_id: this.results[0].name,
    };
  }

  /**
   * Nothing here since all results are already strings
   */
  protected serializeResults(): void {}
}
