import { returnedPostDetails } from "./../queries/qryReturnVals";
import { RocketNameFromURI } from "./../attachId/rocketId";
import { rocketBaseURI } from "../server-const/routeNames";
import prisma from "../prisma";
import { disconnectAndPropogateError } from "../queries/disconnect";
import PostPageResults from "./PostPageResults";

/**
 * Gets the paginated posts of a rocket
 */
export default class RocketPostsPage extends PostPageResults {
  /**
   * @returns the rocket name from the uri
   */
  protected getPageResultIdentifier() {
    return new RocketNameFromURI(this.req).getId();
  }

  protected async queryDb() {
    const rocketPosts = this.isCursorSpecific
      ? await prisma.posts
          .findMany({
            select: {
              ...returnedPostDetails,
              users: { select: { username: true } },
            },
            take: this.refresh ? -this.count : this.count,
            skip: this.skip,
            cursor: { id: this.cursor_id },
            orderBy: { id: "desc" },
            where: {
              rockets: {
                name: this.pageResultOfIdentifier as string,
              },
            },
          })
          .catch(disconnectAndPropogateError)
      : await prisma.posts.findMany({
          select: {
            ...returnedPostDetails,
            users: { select: { username: true } },
          },
          where: {
            rockets: {
              name: this.pageResultOfIdentifier as string,
            },
            id: {
              lte: this.cursor_id,
            },
          },
          take: this.count,
          orderBy: {
            id: "desc",
          },
        });

    return rocketPosts;
  }

  protected getApiRoute() {
    return `${rocketBaseURI}/${this.pageResultOfIdentifier}/posts`;
  }
}
