import { COMMENT_PARAMS_TO_ENCODE } from "./../encodeConsts";
import { returnedCommentDetails } from "./../queries/qryReturnVals";
import { UsernameFromURI } from "../attachId/userId";
import prisma from "../prisma";
import { disconnectAndPropogateError } from "../queries/disconnect";
import PaginatedResults from "./PaginatedResults";
import { encode36Array } from "../encode36";

/**
 * Gets a user's comments
 */
export default class UsersComments extends PaginatedResults {
  /**
   * @returns the users comments paginated
   */
  protected async queryDb() {
    return this.isCursorSpecific
      ? // got a cursor
        await prisma.comments
          .findMany({
            select: {
              ...returnedCommentDetails,
              posts: {
                select: {
                  rockets: {
                    select: {
                      name: true,
                    },
                  },
                },
              },
            },
            where: {
              users: { username: this.pageResultOfIdentifier as string },
            },
            take: this.refresh ? -this.count : this.count,
            skip: this.skip,
            cursor: { id: this.cursor_id },
            orderBy: { id: "desc" },
          })
          .catch(disconnectAndPropogateError)
      : // didn't get a cursor
        await prisma.comments.findMany({
          select: {
            ...returnedCommentDetails,
            posts: {
              select: {
                rockets: {
                  select: {
                    name: true,
                  },
                },
              },
            },
          },
          where: {
            users: { username: this.pageResultOfIdentifier as string },
            id: {
              lte: this.cursor_id,
            },
          },
          take: this.count,
          orderBy: {
            id: "desc",
          },
        });
  }

  protected getApiRoute(): string {
    return `/api/${this.pageResultOfIdentifier}/comments`;
  }

  /**
   * @returns the username of the target
   */
  protected getPageResultIdentifier() {
    let username = new UsernameFromURI(this.req).getId();

    return username;
  }

  protected serializeResults() {
    encode36Array(this.results, COMMENT_PARAMS_TO_ENCODE);
  }

  protected getNextCursors(): { max_id: string; since_id: string } {
    // the posts have already been searalized
    return {
      max_id: this.results[this.results.length - 1].id as unknown as string,
      since_id: this.results[0].id as unknown as string,
    };
  }
}
