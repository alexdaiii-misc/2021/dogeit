import { UsernameFromURI } from "../attachId/userId";
import prisma from "../prisma";
import { disconnectAndPropogateError } from "../queries/disconnect";
import { returnedPostDetails } from "../queries/qryReturnVals";
import PostPageResults from "./PostPageResults";

/**
 * Gets a user's posts
 */
export default class UsersPosts extends PostPageResults {
  /**
   * @returns the username of the target
   */
  protected getPageResultIdentifier() {
    let username = new UsernameFromURI(this.req).getId();

    return username;
  }

  protected async queryDb() {
    const userPosts = this.isCursorSpecific
      ? // we have a cursor
        await prisma.posts
          .findMany({
            select: {
              ...returnedPostDetails,
              users: { select: { username: true } },
              rockets: { select: { name: true } },
            },
            take: this.refresh ? -this.count : this.count,
            skip: this.skip,
            cursor: { id: this.cursor_id },
            orderBy: { id: "desc" },
            where: {
              users: {
                username: this.pageResultOfIdentifier as string,
              },
            },
          })
          .catch(disconnectAndPropogateError)
      : // we don't have a cursor
        await prisma.posts.findMany({
          select: {
            ...returnedPostDetails,
            users: { select: { username: true } },
            rockets: { select: { name: true } },
          },
          where: {
            users: {
              username: this.pageResultOfIdentifier as string,
            },
            id: {
              lte: this.cursor_id,
            },
          },
          take: this.count,
          orderBy: {
            id: "desc",
          },
        });

    return userPosts;
  }

  protected getApiRoute(): string {
    return `/api/${this.pageResultOfIdentifier}/posts`;
  }
}
