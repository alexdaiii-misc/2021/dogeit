import prisma from "../prisma";
import { disconnectAndPropogateError } from "./disconnect";

/**
 * Gets the crypto prices that have been inserted since the given timedId
 * @param coin_id the coin to get the price of
 * @param timedId an id with times and shard info
 * @returns an array of coin prices since the given timedId sorted from latest to earliest
 */
export async function getLatestPrice(coin_id: string, timedId: bigint) {
  return await prisma.crypto_prices
    .findMany({
      where: {
        id: {
          gte: timedId,
        },
        coin_id,
      },
      orderBy: {
        id: "desc",
      },
    })
    .catch(disconnectAndPropogateError);
}

/**
 * Adds latest crypto prices to db
 * @param data data to insert
 */
export async function addLatestPrice(data: any) {
  return await prisma.crypto_prices
    .create({ data })
    .catch(disconnectAndPropogateError);
}
