import { DogeitError } from "../../errors";
import { disconnectAndPropogateError } from "../disconnect";

/**
 * Handles known errors from the database thrown. To call this function, put this in the catch block and throw the error returned from this
 */
export default async function dbErrors(error: any) {
  if (error?.meta?.message && error?.meta?.code === "P0001") {
    console.error(error);

    const msg = (error.meta.message as string).split("db error: ERROR: ");

    if (msg.length >= 2) {
      throw new DogeitError(JSON.parse(msg[1]));
    }
  }

  if (error?.code) {
    throw await disconnectAndPropogateError(error);
  }

  throw error;
}
