import prisma from "../prisma";

export async function disconnect(verbose: boolean = true) {
  // Attempt to disconnect from database
  await prisma.$disconnect().catch((error) => {
    if (verbose) {
      console.error("Error disconnecting from database");
      console.error("Propogating Error from Disconnect");
      console.error(error);
    }

    throw error;
  });
}

/**
 * Attempts to disconnects from a database after getting an error
 * @param error Error from previous call
 */
const disconnectAndPropogateError = async (
  error: any,
  verbose: boolean = false
) => {
  if (verbose) {
    console.error("Attempting to disconnect from database");
  }
  // Attempt to disconnect from database
  await disconnect().catch((error) => {
    throw error;
  });
  if (verbose) {
    console.error("Successfully disconnected from database");
    console.error("Propogating Error from previous call");
    console.error(error);
  }

  // Propagate the error to the client to let them deal with it how they see fit
  throw error;
};

export { disconnectAndPropogateError };
