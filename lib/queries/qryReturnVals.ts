export const returnedUserDetails = {
  public_id: true,
  username: true,
  name: true,
  email: true,
  image: true,
  created_at: true,
  site_admin: true,
};

export const returnedRocketDetails = {
  name: true,
  created_at: true,
  updated_at: true,
  about: true,
  restrict_posting: true,
};

export const returnedPostDetails = {
  id: true,
  crypto_prices_id: true,
  user_id: true,
  created_at: true,
  updated_at: true,
  title: true,
  body: true,
};

export const returnedCommentDetails = {
  id: true,
  post_id: true,
  crypto_prices_id: true,
  root_id: true,
  parent_id: true,
  created_at: true,
  updated_at: true,
  body: true,
};

export const returnedReactToPost = {
  post_id: true,
  created_at: true,
  updated_at: true,
  reaction: true,
};

export const returnedReactToComment = {
  comment_id: true,
  user_id: true,
  created_at: true,
  updated_at: true,
  reaction: true,
};
