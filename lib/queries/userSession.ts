import { disconnectAndPropogateError } from "./disconnect";
import prisma from "../prisma";
import { returnedUserDetails } from "./qryReturnVals";

/**
 * Gets the user information from the session token
 * @param session Session from Next-Auth
 * @returns The userId
 */
export async function getUserFromSessionToken(session_token: string) {
  /*
  SELECT *
  FROM   users
  WHERE  users.id IN (SELECT user_id
                    FROM   sessions
                    WHERE  session_token = $session_token
                    AND    expires >= NOW())  

  */

  return await prisma.users
    .findFirst({
      select: {
        ...returnedUserDetails,
        sessions: { where: { session_token, expires: { gte: new Date() } } },
      },
      where: {
        sessions: {
          some: {
            session_token,
            expires: {
              gt: new Date(),
            },
          },
        },
      },
    })
    .catch(disconnectAndPropogateError);
}
