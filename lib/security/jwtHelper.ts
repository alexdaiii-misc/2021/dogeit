// Typescript
import { NextApiResponse } from "next";
import { userReq } from "../../types/requestTypes";

// lib fns
import { encode36 } from "../encode36";
import { USER_PARAMS_TO_ENCODE } from "../encodeConsts";
import { setCookie, cookieOptions } from "./setCookie";

// security
import jwt from "jsonwebtoken";
import { serialize } from "cookie";

/**
 * Issues a new JWT
 * @param user user information in request to encode
 * @param res response
 */
export function issueJWT(user: userReq, res: NextApiResponse) {
  const userCopy = { ...user };
  encode36(userCopy, USER_PARAMS_TO_ENCODE);

  const jwtToken = jwt.sign({ data: { user: userCopy } }, getJWTSecret(), {
    expiresIn: process.env.JWT_EXPIRES_IN ?? "15m",
  });

  const jwtCookie = serialize(getJWTCookieName(), jwtToken, cookieOptions);

  setCookie(res, jwtCookie);
}

/**
 * Returns the jwt secret
 */
export function getJWTSecret() {
  if (!process.env.JWT_SIGNING_TOKEN) {
    throw new Error("A signing secret must be provided to sign the JWT");
  }

  return process.env.JWT_SIGNING_TOKEN;
}

/**
 * Returns the jwt cookie name
 */
export function getJWTCookieName() {
  if (!process.env.NEXTAUTH_URL) {
    throw new Error("NEXTAUTH_URL must be defined as an enviornment variable");
  }

  return `${
    process.env.NEXTAUTH_URL.startsWith("https://") ? "__Secure-" : ""
  }${process.env.JWT_ACCESS_COOKIE_NAME ?? "access_token"}`;
}
