/**
 * Parts of this based off of the Next-Auth code @nextauthjs (ISC License)
 * https://github.com/nextauthjs/next-auth
 *
 * Function to set cookies server side
 *
 * From Next-Auth:
 * Credit to @huv1k and @jshttp contributors for the code which this is based on (MIT License).
 * * https://github.com/jshttp/cookie/blob/master/index.js
 * * https://github.com/zeit/next.js/blob/master/examples/api-routes-middleware/utils/cookies.js
 *
 */

const DEFAULT_SITE = "";

const site = process.env.NEXTAUTH_URL ?? DEFAULT_SITE;

let secureCookies = site.startsWith("https://");
if (process.env.SECURE_COOKIES) {
  // More selective on false bc overriding the default behavior is much more dangerous if false set in production
  if (process.env.SECURE_COOKIES === "false") {
    secureCookies = false;
  } else {
    secureCookies = true;
  }
}

// Client must be able to grab the csrf token from the cookie and append it to the header
// Header should be something like 'X-XSRF-TOKEN' when client sends to api
// let nextauthCsrfTokenHttpOnly = false;
// if (process.env.NEXTAUTH_CSRF_TOKEN_HTTPONLY) {
//   // More selective on true bc breaks functionality with no increase in security with true
//   if (process.env.NEXTAUTH_CSRF_TOKEN_HTTPONLY === "true") {
//     nextauthCsrfTokenHttpOnly = true;
//   } else {
//     nextauthCsrfTokenHttpOnly = false;
//   }
// }

const DEFAULT_CSRF_COOKIE_NAME = "next-auth.csrf-token";
const DEFAULT_SESSION_COOKIE_NAME = "next-auth.session-token";
const DEFAULT_CALLBACK_COOKIE_NAME = "next-auth.session-token";
const DEFAULT_PKCE_COOKIE_NAME = "next-auth.pkce.code_verifier";

// Basically directly from Next Auth in their server/lib/cookie.js code
const cookiePrefix = secureCookies ? "__Secure-" : "";

// Default to __Host- for CSRF token for additional protection if using secureCookies
// NB: The `__Host-` prefix is stricted than the `__Secure-` prefix.
const csrfTokenName = `${secureCookies ? "__Host-" : ""}${
  process.env.NEXTAUTH_CSRF_TOKEN_NAME ?? DEFAULT_CSRF_COOKIE_NAME
}`;

const sessionTokenName = `${cookiePrefix}${
  process.env.NEXTAUTH_SESSION_TOKEN_NAME ?? DEFAULT_SESSION_COOKIE_NAME
}`;

const callbackUrlTokenName = `${cookiePrefix}${
  process.env.NEXTAUTH_CALLBACK_TOKEN_NAME ?? DEFAULT_CALLBACK_COOKIE_NAME
}`;

const pkceTokenName = `${cookiePrefix}${
  process.env.NEXTAUTH_PKCE_TOKEN_NAME ?? DEFAULT_PKCE_COOKIE_NAME
}`;

/**
 * Handler for importing process.env variables and setting default values
 */
export default {
  databaseUrl: process.env.NEXT_AUTH_DATABASE_URL ?? null,
  cookieSecret: process.env.COOKIE_SECRET ?? null,
  site,
  csrfTokenName,
  secureCookies,
  sessionTokenName,
  callbackUrlTokenName,
  pkceTokenName,
};
