/**
 * Return session cookie name
 */
export function getSessionCookieName() {
  if (!process.env.NEXTAUTH_URL) {
    throw new Error("NEXTAUTH_URL must be defined as an enviornment variable");
  }

  const DEFAULT_SESSION_COOKIE_NAME = "next-auth.session-token";

  return `${
    process.env.NEXTAUTH_URL.startsWith("https://") ? "__Secure-" : ""
  }${process.env.NEXTAUTH_SESSION_TOKEN_NAME ?? DEFAULT_SESSION_COOKIE_NAME}`;
}
