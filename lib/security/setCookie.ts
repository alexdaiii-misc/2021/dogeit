import { NextApiResponse } from "next";

export const cookieOptions = {
  path: "/",
  sameSite: "lax" as "lax",
  secure: true,
};

export function setCookie(res: NextApiResponse, nextCookie: string) {
  let cookiesArr: string[];

  const cookies = removeDuplicateCookie(
    res.getHeaders()["set-cookie"] as string[] | undefined,
    nextCookie
  );

  if (cookies) {
    cookiesArr = [nextCookie, ...cookies];
  } else {
    cookiesArr = [nextCookie];
  }

  res.setHeader("set-cookie", cookiesArr);
  return;
}

function removeDuplicateCookie(
  cookies: string[] | undefined,
  nextCookie: string
) {
  const newCookieName = nextCookie.split("=")[0];

  if (cookies) {
    return cookies
      .map((value) => {
        if (value.split("=")[0] !== newCookieName) {
          return value;
        }
      })
      .filter((value) => {
        return value !== undefined;
      }) as string[] | undefined;
  }
}
