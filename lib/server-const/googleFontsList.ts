export const googleFontsList = [
  "Roboto",
  "Lato",
  "Montserrat",
  "Poppins",
  "Oswald",
  "Roboto Mono",
  "Raleway",
  "Ubuntu",
  "Merriweather",
  "Zen Tokyo Zoo",
  "Pacifico",
  "Amatic SC",
  "Permanent Marker",
  "Caveat",
  "Bebas Neue",
  "Lobster",
  "Abril Fatface",
  "Righteous",
  "Inconsolata",
  "Source Code Pro",
  "IBM Plex Mono",
  "Roboto Slab",
  "Playfair Display",
  "Libre Baskerville",
  "Lora",
];
