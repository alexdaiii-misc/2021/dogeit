// Routing

export const rocketBaseURI = `/api/rocket`;
export const rocketIdSlug = "rocketId";
export const rocketShip = `${rocketBaseURI}/:${rocketIdSlug}`;

export const postBaseURI = `/api/post`;
export const postIdSlug = "postId";
export const postIndiv = `${postBaseURI}/:${postIdSlug}`;

export const geckoBaseURI = `/api/gecko`;
export const cryptocoinSlug = "coinId";
export const coinRoute = `${geckoBaseURI}/coin/:${cryptocoinSlug}`;

export const historicIdSlug = "crypto_price_id";
export const historicalPriceRoute = `${geckoBaseURI}/historic/:${historicIdSlug}`;

export const commentBaseURI = `/api/comment`;
export const commentIdSlug = "commentId";
export const commentIndiv = `${commentBaseURI}/:${commentIdSlug}`;

export const userBaseURI = `/api/user`;
export const usernameSlug = "username";
export const userIndiv = `${userBaseURI}/:${usernameSlug}`;
