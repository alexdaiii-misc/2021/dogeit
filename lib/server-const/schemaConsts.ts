// Schema validation

import { reactions } from "@prisma/client";

export const USER_USERNAME_REGEX = /^[\w-]+$/;
export const USER_NAME_REGEX = /^[\w-][\w- ]+$/;
export const ROCKET_NAME_REGEX = /^[a-z0-9][a-z0-9_]+$/;
export const ONLY_INT_REGEX = /^\d+$/;
export const ONLY_ALPHANUMERIC_REGEX = /^[a-zA-Z0-9]*$/;
export const CSS_COLOR_REGEX = /^#[0-9a-f]{3,6}$/i;

export const MIN_USERNAME_LENGTH = 3;
export const MAX_USERNAME_LENGTH = 20;
export const MAX_NAME_LEN = 255;

export const MIN_ROCKET_NAME = 3;
export const MAX_ROCKET_NAME = 21;

export const MAX_ABOUT_CHARS = 1000;
export const MAX_RULES_CHARS = 1000;

export const MIN_SUBMISSION_LEN = 1;

// pagination constants
export const SUBS_PER_PAGE = 50;

export const MIN_POSTS_PER_PAGE = 10;
export const MAX_POSTS_PER_PAGE = 25;
export const DEFAULT_POSTS_PER_PAGE = 25;

export const ROCKET_POSTS_SORTS = [
  "hot",
  "new",
  "top",
  "controversial",
] as const;

export const POST_REACTIONS = ["0", ...Object.values(reactions)] as const;

export const POST_DELETION_ACTIONS = ["mark", "delete"] as const;

/**
 * The values in the post body that needs to be checked
 */
export const POST_BODY_CHK_LEN = ["title", "body"];
/**
 * The values in the comment body that needs to be checked
 */
export const COMMENT_BODY_CHK_LEN = ["body"];

export const SUBSCRIBER_QUERY = ["sub", "mod"] as const;
