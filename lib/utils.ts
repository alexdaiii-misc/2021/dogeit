import { StatusCodes } from "http-status-codes";
import { NextApiRequest, NextApiResponse } from "next";

/**
 * Callback on no match for next connect
 */
export function onNoMatch(req: NextApiRequest, res: NextApiResponse) {
  const GETTER_METHODS = ["GET", "HEAD", "OPTIONS"];

  // My client should never encounter these errors
  if (GETTER_METHODS.includes(req.method!)) {
    return res.status(StatusCodes.NOT_FOUND).json({ msg: "Page Not Found" });
  }

  return res.status(StatusCodes.METHOD_NOT_ALLOWED).json({ msg: "Method not supported" });
}
