import { StatusCodes } from "http-status-codes";
import { NextApiResponse } from "next";
import { RequestWithSession } from "../types/requestTypes";
import { serialize } from "cookie";
import Tokens from "csrf";
import { setCookie } from "../lib/security/setCookie";
import { getJWTCookieName } from "../lib/security/jwtHelper";
import { errorHandler } from "../lib/errors";
import { getSessionCookieName } from "../lib/security/sessionHelper";

/*
 * Inspired by the next-auth csrf verification code and csurf csrf verification code
 */
type sameSite = boolean | "strict" | "lax" | "none";

type CsrfOptions = {
  ignoreMethods?: Array<string>;
  setCookieMethod?: Array<string>;
  key?: string;
  secret?: string;
  secure?: boolean;
  sameSite?: boolean | "strict" | "lax" | "none";
  // log?: boolean;
  onFailure?: Function;
};

enum CsrfCheck {
  NoCookie,
  NoHeader,
  CookieTokenMismatch,
  CookieHashMismatch,
  Passed,
}

/**
 * Ensure CSRF Token cookie is set for any subsequent requests.
 * Used as part of the strategy for mitigation for CSRF tokens.
 * Automatically sets a CSRF cookie if there isn't a cookie on a GET method.
 * Designed to be easier to use with serverless functions by reading
 * process.env variables on server instead of passing in options, since
 * Next.js splits up api routes into different files. Best used with
 * next-connect, which makes Next.js api routes more express-like.
 * Verifies using the cookie double submit method.
 *
 * **Note:** Will not close out database connections after response if no value passed to onFailure
 *
 * ## Options
 * - **ignoreMethods**: Methods that the csrf token is not checked. Defaults to "GET", "HEAD", and "OPTIONS"
 * - **setCookieMethod**: Methods to automatically set the csrf token if the token is not found. Defaults to "GET"
 * - **secure**: Use secure cookies (Only accessable with https). If no options passed, can be set in `processs.env.SECURE_COOKIES`, or will be set to `true` for https sites and `false` for http sites in `process.env.SITE_URL` or `process.env.NEXTAUTH_URL`. Otherwise, defaults to false.
 * - **key**: The name of the csrf cookie. Can be set in `process.env.CSRF_TOKEN_NAME`. Defaults to '__Host-xsrf-token' for secure cookies and 'xsrf-token' for non secure cookies.
 * - **secret**: The secret to sign the csrf cookie. Can be set in `process.env.COOKIE_SECRET`.
 * - **sameSite**: Sets the same site policy for the cookie (defaults to lax). This can be set to 'strict', 'lax', 'none', or true (which maps to 'strict'). This can also be set in `process.env.SAME_SITE`
 * - **onFailure**: Function to run on failure. Can be used to close out database connections if previous middleware opened a db connection.
 * @param options Options for the csrf cookie
 * @returns middleware function to handle csrf token verification
 */
export default function csrfProtection(opt: CsrfOptions = {}) {
  return async function (
    req: RequestWithSession,
    res: NextApiResponse,
    next: Function
  ) {
    try {
      const options = getOptions(req, opt);

      const forbiddenErrorMsg = {
        msg: "Invalid CSRF Token",
      };

      const tokens = new Tokens({
        secretLength: options.secret.length,
      });

      const csrfCheck = passedCsrfChecks(req, options, tokens);

      if (csrfCheck !== CsrfCheck.Passed) {
        // Types of failures to set a cookie on
        const setCookieFailures =
          csrfCheck === CsrfCheck.NoCookie ||
          csrfCheck === CsrfCheck.CookieHashMismatch;

        // if its a set cookie method, set the csrf token
        if (
          setCookieFailures &&
          options.setCookieMethod.includes(req.method!)
        ) {
          const cookie = serialize(options.key, tokens.create(options.secret), {
            secure: options.secure,
            sameSite: options.sameSite as sameSite,
            path: "/",
          });
          setCookie(res, cookie);
        }

        // Don't care if we failed csrf checks
        if (options.ignoreMethods.includes(req.method!)) {
          // Need to set the cookie on get request
          return next();
        }

        // Runs any function on failure if passed in
        if (options.onFailure) {
          options.onFailure();
        }

        // We do care if failed csrf checks
        return res.status(StatusCodes.FORBIDDEN).json(forbiddenErrorMsg);
      }

      return next();
    } catch (error: any) {
      return errorHandler(error, res);
    }
  };
}

/**
 * Checks if the request passes the CSRF checks
 * @param req Request with session
 * @param options Options passed into middleware
 * @param tokens Token object used to verify csrf token
 * @returns true if passed checks false if not
 */
function passedCsrfChecks(
  req: RequestWithSession,
  options: ReturnType<typeof getOptions>,
  tokens: Tokens
) {
  // the csrf cookie
  const cookiesCsrfToken = req.cookies[options.key];

  // No CSRF cookie set
  if (!req.headers.cookie || !cookiesCsrfToken) {
    return CsrfCheck.NoCookie;
  }

  // Second check to make sure that the tokens were signed from the server
  if (!tokens.verify(options.secret, cookiesCsrfToken)) {
    // hash doesn't match because it's been modifed or because the secret has changed
    return CsrfCheck.CookieHashMismatch;
  }

  // Now check headers - Must be done after since 'GET'/'HEAD'/'OPTIONS' may not have the headers
  // Check if the client took the cookie and double submitted it
  let headerToken = defaultValue(req);
  if (!headerToken) {
    return CsrfCheck.NoHeader;
  }

  // The header token may be url encoded when we get it
  headerToken = decodeURI(defaultValue(req));

  // Basic check to make sure the x-xsrf-token and the cookie xsrf-token are the same
  if (headerToken !== cookiesCsrfToken) {
    return CsrfCheck.CookieTokenMismatch;
  }

  return CsrfCheck.Passed;
}

/**
 * Set secure cookies or not
 * @param secure userdefined option returned if set
 * @returns set secure cookies or not
 */
function secureCookies(secure?: boolean) {
  // secure option passed in overrides everything

  if (secure !== undefined) {
    return secure;
  }

  // SECURE_COOKIES is set in env file
  if (
    process.env.SECURE_COOKIES &&
    process.env.SECURE_COOKIES.toLowerCase() === "true"
  ) {
    return true;
  }

  // Check if site passed in
  const site = process.env.SITE_URL || process.env.NEXTAUTH_URL;

  // Default behavior
  if (!site) {
    return false;
  }

  return site.startsWith("https://");
}

/**
 * Returns user defined options or defaults for
 * @param opt options object
 * @returns options object with no undefined
 */
function getOptions(req: RequestWithSession, opt: CsrfOptions) {
  const secure = secureCookies(opt.secure);

  let secret;
  const session = req.cookies[getSessionCookieName()];

  if (!opt.secret && !process.env.COOKIE_SECRET && !session) {
  }
  if (opt.secret) {
    // first if option is provided, then use that
    secret = opt.secret;
  } else if (session || req.cookies[getJWTCookieName()]) {
    // then if the user is logged in, use the session token as the signing secret
    secret = session + process.env.COOKIE_SECRET;
  } else if (process.env.COOKIE_SECRET) {
    // last, user not logged in so just use the server secret
    secret = process.env.COOKIE_SECRET;
  } else {
    // no secret provided, no set server secret, or no session to sign the cookie
    console.warn(
      "\x1b[31m",
      "Warning: No secret provided and no session cookie detected. Are you sure you are calling this after getting the session? Setting secret to: 'session'. This is pretty much equivalent to not signing the cookie"
    );
    secret = "session";
  }

  return {
    ignoreMethods: opt.ignoreMethods || ["GET", "HEAD", "OPTIONS"],
    setCookieMethod: opt.setCookieMethod || ["GET"],
    key: `${secure ? "__Host-" : ""}${
      opt.key || process.env.CSRF_TOKEN_NAME || "XSRF-TOKEN"
    }`,
    secret: secret,
    secure,
    sameSite: opt.sameSite || (process.env.SAME_SITE as sameSite) || "lax",
    // log,
    onFailure: opt.onFailure,
  };
}

/**
 *
 * Default value function, checking the `req.body`
 * and `req.query` for the CSRF token.
 *
 * @param {IncomingMessage} req
 * @return {String}
 * @api private
 */
function defaultValue(req: RequestWithSession): string {
  return (
    (req.body && req.body._csrf) ||
    (req.query && req.query._csrf) ||
    req.headers["csrf-token"] ||
    req.headers["xsrf-token"] ||
    req.headers["x-csrf-token"] ||
    req.headers["x-xsrf-token"]
  );
}
