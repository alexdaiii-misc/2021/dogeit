import { getReasonPhrase, StatusCodes } from "http-status-codes";
import { NextApiResponse } from "next";
import { NextApiRequest } from "next";

/**
 * Converts the query param to an array
 * @param targets The query param to convert to an array
 * @param maxLength the max length of the array
 */
export default function queryToArray(targets: string[], maxLength: number) {
  return (req: NextApiRequest, res: NextApiResponse, next: Function) => {
    try {
      targets.forEach((value) => {
        const query = req?.query[value] as string;

        if (query) {
          const qryArray = query.split(",");

          if (qryArray.length > maxLength) {
            throw new Error("Client: Too many URI parameters");
          }

          req.query[value] = qryArray;
        }
      });
    } catch (error) {
      return res
        .status(StatusCodes.REQUEST_URI_TOO_LONG)
        .json({ msg: getReasonPhrase(StatusCodes.REQUEST_URI_TOO_LONG) });
    }

    return next();
  };
}
