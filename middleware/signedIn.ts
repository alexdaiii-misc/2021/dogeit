import { ENCODING_RADIX } from "./../lib/encodeConsts";
// typescript
import { RequestWithUser, userReq } from "../types/requestTypes";
import { NextApiResponse } from "next";

// lib
import { DogeitError, errorHandler } from "../lib/errors";
import { cookieOptions, setCookie } from "../lib/security/setCookie";
import {
  getJWTCookieName,
  getJWTSecret,
  issueJWT,
} from "../lib/security/jwtHelper";

// next
import { ReasonPhrases, StatusCodes } from "http-status-codes";
import jwt from "jsonwebtoken";
import { serialize } from "cookie";

// db
import { getUserFromSessionToken } from "../lib/queries/userSession";
import { stringToBigint } from "../lib/encode36";
import { getSessionCookieName } from "../lib/security/sessionHelper";

/**
 * Checks if user is signed in. Will check if the JWT is valid or not. If it isn't then,
 * check if the user has a session. If they have a session, then issue a JWT also.
 */
export default function signedIn() {
  return async (req: RequestWithUser, res: NextApiResponse, next: Function) => {
    try {
      if (!(await checkJWT(req, res)) && !(await checkSession(req, res))) {
        throw new DogeitError({
          msg: ReasonPhrases.UNAUTHORIZED,
          statusCode: StatusCodes.UNAUTHORIZED,
        });
      }

      verifyUserPublicId(req);

      return next();
    } catch (error) {
      return errorHandler(error, res);
    }
  };
}

/**
 * Verifies that the user public id is the correct data type
 */
function verifyUserPublicId(req: RequestWithUser) {
  // prevent the case the jwt signing key is released and someone issues a jwt with
  // this not being a bigint
  if (typeof req.user.public_id !== "bigint") {
    throw new Error("The user's public_id must be a BIGINT");
  }
}

/**
 * checks the jwt token to see if it is valid
 */
async function checkJWT(req: RequestWithUser, res: NextApiResponse) {
  const jwtCookie = req.cookies[getJWTCookieName()];

  const sessionCookie = req.cookies[getSessionCookieName()];

  // no jwt cookie
  if (!jwtCookie) {
    return false;
  }

  // jwt cookie is set but there is no session cookie
  if (!sessionCookie) {
    // set the jwt cookie to nothing
    deleteCookies(res);
    return false;
  }

  // jwt exists - check validity of the jwt
  try {
    const data: any = jwt.verify(jwtCookie, getJWTSecret());

    const user: userReq | undefined = data?.data?.user;

    if (!user) {
      throw { name: "UnknownDataPayload", data, user };
    }

    user.public_id = stringToBigint(
      user.public_id as unknown as string,
      ENCODING_RADIX
    );

    // let them pass and reissue the jwt
    req.user = user;

    issueJWT(user, res);

    return true;
  } catch (error: any) {
    if (error?.name === "TokenExpiredError") {
      return await checkSession(req, res);
    }

    if (error?.name === "UnknownDataPayload") {
      throw error;
    }

    // gave a jwt but can't be decoded
    throw new DogeitError({
      msg: ReasonPhrases.FORBIDDEN,
      statusCode: StatusCodes.UNAUTHORIZED,
    });
  }
}

/**
 * checks if a session is attached to the request and attaches the user to the request body.
 */
async function checkSession(req: RequestWithUser, res: NextApiResponse) {
  const sessionToken = req.cookies[getSessionCookieName()];

  // user has no session cookie set
  if (!sessionToken) {
    return false;
  }

  // user has a session cookie
  let result = await getUserFromSessionToken(sessionToken);

  if (!result) {
    // If sessionCookie was found set but it's not valid or expired then
    // remove the sessionCookie cookie from browser.
    deleteCookies(res);

    throw new DogeitError({
      msg: ReasonPhrases.UNAUTHORIZED,
      statusCode: StatusCodes.UNAUTHORIZED,
    });
  }

  let { sessions, ...user } = result;

  req.user = user;

  // didn't have a jwt or a valid jwt - reissue the jwt
  issueJWT(user, res);

  // reissue the same cookie
  const sessionCookie = serialize(getSessionCookieName(), sessionToken, {
    ...cookieOptions,
    httpOnly: true,
    expires: sessions[0].expires,
  });

  setCookie(res, sessionCookie);

  return true;
}

/**
 * Deletes all session and access token cookies
 */
function deleteCookies(res: NextApiResponse) {
  setCookie(
    res,
    serialize(getJWTCookieName(), "", { ...cookieOptions, maxAge: 0 })
  );

  setCookie(
    res,
    serialize(getSessionCookieName(), "", {
      ...cookieOptions,
      httpOnly: true,
      maxAge: 0,
    })
  );
}
