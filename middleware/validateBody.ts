import { StatusCodes } from "http-status-codes";
import { AnyObjectSchema } from "yup";
import { NextApiRequest, NextApiResponse } from "next";

/**
 * Validates that the request matches the given schema. Body must be exactly what is specified in schema and will return all errors to client.
 * @param schema The schema to validate against
 */
export default function validateBody(schema: AnyObjectSchema) {
  return async (req: NextApiRequest, res: NextApiResponse, next: Function) => {
    try {
      req.body = await schema.noUnknown(true).validate(req.body, {
        abortEarly: false,
        strict: true,
      });

      return next();
    } catch (error) {
      return res
        .status(StatusCodes.BAD_REQUEST)
        .json({ error, msg: "Request body failed validation" });
    }
  };
}
