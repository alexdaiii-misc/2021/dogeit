import { StatusCodes } from "http-status-codes";
import { object, string } from "yup";
import { NextApiResponse } from "next";
import { DogeitError, errorHandler } from "../lib/errors";
import latestCoinPrice from "../lib/handlers/latestCoinPrice";
import { RequestWithCrypto } from "../types/requestTypes";

/**
 * Validates that the post or comment is not too long. Max length is determined by price of dogecoin.
 * @param params body parameters to check against
 * @param crypto_price_id if given this argument, will use this crypto price in the post instead of the latest crypto price
 */
export default function validateSubmissionLength(params: string[]) {
  return async (
    req: RequestWithCrypto,
    res: NextApiResponse,
    next: Function
  ) => {
    try {
      // constructs the schema from params
      const schema = await makeSchema(req, params);

      req.body = await schema
        .validate(req.body, {
          abortEarly: false,
          strict: true,
        })
        .catch((error) => {
          throw new DogeitError({
            blob: error,
            msg: "Submission too long",
            statusCode: StatusCodes.BAD_REQUEST,
          });
        });

      return next();
    } catch (error) {
      return errorHandler(error, res);
    }
  };
}

/**
 * Constructs a schema to validate against
 * @param req the request. The request header must have the crypto price already appended
 * @param current_price the price of doge
 * @param params the parameters that need length avlidation against the current price
 * @returns a yup validation schema object
 */
async function makeSchema(req: RequestWithCrypto, params: string[]) {
  // gets the latest crypto price
  const coin = await latestCoinPrice("dogecoin", false);

  req.crypto_price = coin as any;

  const COINS_IN_DOLLAR = 100;
  let spec: any = {};

  const maxLen = Math.round((coin.current_price as any) * COINS_IN_DOLLAR);

  params.forEach((value) => {
    spec[value] = string().required().max(maxLen);
  });

  return object(spec);
}
