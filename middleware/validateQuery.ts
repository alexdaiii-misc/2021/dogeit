import { StatusCodes } from "http-status-codes";
import { AnyObjectSchema } from "yup";
import { NextApiRequest, NextApiResponse } from "next";
import { ONLY_INT_REGEX } from "../lib/server-const/schemaConsts";

/**
 * Validates that the request query matches the given schema. Query input types must be exactly what is specified in schema and will return all errors to client.
 * @param schema The schema to validate against
 */
export default function validateQuery(schema: AnyObjectSchema, route: string) {
  return async (req: NextApiRequest, res: NextApiResponse, next: Function) => {
    try {
      parseIntQueries(req, schema);

      // let queries: any = {};

      const uriSplit = route.split("/");

      // delete req params from the req query
      uriSplit.forEach((ele) => {
        if (ele.includes(":")) {
          delete req.query[ele.substr(1)];
        }
      });

      req.query = await schema.noUnknown(true).validate(req.query, {
        abortEarly: false,
        strict: true,
      });

      //   // This will automatically get rid of unknown queries
      //   // However, it now won't throw error with unknown queries
      //   Object.keys(schema.fields).forEach((query) => {
      //     queries[query] = req.query[query];
      //   });

      //   req.query = await schema.noUnknown(true).validate(queries, {
      //     abortEarly: false,
      //     strict: true,
      //   });
      // }

      const defaults = schema.getDefault();

      // automatically replaces all unknown values with default values if undefined
      for (const key in defaults) {
        if (defaults[key] !== undefined && req.query[key] === undefined) {
          req.query[key] = defaults[key];
        }
      }

      return next();
    } catch (error) {
      return res
        .status(StatusCodes.BAD_REQUEST)
        .json({ blob: error, msg: "User query failed validation." });
    }
  };
}

/**
 * Convert the request query fields that are supposed to be numbers into numbers
 */
function parseIntQueries(req: NextApiRequest, schema: AnyObjectSchema) {
  for (const key in schema.fields) {
    try {
      const query = req.query[key] as string;

      // regex to maych string numbers to convert
      if (schema.fields[key].type === "number" && query.match(ONLY_INT_REGEX)) {
        const converted = Number(query) as any;
        if (!isNaN(converted)) {
          req.query[key] = converted;
        }
      }
    } catch {
      // cant convert the number - keep it a string
    }
  }
}
