import { NextApiRequest, NextApiResponse } from "next";
import xxs from "xss";

/**
 * Sanatizes the request body. All strings in the request body are put through the xxs sanatizer.
 * NOTE: should be placed after the validateBody middleware
 * @param schema The schema to validate against
 */
export default function xxsSanatize(
  req: NextApiRequest,
  _res: NextApiResponse,
  next: Function
) {
  // not really necessary with react since they run all string through this xxs lib also
  for (const key in req.body) {
    if (typeof req.body[key] === "string") {
      req.body[key] = xxs(req.body[key]);
    }
  }

  return next();
}
