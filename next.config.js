// This will use webpack 4 - only use this to analyze
// run npm run analyze
// const withBundleAnalyzer = require("@next/bundle-analyzer")({
//   enabled: process.env.ANALYZE === "true",
// });

// // next.config.js
// module.exports = withBundleAnalyzer({
//   images: {
//     domains: ["avatars.githubusercontent.com"],
//   },
//   poweredByHeader: false,
// });

module.exports = {
  images: {
    domains: ["avatars.githubusercontent.com"],
  },
  poweredByHeader: false,
};
