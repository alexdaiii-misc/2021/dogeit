# This only needs to be run once to generate a ssl certificate 
# NOTE: THIS IS ONLY FOR TESTING - DO NOT USE A SELF SIGNED SSL CERTIFICATE IN PRODUCTION
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./ssl/nginx-selfsigned.key -out ./ssl/nginx-selfsigned.crt