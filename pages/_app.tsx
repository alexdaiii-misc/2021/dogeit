import { Provider } from "next-auth/client";

interface MyProps {
  Component: any;
  pageProps: any;
  // any other props that come into the component
}

export default function App({ Component, pageProps }: MyProps) {
  return (
    <Provider session={pageProps.session}>
      <Component {...pageProps} />
    </Provider>
  );
}
