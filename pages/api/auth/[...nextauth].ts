import NextAuth from "next-auth";
import Providers from "next-auth/providers";
import processEnv from "../../../lib/security/processEnv";

const secureCookieOptions = {
  httpOnly: true,
  sameSite: "lax" as "lax",
  path: "/",
  secure: processEnv.secureCookies,
};

export default NextAuth({
  // Configure one or more authentication providers
  providers: [
    Providers.GitHub({
      clientId: process.env.GITHUB_ID,
      clientSecret: process.env.GITHUB_SECRET,
    }),
    // ...add more providers here
  ],
  // A database is optional, but required to persist accounts in a database
  database: process.env.DATABASE_URL,
  secret: process.env.COOKIE_SECRET,
  cookies: {
    csrfToken: {
      name: processEnv.csrfTokenName,
      options: {
        httpOnly: true,
        sameSite: "lax",
        path: "/",
        secure: processEnv.secureCookies,
      },
    },
    sessionToken: {
      name: processEnv.sessionTokenName,
      options: {
        ...secureCookieOptions,
      },
    },
    callbackUrl: {
      name: processEnv.callbackUrlTokenName,
      options: {
        httpOnly: false,
        sameSite: "lax",
        path: "/",
        secure: processEnv.secureCookies,
      },
    },
    pkceCodeVerifier: {
      name: processEnv.pkceTokenName,
      options: {
        ...secureCookieOptions,
      },
    },
  },
});
