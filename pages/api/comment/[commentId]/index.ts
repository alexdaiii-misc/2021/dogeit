// next http
import { StatusCodes } from "http-status-codes";
import nc from "next-connect";

// ts
import { NextApiResponse } from "next";
import { onNoMatch } from "../../../../lib/utils";
import { NextApiRequestWithParams } from "../../../../types/requestTypes";
import { deletePostSchemaType } from "../../../../types/schemas/postSchemas";

// lib
import { commentIndiv } from "../../../../lib/server-const/routeNames";
import { encode36 } from "../../../../lib/encode36";
import {
  COMMENT_PARAMS_TO_ENCODE,
  ENCODING_RADIX,
} from "../../../../lib/encodeConsts";
import { DogeitError, errorHandler } from "../../../../lib/errors";
import { DeleteComment } from "../../../../lib/handlers/deleteRecord/DeleteComment";

// middleware
import validateBody from "../../../../middleware/validateBody";
import signedIn from "../../../../middleware/signedIn";
import { CommentIdFromURI } from "../../../../lib/attachId/commentId";
import csrfProtection from "../../../../middleware/csrfProtection";
import prisma from "../../../../lib/prisma";

// schemas
import { deletePostSchema } from "../../../../schemas/postSchemas";

const handler = nc<NextApiRequestWithParams, NextApiResponse>({
  attachParams: true,
  onNoMatch: onNoMatch,
});

handler.use(csrfProtection());

/**
 * Route: "api/comment/:commentId"
 * Method: GET,
 * Desc: Gets an individual comment
 */
handler.get(commentIndiv, async (req, res) => {
  try {
    const commentId = new CommentIdFromURI(req).getId();

    let comment = await getComment(commentId);

    encode36(comment, COMMENT_PARAMS_TO_ENCODE);

    return res.status(StatusCodes.OK).json({ comment });
  } catch (error) {
    return errorHandler(error, res);
  }
});

/**
 * Route: "api/comment/:comemntid"
 * Method: DELETE,
 * Desc: Deletes the user's comment. Only user, moderators of rocket, siteadmin can delete the comment
 * By default:
 * - mods and user mark the comment for deletion
 * - admins can actually remove post
 */
handler.delete(
  commentIndiv,
  validateBody(deletePostSchema),
  signedIn(),
  async (req, res) => {
    try {
      const action = (req.body as deletePostSchemaType).action;

      const deleteComment = new DeleteComment(req, action);
      await deleteComment.run();
      const comment = deleteComment.getRecord();

      return res.status(StatusCodes.OK).json({ comment });
    } catch (error) {
      return errorHandler(error, res);
    }
  }
);

export default handler;

/**
 * Returns the comment
 */
async function getComment(id: bigint) {
  const comment = await prisma.comments.findUnique({ where: { id } });

  if (!comment) {
    throw new DogeitError({
      msg: `Comment ${id.toString(ENCODING_RADIX)} not found`,
      statusCode: StatusCodes.NOT_FOUND,
    });
  }

  return comment;
}
