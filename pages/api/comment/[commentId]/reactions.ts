// http next
import nc from "next-connect";
import { StatusCodes } from "http-status-codes";

// lib
import { onNoMatch } from "../../../../lib/utils";
import { commentIndiv } from "../../../../lib/server-const/routeNames";
import { CommentIdFromURI } from "./../../../../lib/attachId/commentId";

// middleware
import csrfProtection from "../../../../middleware/csrfProtection";

// ts
import { NextApiRequestWithParams } from "../../../../types/requestTypes";
import { NextApiResponse } from "next";

// db
import prisma from "../../../../lib/prisma";
import { disconnectAndPropogateError } from "../../../../lib/queries/disconnect";
import { errorHandler } from "../../../../lib/errors";

const handler = nc<NextApiRequestWithParams, NextApiResponse>({
  attachParams: true,
  onNoMatch: onNoMatch,
});

handler.use(csrfProtection());

const ROUTE = `${commentIndiv}/reactions`;

/**
 * Route: "api/comment/[commentId]/reactions"
 * Method: GET,
 * Desc: Get the reactions to a comment
 */
handler.get(ROUTE, async (req, res) => {
  try {
    const commentId = new CommentIdFromURI(req).getId();

    const reactions = await getReactionToComment(commentId);

    return res.status(StatusCodes.OK).json({ reactions });
  } catch (error) {
    return errorHandler(error, res);
  }
});

export default handler;

async function getReactionToComment(comment_id: bigint) {
  return await prisma.reaction
    .groupBy({
      by: ["reaction"],
      _count: { comment_id: true },
      where: { comment_id },
    })
    .catch(disconnectAndPropogateError);
}
