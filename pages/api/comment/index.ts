// schema
import { newCommentSchema } from "../../../schemas/commentsSchema";

// ts
import { NextApiResponse } from "next";
import {
  RequestWithCrypto,
  RequestWithUser,
} from "../../../types/requestTypes";
import { newCommentType } from "../../../types/schemas/commentsSchemas";

// next http
import nc from "next-connect";
import { StatusCodes } from "http-status-codes";

// lib
import {
  COMMENT_PARAMS_TO_ENCODE,
  POST_PARAMS_TO_ENCODE,
} from "../../../lib/encodeConsts";
import { onNoMatch } from "../../../lib/utils";
import {
  encode36,
  generateLatestId,
  validateInputString,
} from "../../../lib/encode36";
import { DogeitError, errorHandler } from "../../../lib/errors";
import { COMMENT_BODY_CHK_LEN } from "../../../lib/server-const/schemaConsts";

// middleware
import validateBody from "../../../middleware/validateBody";
import signedIn from "../../../middleware/signedIn";
import validateSubmissionLength from "../../../middleware/validatePostLength";
import xxsSanatize from "../../../middleware/xxsSanatize";
import dbErrors from "../../../lib/queries/dbError/dbUserThrownErrors";
import prisma from "../../../lib/prisma";
import {
  returnedCommentDetails,
  returnedPostDetails,
} from "../../../lib/queries/qryReturnVals";
import csrfProtection from "../../../middleware/csrfProtection";

const handler = nc<RequestWithCrypto, NextApiResponse>({
  attachParams: true,
  onNoMatch: onNoMatch,
});

handler.use(csrfProtection());

/**
 * Route: "api/comment"
 * Method: POST,
 * Desc: Creates a new comment
 */
handler.post(
  validateBody(newCommentSchema),
  validateSubmissionLength(COMMENT_BODY_CHK_LEN),
  signedIn(),
  xxsSanatize,
  async (req, res) => {
    try {
      const { postId, parentId } = decodeIds(req.body);

      const post = await createNewComment(req, postId, parentId, req.body.body);

      return res.status(StatusCodes.CREATED).json({ post });
    } catch (error) {
      return errorHandler(error, res, {
        prisma: { msg: `Post ${req.body.post_id} does not exist` },
      });
    }
  }
);

export default handler;

/**
 * Converts the strings into bigints or null
 */
function decodeIds(data: newCommentType) {
  const postId = validateInputString(data.post_id);

  const parentId = data.parent_id ? validateInputString(data.parent_id) : null;

  return { postId, parentId };
}

async function createNewComment(
  req: RequestWithCrypto & RequestWithUser,
  post_id: bigint,
  parent_id: bigint | null,
  body: string
) {
  const newCommentId = generateLatestId();

  try {
    let post = await prisma.posts.update({
      select: {
        ...returnedPostDetails,
        comments: {
          select: returnedCommentDetails,
          where: { id: newCommentId },
        },
      },
      data: {
        comments: {
          create: [
            {
              id: newCommentId,
              crypto_prices_id: req.crypto_price.id,
              user_id: req.user.public_id,
              body,
              parent_id,
              root_id: parent_id ? parent_id : undefined,
            },
          ],
        },
      },
      where: {
        id: post_id,
      },
    });

    encode36(post, POST_PARAMS_TO_ENCODE);
    encode36(post.comments[0], COMMENT_PARAMS_TO_ENCODE);

    return post;
  } catch (error: any) {
    if (error?.stack) {
      getErrorFromStackTrace(error);
    }

    throw await dbErrors(error);
  }
}

/**
 * Handler for getting error msg from error stack trace
 */
function getErrorFromStackTrace(error: any) {
  console.error(error);

  const dbError = getDatabaseError(error);

  switch (dbError) {
    case '"Parent post does not exist"':
      throw new DogeitError({
        msg: dbError,
        statusCode: StatusCodes.NOT_FOUND,
      });

    case '\\"Not a moderator of doge\\"':
      throw new DogeitError({
        msg: dbError,
        statusCode: StatusCodes.FORBIDDEN,
      });

    case '"Child post must be the same as parent post"':
      throw new DogeitError({
        msg: dbError,
        statusCode: StatusCodes.BAD_REQUEST,
      });

    default:
      break;
  }
}

/**
 * Extract the database error message from error stack trace
 */
function getDatabaseError(error: any) {
  // n is the length of the string
  // split it into arrays - op is O(n) which is a little faster than
  // O(mn) when using string.includes()
  const stackTrace = (error.stack as string).split(", ");

  // another O(n) operation
  const splitted = stackTrace.map((value) => {
    return value.split(": ");
  });

  for (let i = 0; i < splitted.length; ++i) {
    if (splitted[i].includes("message")) {
      console.error(splitted[i]);
      return splitted[i][splitted[i].length - 1];
    }
  }
}
