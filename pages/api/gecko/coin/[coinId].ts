// lib
import { coinRoute } from "../../../../lib/server-const/routeNames";
import { onNoMatch } from "../../../../lib/utils";
import { errorHandler } from "../../../../lib/errors";
import latestCoinPrice from "../../../../lib/handlers/latestCoinPrice";

// ts
import { NextApiResponse } from "next";
import { NextApiRequestWithParams } from "../../../../types/requestTypes";

// next
import nc from "next-connect";
import { StatusCodes } from "http-status-codes";

// middleware
import csrfProtection from "../../../../middleware/csrfProtection";

const handler = nc<NextApiRequestWithParams, NextApiResponse>({
  attachParams: true,
  onNoMatch: onNoMatch,
});

handler.use(csrfProtection());

// TODO: cache result

/**
 * Route: "api/gecko/coin/:coinId"
 * Method: GET,
 * Desc: Returns latest information about the cryptocurrecny stored in db.
 */
handler.get(coinRoute, async (req, res) => {
  const coin: string = req.params.coinId;

  try {
    return res
      .status(StatusCodes.OK)
      .json({ coin: await latestCoinPrice(coin) });
  } catch (error) {
    return errorHandler(error, res);
  }
});

export default handler;
