// lib
import { historicalPriceRoute } from "../../../../lib/server-const/routeNames";
import { onNoMatch } from "../../../../lib/utils";
import { DogeitError, errorHandler } from "../../../../lib/errors";
import { encode36 } from "../../../../lib/encode36";

// next
import nc from "next-connect";
import { ReasonPhrases, StatusCodes } from "http-status-codes";

// ts
import { NextApiResponse } from "next";
import { HistoricCryptoId } from "../../../../lib/attachId/cryptoId";
import { NextApiRequestWithParams } from "../../../../types/requestTypes";

// db
import prisma from "../../../../lib/prisma";
import { disconnectAndPropogateError } from "../../../../lib/queries/disconnect";

// middleware
import csrfProtection from "../../../../middleware/csrfProtection";

const handler = nc<NextApiRequestWithParams, NextApiResponse>({
  attachParams: true,
  onNoMatch: onNoMatch,
});

handler.use(csrfProtection());

// TODO: cache result

handler.get(historicalPriceRoute, async (req, res) => {
  try {
    const historicId = new HistoricCryptoId(req).getId();

    const coin = await getHistoricCryptoPriceFromId(historicId);

    return res.status(StatusCodes.OK).json({ coin });
  } catch (error) {
    return errorHandler(error, res);
  }
});

export default handler;

/**
 * Returns the crypto price from the id of the timestamp
 * @param id the id of the price
 */
export async function getHistoricCryptoPriceFromId(id: bigint) {
  const price = await prisma.crypto_prices
    .findUnique({ where: { id } })
    .catch(disconnectAndPropogateError);

  if (!price) {
    throw new DogeitError({
      msg: ReasonPhrases.NOT_FOUND,
      statusCode: StatusCodes.NOT_FOUND,
    });
  }

  encode36(price, ["id"]);
  encode36(price, ["total_volume"], 10);

  return price;
}
