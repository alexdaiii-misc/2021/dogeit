// next js
import nc from "next-connect";

// lib fns
import { onNoMatch } from "../../../lib/utils";
import { paginatedResult } from "../../../lib/handlers/paginatedResult";
import MyComments from "../../../lib/paginatedClass/MyComments";

// middleware
import csrfProtection from "../../../middleware/csrfProtection";
import signedIn from "../../../middleware/signedIn";
import validateQuery from "../../../middleware/validateQuery";

// ts
import { NextApiRequestWithParams } from "../../../types/requestTypes";
import { NextApiResponse } from "next";

// schema
import { paginatedQuery } from "../../../schemas/paginatedQuerySchema";

const handler = nc<NextApiRequestWithParams, NextApiResponse>({
  attachParams: true,
  onNoMatch,
});

handler.use(csrfProtection());

const ROUTE = `me/comments`;

/**
 * Route: "api/me/comments"
 * Method: GET,
 * Desc: Returns comments of logged in user. Cursor paganated using the comment id as the cursor.
 *
 * Auth: signed in
 */
handler.get(
  signedIn(),
  validateQuery(paginatedQuery(), ROUTE),
  async (req, res) => {
    return await paginatedResult(req, res, MyComments);
  }
);

export default handler;
