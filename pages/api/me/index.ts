// next js
import nc from "next-connect";
import { StatusCodes } from "http-status-codes";

// lib functions
import { onNoMatch } from "../../../lib/utils";
import { errorHandler } from "../../../lib/errors";
import { encode36 } from "../../../lib/encode36";
import { USER_PARAMS_TO_ENCODE } from "../../../lib/encodeConsts";

// middleware
import validateBody from "../../../middleware/validateBody";
import csrfProtection from "../../../middleware/csrfProtection";
import xxsSanatize from "../../../middleware/xxsSanatize";
import signedIn from "../../../middleware/signedIn";

// yup schemas
import { patchUserSchema } from "../../../schemas/userSchema";

// typescript
import { RequestWithSession } from "../../../types/requestTypes";
import { NextApiResponse } from "next";
import { issueJWT } from "../../../lib/security/jwtHelper";

// db
import prisma from "../../../lib/prisma";
import { returnedUserDetails } from "../../../lib/queries/qryReturnVals";
import { disconnectAndPropogateError } from "../../../lib/queries/disconnect";

const handler = nc<RequestWithSession, NextApiResponse>({
  attachParams: true,
  onNoMatch,
});

handler.use(csrfProtection());

/**
 * Route: "api/me"
 * Method: GET,
 * Desc: Returns information about the logged in user
 * Reddit API eq: /api/me
 *
 * Auth: signed in
 */
handler.get(signedIn(), async (req, res) => {
  try {
    const user = req.user;
    encode36(user, USER_PARAMS_TO_ENCODE);

    return res.status(StatusCodes.OK).json({ user });
  } catch (error) {
    return errorHandler(error, res);
  }
});

/**
 * Route: "api/me"
 * Method: PATCH
 * Desc: Updates the information of the logged in user
 *
 * Auth: signed in
 */
handler.patch(
  validateBody(patchUserSchema),
  xxsSanatize,
  signedIn(),
  async (req, res) => {
    try {
      let user = await updateUser(req.user.public_id, req.body);

      // need to update the jwt
      issueJWT(user, res);
      encode36(user, USER_PARAMS_TO_ENCODE);

      return res.status(StatusCodes.OK).json({ user });
    } catch (error: any) {
      return errorHandler(error, res, {
        prisma: {
          msg: "Username or email already taken",
          meta: error.meta,
          statusCode: StatusCodes.CONFLICT,
        },
      });
    }
  }
);

export default handler;

type userInfo = {
  username?: string;
  name?: string;
  email?: string;
  image?: string;
};

/**
 * Updates the user given the user id with the updated user information
 * @param userId the user logged in to update
 * @param userInfo The user information to update
 */
async function updateUser(public_id: bigint, userInfo: userInfo = {}) {
  return await prisma.users
    .update({
      where: {
        public_id,
      },
      data: {
        ...userInfo,
      },
      select: {
        ...returnedUserDetails,
        updated_at: true,
      },
    })
    .catch(disconnectAndPropogateError);
}
