// next js
import { StatusCodes } from "http-status-codes";
import nc from "next-connect";

// lib functions
import { errorHandler } from "../../../lib/errors";
import { onNoMatch } from "../../../lib/utils";

// middleware
import csrfProtection from "../../../middleware/csrfProtection";
import signedIn from "../../../middleware/signedIn";

// typescript
import { RequestWithSession } from "../../../types/requestTypes";
import { NextApiResponse } from "next";

// db
import prisma from "../../../lib/prisma";
import { disconnectAndPropogateError } from "../../../lib/queries/disconnect";

const handler = nc<RequestWithSession, NextApiResponse>({
  attachParams: true,
  onNoMatch,
});

handler.use(csrfProtection());

/**
 * Route: "api/me/invitations"
 * Method: GET,
 * Desc: Returns all invitations of logged in user
 * Reddit API eq: /api/mod/conversations/:conversation_id
 *
 * Auth: signedIn
 */
handler.get(signedIn(), async (req, res) => {
  try {
    const invitations = await getUserInvitations(req.user.public_id);

    return res.status(StatusCodes.OK).json({ invitations });
  } catch (error) {
    return errorHandler(error, res);
  }
});

export default handler;

/**
 * Gets the invitations of a logged in user
 * @param public_id
 * @returns the invitation
 */
async function getUserInvitations(user_id: bigint) {
  return await prisma.invitations
    .findMany({
      select: {
        created_at: true,
        rockets: { select: { name: true } },
        users_invitations_inviterTousers: { select: { username: true } },
      },
      where: {
        user_id,
      },
    })
    .catch(disconnectAndPropogateError);
}
