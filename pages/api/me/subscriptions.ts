// next
import nc from "next-connect";

// lib
import { errorHandler } from "../../../lib/errors";
import { OK } from "../../../lib/httpStatusCodeWrapper";
import { onNoMatch } from "../../../lib/utils";

// middleware
import csrfProtection from "../../../middleware/csrfProtection";
import signedIn from "../../../middleware/signedIn";

// db
import prisma from "../../../lib/prisma";
import { disconnectAndPropogateError } from "../../../lib/queries/disconnect";

// ts
import { NextApiRequest, NextApiResponse } from "next";

const handler = nc<NextApiRequest, NextApiResponse>({
  attachParams: true,
  onNoMatch,
});

handler.use(csrfProtection());

/**
 * Route: "api/me/subscriptions"
 * Method: GET,
 * Desc: Returns all rockets user is subscribed to
 *
 * CLIENT: cache or store this inside local db?
 *
 * Auth: signed in
 */
handler.get(signedIn(), async (req, res) => {
  try {
    const subscriptions = await getSubscribedRockets(req.user.public_id);

    return res.status(OK).json({ subscriptions });
  } catch (error) {
    return errorHandler(error, res);
  }
});

export default handler;

/**
 * Returns the rockets of the currently logged in user
 * @param userId public_id of the user
 */
async function getSubscribedRockets(user_id: bigint) {
  return await prisma.subscribers
    .findMany({
      select: {
        auth_level: true,
        rockets: {
          select: {
            name: true,
          },
        },
      },
      where: {
        user_id,
      },
    })
    .catch(disconnectAndPropogateError);
}
