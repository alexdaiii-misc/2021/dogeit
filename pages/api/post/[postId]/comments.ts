import { postIndiv } from "../../../../lib/server-const/routeNames";
import { paginatedQuery } from "../../../../schemas/paginatedQuerySchema";
import { NextApiRequest, NextApiResponse } from "next";
import nc from "next-connect";
import validateQuery from "../../../../middleware/validateQuery";
import { paginatedResult } from "../../../../lib/handlers/paginatedResult";
import PostCommentsPage from "../../../../lib/paginatedClass/PostCommentsPage";
import csrfProtection from "../../../../middleware/csrfProtection";

const handler = nc<NextApiRequest, NextApiResponse>({ attachParams: true });

const ROUTE = `${postIndiv}/comments`;

handler.use(csrfProtection());

/**
 * Route: "api/post/:postid/comments"
 * Method: GET,
 * Desc: Gets the comments of a post. Paginated using pointers
 */
handler.get(
  ROUTE,
  validateQuery(paginatedQuery({ min_count: 5, max_count: 10 }), ROUTE),
  async (req, res) => {
    return await paginatedResult(req, res, PostCommentsPage);
  }
);

export default handler;
