import { DeletePost } from "./../../../../lib/handlers/deleteRecord/DeletePost";
// schema
import { deletePostSchema } from "../../../../schemas/postSchemas";

// ts
import { NextApiResponse } from "next";
import { NextApiRequestWithParams } from "../../../../types/requestTypes";
import { deletePostSchemaType } from "../../../../types/schemas/postSchemas";

// lib
import { onNoMatch } from "../../../../lib/utils";
import { encode36 } from "../../../../lib/encode36";
import { postIndiv } from "../../../../lib/server-const/routeNames";
import {
  POST_PARAMS_TO_ENCODE,
  ENCODING_RADIX,
} from "../../../../lib/encodeConsts";
import { errorHandler, DogeitError } from "../../../../lib/errors";
import { PostIdFromURI } from "../../../../lib/attachId/postId";

// middleware
import signedIn from "../../../../middleware/signedIn";
import validateBody from "../../../../middleware/validateBody";
import csrfProtection from "../../../../middleware/csrfProtection";

// http next
import nc from "next-connect";
import { StatusCodes } from "http-status-codes";

// db
import prisma from "../../../../lib/prisma";
import { disconnectAndPropogateError } from "../../../../lib/queries/disconnect";

const handler = nc<NextApiRequestWithParams, NextApiResponse>({
  attachParams: true,
  onNoMatch: onNoMatch,
});

handler.use(csrfProtection());

// TODO: cache

const ROUTE = `${postIndiv}`;

/**
 * Route: "api/post/:postId"
 * Method: GET,
 * Desc: Gets an individual post
 */
handler.get(ROUTE, async (req, res) => {
  try {
    const postId = new PostIdFromURI(req).getId();

    let { rocket_id, ...post } = await getPost(postId);

    encode36(post, POST_PARAMS_TO_ENCODE);

    return res.status(StatusCodes.OK).json({ post });
  } catch (error) {
    errorHandler(error, res);
  }
});

/**
 * Route: "api/post/:postId"
 * Method: DELETE,
 * Desc: Deletes the user's post. Only user, moderators of rocket, siteadmin can delete the post
 *
 * Moderators and siteadmin can fully delete post
 */
handler.delete(
  ROUTE,
  validateBody(deletePostSchema),
  signedIn(),
  async (req, res) => {
    try {
      const action = (req.body as deletePostSchemaType).action;

      const deletePost = new DeletePost(req, action);
      await deletePost.run();
      const posts = deletePost.getRecord();

      return res.status(StatusCodes.OK).json({ posts });
    } catch (error) {
      return errorHandler(error, res);
    }
  }
);

export default handler;

/**
 * Gets post from id
 */
async function getPost(id: bigint) {
  const post = await prisma.posts
    .findUnique({
      where: { id },
      include: { rockets: { select: { name: true } } },
    })
    .catch(disconnectAndPropogateError);

  if (!post) {
    throw new DogeitError({
      msg: `Post ${id.toString(ENCODING_RADIX)} not found`,
      statusCode: StatusCodes.NOT_FOUND,
    });
  }

  return post;
}
