// http next
import nc from "next-connect";
import { StatusCodes } from "http-status-codes";

// lib
import { onNoMatch } from "../../../../lib/utils";
import { postIndiv } from "../../../../lib/server-const/routeNames";

// middleware
import csrfProtection from "../../../../middleware/csrfProtection";

// ts
import { NextApiRequestWithParams } from "../../../../types/requestTypes";
import { NextApiResponse } from "next";

// db
import prisma from "../../../../lib/prisma";
import { disconnectAndPropogateError } from "../../../../lib/queries/disconnect";
import { errorHandler } from "../../../../lib/errors";
import { PostIdFromURI } from "../../../../lib/attachId/postId";

const handler = nc<NextApiRequestWithParams, NextApiResponse>({
  attachParams: true,
  onNoMatch: onNoMatch,
});

handler.use(csrfProtection());

const ROUTE = `${postIndiv}/reactions`;

/**
 * Route: "api/post/[postId]/reactions"
 * Method: GET,
 * Desc: Get the reactions to a post
 */
handler.get(ROUTE, async (req, res) => {
  try {
    const postId = new PostIdFromURI(req).getId();

    const reactions = await getReactionToPost(postId);

    return res.status(StatusCodes.OK).json({ reactions });
  } catch (error) {
    return errorHandler(error, res);
  }
});

export default handler;

async function getReactionToPost(post_id: bigint) {
  return await prisma.reaction
    .groupBy({
      by: ["reaction"],
      _count: { post_id: true },
      where: { post_id },
    })
    .catch(disconnectAndPropogateError);
}
