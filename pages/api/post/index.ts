import { POST_PARAMS_TO_ENCODE } from "./../../../lib/encodeConsts";
// http next
import nc from "next-connect";
import { StatusCodes } from "http-status-codes";

// middleware
import xxsSanatize from "../../../middleware/xxsSanatize";
import validateSubmissionLength from "../../../middleware/validatePostLength";
import signedIn from "../../../middleware/signedIn";
import validateBody from "../../../middleware/validateBody";
import csrfProtection from "../../../middleware/csrfProtection";
import validateQuery from "../../../middleware/validateQuery";

// schemas
import { newPostSchema } from "../../../schemas/postSchemas";
import { paginatedQuery } from "../../../schemas/paginatedQuerySchema";

// lib
import { DogeitError, errorHandler } from "../../../lib/errors";
import { POST_BODY_CHK_LEN } from "../../../lib/server-const/schemaConsts";
import { onNoMatch } from "../../../lib/utils";
import { encode36, generateLatestId } from "../../../lib/encode36";

// ts
import {
  RequestWithCrypto,
  RequestWithUser,
} from "../../../types/requestTypes";
import { newPostSchemaType } from "../../../types/schemas/postSchemas";
import { NextApiResponse } from "next";

// db
import prisma from "../../../lib/prisma";
import dbErrors from "../../../lib/queries/dbError/dbUserThrownErrors";
import { paginatedResult } from "../../../lib/handlers/paginatedResult";
import NewPosts from "../../../lib/paginatedClass/NewPosts";

const ROUTE = "/api/post";

const handler = nc<RequestWithCrypto, NextApiResponse>({
  attachParams: true,
  onNoMatch: onNoMatch,
});

handler.use(csrfProtection());

/**
 * Route: "api/post"
 * Method: POST,
 * Desc: Creates a new post
 *
 * Auth: signed in
 */
handler.post(
  validateBody(newPostSchema),
  signedIn(),
  validateSubmissionLength(POST_BODY_CHK_LEN),
  xxsSanatize,
  async (req, res) => {
    try {
      const rocket = await createNewPost(req, req.body);

      encode36(rocket.posts[0], POST_PARAMS_TO_ENCODE);

      return res.status(StatusCodes.CREATED).json({ rocket });
    } catch (error: any) {
      let options = {} as any;

      if (error?.code === "P2025") {
        options = {
          prisma: {
            msg: `Rocket ${req.body.rocket} does not exist.`,
          },
        };
      }

      return errorHandler(error, res, options);
    }
  }
);

// TODO: cache result

/**
 * Route: "api/post"
 * Method: GET,
 * Desc: Route to get the hottest and top posts.
 */
handler.get(ROUTE, validateQuery(paginatedQuery(), ROUTE), async (req, res) => {
  return await paginatedResult(req, res, NewPosts);
});

export default handler;

async function createNewPost(
  req: RequestWithCrypto & RequestWithUser,
  data: newPostSchemaType
) {
  const newPostId = generateLatestId();

  try {
    return await prisma.rockets.update({
      select: {
        name: true,
        posts: {
          where: {
            id: newPostId,
          },
        },
      },
      data: {
        posts: {
          create: [
            {
              id: newPostId,
              title: data.title,
              body: data.body,
              crypto_prices_id: req.crypto_price.id,
              user_id: req.user.public_id,
            },
          ],
        },
      },
      where: {
        name: data.rocket,
      },
    });
  } catch (error: any) {
    if (
      error?.stack &&
      (error.stack as string).includes('code: SqlState("P0001")')
    ) {
      console.error(error);

      throw new DogeitError({
        msg: `Rocket ${data.rocket} has restricted posting`,
        statusCode: StatusCodes.FORBIDDEN,
      });
    }

    throw await dbErrors(error);
  }
}
