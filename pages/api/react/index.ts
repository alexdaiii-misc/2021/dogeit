// http next
import nc from "next-connect";

// lib
import { onNoMatch } from "../../../lib/utils";
import { errorHandler } from "../../../lib/errors";

// middleware
import csrfProtection from "../../../middleware/csrfProtection";

// schemas
import {
  reactToRecordSchema,
  getReactionSchema,
} from "../../../schemas/reactSchema";

// ts
import { NextApiResponse } from "next";
import { NextApiRequestWithParams } from "../../../types/requestTypes";
import validateBody from "../../../middleware/validateBody";
import { reactionHandler } from "../../../lib/handlers/reactionHandler";
import signedIn from "../../../middleware/signedIn";
import validateQuery from "../../../middleware/validateQuery";

const handler = nc<NextApiRequestWithParams, NextApiResponse>({
  attachParams: true,
  onNoMatch: onNoMatch,
});

handler.use(csrfProtection());

/**
 * Route: "api/react"
 * Method: POST,
 * Desc: Create or replace reaction to a post or comment
 */
handler.post(
  validateBody(reactToRecordSchema),
  signedIn(),
  async (req, res) => {
    try {
      return await reactionHandler("POST", req, res, req.body);
    } catch (error) {
      return errorHandler(error, res);
    }
  }
);

const ROUTE = "react";

/**
 * Route: "api/react"
 * Method: GET,
 * Desc: Get a user reaction to a post or comment
 */
handler.get(
  validateQuery(getReactionSchema, ROUTE),
  signedIn(),
  async (req, res) => {
    try {
      return await reactionHandler("GET", req, res, req.query as any);
    } catch (error) {
      return errorHandler(error, res);
    }
  }
);

export default handler;
