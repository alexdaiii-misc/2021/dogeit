// next
import nc from "next-connect";
import { StatusCodes } from "http-status-codes";

// lib
import { onNoMatch } from "../../../lib/utils";
import { MAX_POSTS_PER_PAGE } from "../../../lib/server-const/schemaConsts";
import { errorHandler } from "../../../lib/errors";
import { encode36Array, stringArrToBigInts } from "../../../lib/encode36";

// middleware
import csrfProtection from "../../../middleware/csrfProtection";
import validateQuery from "../../../middleware/validateQuery";
import queryToArray from "../../../middleware/queryToArray";

// schemas
import { getMultiReactionsSchema } from "./../../../schemas/reactSchema";

// ts
import { NextApiRequestWithParams } from "../../../types/requestTypes";
import { NextApiResponse } from "next";
import { getMultiReactionsSchemaType } from "../../../types/schemas/reactSchemas";

// db
import prisma from "../../../lib/prisma";
import { disconnectAndPropogateError } from "../../../lib/queries/disconnect";

const handler = nc<NextApiRequestWithParams, NextApiResponse>({
  attachParams: true,
  onNoMatch: onNoMatch,
});

handler.use(csrfProtection());

const ROUTE = "react/reactions";

/**
 * Route: "api/react/reactions"
 * Method: GET,
 * Desc: Get a user reaction to a post or comment
 */
handler.get(
  queryToArray(["post_id", "comment_id"], MAX_POSTS_PER_PAGE),
  validateQuery(getMultiReactionsSchema, ROUTE),
  async (req, res) => {
    try {
      const target = req.query.post_id ? "post" : "comment";

      let reactions;

      switch (target) {
        case "post":
          reactions = await getPostReactions(req.query as any);
          encode36Array(reactions, ["post_id"]);
          break;

        case "comment":
          reactions = await getCommentsReactions(req.query as any);
          encode36Array(reactions, ["comment_id"]);
          break;

        default:
          throw new Error(`Target must be post or comment. Got ${target}`);
      }

      return res.status(StatusCodes.OK).json({ reactions });
    } catch (error) {
      return errorHandler(error, res);
    }
  }
);

export default handler;

async function getPostReactions(data: getMultiReactionsSchemaType) {
  if (!data.post_id) {
    throw new Error(`Posts id must be attached to the query`);
  }

  const postIds = stringArrToBigInts(data.post_id);

  return await prisma.reaction
    .groupBy({
      by: ["post_id", "reaction"],
      _count: { reaction: true },
      where: {
        post_id: {
          in: postIds,
        },
      },
    })
    .catch(disconnectAndPropogateError);
}

async function getCommentsReactions(data: getMultiReactionsSchemaType) {
  if (!data.comment_id) {
    throw new Error(`Comments id must be attached to the query`);
  }

  const commentIds = stringArrToBigInts(data.comment_id);

  return await prisma.reaction
    .groupBy({
      by: ["comment_id", "reaction"],
      _count: { reaction: true },
      where: {
        comment_id: {
          in: commentIds,
        },
      },
    })
    .catch(disconnectAndPropogateError);
}
