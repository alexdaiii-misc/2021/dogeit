// lib
import { errorHandler } from "../../../../lib/errors";
import { rocketShip } from "../../../../lib/server-const/routeNames";
import { onNoMatch } from "../../../../lib/utils";
import { RocketNameFromURI } from "../../../../lib/attachId/rocketId";
import dbErrors from "../../../../lib/queries/dbError/dbUserThrownErrors";

// ts
import { NextApiResponse } from "next";
import {
  NextApiRequestWithParams,
  RequestWithUser,
} from "../../../../types/requestTypes";

// http next
import nc from "next-connect";
import { StatusCodes } from "http-status-codes";

// db
import prisma from "../../../../lib/prisma";

// middleware
import signedIn from "../../../../middleware/signedIn";
import csrfProtection from "../../../../middleware/csrfProtection";

const handler = nc<NextApiRequestWithParams, NextApiResponse>({
  attachParams: true,
  onNoMatch: onNoMatch,
});

handler.use(csrfProtection());

const ROUTE = `${rocketShip}/accept_moderator_invite`;

/**
 * Route: /api/rocket/:rocketId/accept_moderator_invite
 * Method: POST
 * Desc: Accept an invite to subscribe or moderate a rocketship. The authenticated user must have been invited to moderate or subscribe to the subreddit by one of its current moderators.
 * Reddit API Eq: [/r/subreddit]/api/accept_moderator_invite
 *
 * Auth: signed in
 */
handler.post(ROUTE, signedIn(), async (req, res) => {
  try {
    const rocketName = new RocketNameFromURI(req).getId();

    await makeUserModerator(req, rocketName);
    return res
      .status(StatusCodes.OK)
      .json({ msg: `Made ${req.user.username} a moderator of ${rocketName}` });
  } catch (error) {
    return errorHandler(error, res);
  }
});

/**
 * Makes the user the moderator if the rocket
 */
async function makeUserModerator(req: RequestWithUser, rocketName: string) {
  try {
    // SELECT * FROM accept_invite (rocket_name TEXT, usr_pid BIGINT)
    await prisma.$queryRaw(
      `SELECT * FROM accept_invite ($1, ${req.user.public_id.toString(10)});`,
      rocketName
    );
  } catch (error) {
    throw await dbErrors(error);
  }
}

export default handler;
