import { returnedRocketDetails } from "./../../../../lib/queries/qryReturnVals";
// schemas
import { updateRocketSchema } from "../../../../schemas/rocketSchemas";

// next
import nc from "next-connect";
import { StatusCodes } from "http-status-codes";

// middleware
import xxsSanatize from "../../../../middleware/xxsSanatize";
import validateBody from "../../../../middleware/validateBody";
import signedIn from "../../../../middleware/signedIn";
import csrfProtection from "../../../../middleware/csrfProtection";

// lib
import { DogeitError, errorHandler } from "../../../../lib/errors";
import { onNoMatch } from "../../../../lib/utils";
import { rocketShip } from "../../../../lib/server-const/routeNames";
import { RocketNameFromURI } from "../../../../lib/attachId/rocketId";

// ts
import { NextApiResponse } from "next";
import {
  NextApiRequestWithParams,
  RequestWithUser,
} from "../../../../types/requestTypes";
import { rocketUpdateInfo } from "../../../../types/schemas/rocketSchemas";

// db
import prisma from "../../../../lib/prisma";
import { disconnectAndPropogateError } from "../../../../lib/queries/disconnect";
import { rockets } from "@prisma/client";
import dbErrors from "../../../../lib/queries/dbError/dbUserThrownErrors";

const handler = nc<NextApiRequestWithParams, NextApiResponse>({
  attachParams: true,
  onNoMatch: onNoMatch,
});

handler.use(csrfProtection());

const ROUTE = rocketShip;

/**
 * Route: "api/rocket/:rocketId"
 * Method: GET,
 * Desc: Returns information about a rocketship
 */
handler.get(ROUTE, async (req, res) => {
  try {
    const rocket = await getRocketByName(req);

    return res.status(StatusCodes.OK).json({ rocket });
  } catch (error) {
    return errorHandler(error, res);
  }
});

/**
 * Route: "api/rocket/:rocketId"
 * Method: PATCH,
 * Desc: Updates information about a rocketship
 *
 * Auth: must be a moderator
 */
handler.patch(
  ROUTE,
  validateBody(updateRocketSchema),
  signedIn(),
  xxsSanatize,
  async (req, res) => {
    try {
      const { id, ...rocket } = await updateRocketInfo(req, req.body);

      return res.status(StatusCodes.OK).json({ rocket });
    } catch (error) {
      return errorHandler(error, res);
    }
  }
);

export default handler;

/**
 * Returns the rocket information by the name of the rocket
 */
async function getRocketByName(req: NextApiRequestWithParams) {
  const name = new RocketNameFromURI(req).getId();

  const rocket = await prisma.rockets
    .findUnique({
      select: returnedRocketDetails,
      where: { name },
    })
    .catch(disconnectAndPropogateError);

  if (!rocket) {
    throw new DogeitError({
      msg: `Rocketship ${name} cannot be found`,
      statusCode: StatusCodes.NOT_FOUND,
    });
  }

  return rocket;
}

/**
 * Updates the rocket information if the user is a moderator
 */
async function updateRocketInfo(req: RequestWithUser, data: rocketUpdateInfo) {
  const name = new RocketNameFromURI(req).getId();

  /**
   * Note without paramaterizing it would be vulnerable to this input
   * 	{"about": "sql injection','doge', 123456), users where 1=1; -- Anyone would be able to perform a sql injection to get all users"}
   */

  try {
    /**
     * result from
     *
     * SELECT *
     * FROM update_rocket(r_about TEXT, rocket_name TEXT, usr_pid BIGINT)
     */
    const rocketArray = (await prisma.$queryRaw(
      // should be fine to not paramaterize req.user.public_id since server creates it
      `SELECT * FROM update_rocket($1, $2, $3, ${req.user.public_id})`,
      // MUST be paramaterized or it will be vulnerable to sql injection
      data.about ?? null,
      data.restrict_posting ?? null,
      name
    )) as rockets[];

    if (rocketArray.length !== 1) {
      console.error("Rocket Array");
      console.error(rocketArray);
      throw new Error(
        `Rocket array length does not equal 1. Length is ${rocketArray.length}`
      );
    }

    return rocketArray[0];
  } catch (error) {
    throw await dbErrors(error);
  }
}
