// schemas
import { inviteUserSchema } from "../../../../schemas/rocketSchemas";

// next
import nc from "next-connect";
import { StatusCodes } from "http-status-codes";

// middleware
import signedIn from "../../../../middleware/signedIn";
import validateBody from "../../../../middleware/validateBody";
import csrfProtection from "../../../../middleware/csrfProtection";

// lib
import { DogeitError, errorHandler } from "../../../../lib/errors";
import { onNoMatch } from "../../../../lib/utils";
import { rocketShip } from "../../../../lib/server-const/routeNames";
import { RocketNameFromURI } from "../../../../lib/attachId/rocketId";

// ts
import { NextApiResponse } from "next";
import {
  NextApiRequestWithParams,
  RequestWithUser,
} from "../../../../types/requestTypes";
import { inviteUserType } from "../../../../types/schemaTypes";

// db
import prisma from "../../../../lib/prisma";
import { invitations } from "@prisma/client";
import dbErrors from "../../../../lib/queries/dbError/dbUserThrownErrors";

const handler = nc<NextApiRequestWithParams, NextApiResponse>({
  attachParams: true,
  onNoMatch,
});

handler.use(csrfProtection());

const ROUTE = `${rocketShip}/invite_moderator`;

/**
 * Route: "api/rocket/:rocketId/invite"
 * Method: POST,
 * Desc: Create an invitiation to a user to become a mod or subscriber of a sub
 *
 * Auth: Signed In, Moderator, Site Admin
 */
handler.post(
  ROUTE,
  validateBody(inviteUserSchema),
  signedIn(),
  async (req, res) => {
    try {
      const data: inviteUserType = req.body;
      const rocketName = new RocketNameFromURI(req).getId();

      checkInvitedSelf(req, data);

      await addInvite(req, data, rocketName);

      return res
        .status(StatusCodes.CREATED)
        .json({ msg: `Invited ${data.username} to ${rocketName}` });
    } catch (error: any) {
      // handler for all errors
      return errorHandler(error, res, { prisma: { msg: error?.meta?.cause } });
    }
  }
);

/**
 * Invitee can't be
 *
 * - same as invitee
 * - doesn't exist
 *
 * Invitee can be
 * - already a moderator
 *
 * Performs the check that you cannot invite yourself
 */
function checkInvitedSelf(req: RequestWithUser, data: inviteUserType) {
  if (req.user.username.toLowerCase() === data.username.toLowerCase()) {
    throw new DogeitError({
      msg: "Cannot invite yourself to the rocketship",
      statusCode: StatusCodes.BAD_REQUEST,
    });
  }
}

/**
 * Adds the invite
 */
async function addInvite(
  req: RequestWithUser,
  data: inviteUserType,
  rocketName: string
) {
  try {
    (await prisma.$queryRaw(
      `SELECT * FROM add_invitation($1, $2, ${req.user.public_id.toString()})`,
      rocketName,
      data.username
    )) as invitations[];
  } catch (error) {
    throw await addInviteErrorHandler(error, data, rocketName);
  }
}

/**
 * Throws the appropriate error message
 */
async function addInviteErrorHandler(
  error: any,
  data: inviteUserType,
  rocketName: string
) {
  if (error?.meta) {
    // print the original prisma messsage
    console.error(error);

    if (error?.code === "P2010" && error?.meta?.code === "23505") {
      throw new DogeitError({
        msg: `User: '${data.username}' has already been invited to '${rocketName}'`,
        statusCode: StatusCodes.CONFLICT,
      });
    }

    throw await dbErrors(error);
  }

  throw error;
}

export default handler;
