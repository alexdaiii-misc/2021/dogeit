import { paginatedQuery } from "../../../../schemas/paginatedQuerySchema";
import { NextApiRequest, NextApiResponse } from "next";
import nc from "next-connect";
import { rocketShip } from "../../../../lib/server-const/routeNames";
import validateQuery from "../../../../middleware/validateQuery";
import { paginatedResult } from "../../../../lib/handlers/paginatedResult";
import RocketPostsPage from "../../../../lib/paginatedClass/RocketPostsPage";

const handler = nc<NextApiRequest, NextApiResponse>({ attachParams: true });

const ROUTE = `${rocketShip}/posts`;

import csrfProtection from "../../../../middleware/csrfProtection";

handler.use(csrfProtection());

/**
 * Route: "api/rocket/:rocketid/posts"
 * Method: GET,
 * Desc: Gets the posts of a rocket. Paginated using pointers
 */
handler.get(ROUTE, validateQuery(paginatedQuery(), ROUTE), async (req, res) => {
  return await paginatedResult(req, res, RocketPostsPage);
});

export default handler;
