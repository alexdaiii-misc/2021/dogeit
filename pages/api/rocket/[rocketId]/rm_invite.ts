// next http
import nc from "next-connect";

// middleware
import signedIn from "../../../../middleware/signedIn";
import csrfProtection from "../../../../middleware/csrfProtection";

// lib
import { errorHandler } from "../../../../lib/errors";
import { onNoMatch } from "../../../../lib/utils";
import { rocketShip } from "../../../../lib/server-const/routeNames";
import { RocketNameFromURI } from "../../../../lib/attachId/rocketId";
import prisma from "../../../../lib/prisma";

// ts
import {
  NextApiRequestWithParams,
  RequestWithUser,
} from "../../../../types/requestTypes";
import { NextApiResponse } from "next";
import dbErrors from "../../../../lib/queries/dbError/dbUserThrownErrors";
import { StatusCodes } from "http-status-codes";

const handler = nc<NextApiRequestWithParams, NextApiResponse>({
  attachParams: true,
  onNoMatch: onNoMatch,
});

handler.use(csrfProtection());

const ROUTE = `${rocketShip}/rm_invite`;
/**
 * Route: "api/rocket/:rocketId/rm_invite"
 * Method: DELETE,
 * Desc: Deletes the rocket invite
 *
 * Auth: signed in
 */
handler.delete(ROUTE, signedIn(), async (req, res) => {
  try {
    const rocketName = new RocketNameFromURI(req).getId();

    await deleteInvitation(req, rocketName);

    res
      .status(StatusCodes.GONE)
      .json({ msg: `Deleted invitation to ${rocketName}` });
  } catch (error) {
    return errorHandler(error, res);
  }
});

/**
 * Deletes user invitation from the rocket
 */
async function deleteInvitation(req: RequestWithUser, rocketName: string) {
  try {
    // SELECT * FROM delete_user_invite(rocket_name TEXT, usr_pid BIGINT)
    return await prisma.$queryRaw(
      `SELECT * FROM delete_user_invite($1, ${req.user.public_id.toString()})`,
      rocketName
    );
  } catch (error) {
    throw await dbErrors(error);
  }
}

export default handler;
