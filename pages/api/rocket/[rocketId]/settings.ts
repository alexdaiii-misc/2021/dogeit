// schemas
import { updateRocketSettingsSchema } from "../../../../schemas/rocketSchemas";

// lib
import { rocketShip } from "../../../../lib/server-const/routeNames";
import { onNoMatch } from "../../../../lib/utils";
import { errorHandler, DogeitError } from "../../../../lib/errors";
import prisma from "../../../../lib/prisma";
import { RocketNameFromURI } from "../../../../lib/attachId/rocketId";

// http next
import nc from "next-connect";
import { StatusCodes } from "http-status-codes";

// ts
import { NextApiResponse } from "next";
import {
  NextApiRequestWithParams,
  RequestWithUser,
} from "../../../../types/requestTypes";
import { rocket_settings } from "@prisma/client";
import { rocketSettings } from "../../../../types/schemas/rocketSchemas";

// middleware
import signedIn from "../../../../middleware/signedIn";
import validateBody from "../../../../middleware/validateBody";
import xxsSanatize from "../../../../middleware/xxsSanatize";
import csrfProtection from "../../../../middleware/csrfProtection";

// db
import { disconnectAndPropogateError } from "../../../../lib/queries/disconnect";
import dbErrors from "../../../../lib/queries/dbError/dbUserThrownErrors";

const handler = nc<NextApiRequestWithParams, NextApiResponse>({
  attachParams: true,
  onNoMatch: onNoMatch,
});

handler.use(csrfProtection());

const ROUTE = `${rocketShip}/settings`;

/**
 * Route: "api/rocket/:rocketId/about/settings"
 * Method: GET,
 * Desc: Returns all settings of a rocketship
 * Reddit API eq: [/r/subreddit]/about/settings
 */
handler.get(ROUTE, async (req, res) => {
  try {
    const rocketName = new RocketNameFromURI(req).getId();

    const rocket_settings = await getSettings(rocketName);

    return res.status(StatusCodes.OK).json({ rocket_settings });
  } catch (error) {
    errorHandler(error, res);
  }
});

/**
 * Route: "api/rocket/:rocketId/about/settings"
 * Method: PATCH,
 * Desc: Updates settings of a rocketship
 * Reddit API eq: [/r/subreddit]/about/settings
 *
 * Auth: signedIn(), moderator
 */
handler.put(
  ROUTE,
  validateBody(updateRocketSettingsSchema),
  signedIn(),
  xxsSanatize,
  async (req, res) => {
    try {
      const rocketName = new RocketNameFromURI(req).getId();

      const { id, ...settings } = await updateSettings(
        req,
        rocketName,
        req.body
      );

      return res.status(StatusCodes.CREATED).json({ settings });
    } catch (error) {
      return errorHandler(error, res);
    }
  }
);

export default handler;

/**
 * Gets the settings of the rocket
 */
async function getSettings(rocketName: string) {
  const rocket = await prisma.rockets
    .findUnique({
      select: { name: true, rocket_settings: { select: { settings: true } } },
      where: { name: rocketName },
    })
    .catch(disconnectAndPropogateError);

  if (!rocket) {
    throw new DogeitError({
      msg: `Rocket ${rocketName} does not exist`,
      statusCode: StatusCodes.NOT_FOUND,
    });
  }

  return rocket;
}

/**
 * Updates a rocket settings
 */
async function updateSettings(
  req: RequestWithUser,
  rocketName: string,
  data: rocketSettings
) {
  try {
    /**
     * Result from
     *
     * update_rkt_settings(rocket_name TEXT,usr_pid BIGINT,new_settings JSON)
     */
    const settings = (await prisma.$queryRaw(
      `SELECT * FROM update_rkt_settings($1, ${req.user.public_id.toString()}, $2::json)`,
      rocketName,
      data
    )) as rocket_settings[];

    if (settings.length !== 1) {
      console.error(settings);
      throw new Error(
        `Could not properly update rocket settings for ${rocketName}`
      );
    }

    return settings[0];
  } catch (error) {
    throw await dbErrors(error);
  }
}
