// next
import nc from "next-connect";
import { StatusCodes } from "http-status-codes";

// middleware
import validateBody from "../../../../middleware/validateBody";
import signedIn from "../../../../middleware/signedIn";
import csrfProtection from "../../../../middleware/csrfProtection";

// schemas
import { subRocketSchema } from "../../../../schemas/rocketSchemas";

// lib
import { rocketShip } from "../../../../lib/server-const/routeNames";
import { onNoMatch } from "../../../../lib/utils";
import { DogeitError, errorHandler } from "../../../../lib/errors";
import { RocketNameFromURI } from "../../../../lib/attachId/rocketId";

// db
import prisma from "../../../../lib/prisma";
import { disconnectAndPropogateError } from "../../../../lib/queries/disconnect";

// ts
import {
  NextApiRequestWithParams,
  RequestWithUser,
} from "../../../../types/requestTypes";
import { NextApiResponse } from "next";
import { subscribers } from "@prisma/client";

const handler = nc<NextApiRequestWithParams, NextApiResponse>({
  attachParams: true,
  onNoMatch,
});

handler.use(csrfProtection());

const ROUTE = `${rocketShip}/subscribe`;

/**
 * Route: "api/rocket/:rocketId/subscribe"
 * Method: POST,
 * Desc: Subscribes or unsubscribes to a rocketship
 * Reddit API Eq: /api/subscribe
 */
handler.post(
  ROUTE,
  validateBody(subRocketSchema),
  signedIn(),
  async (req, res) => {
    switch (req.body.action) {
      case "sub":
        return await subscribeUser(req, res);

      case "unsub":
        return await unsubscribeUser(req, res);

      default:
        // Should never get to here - return generic error
        return errorHandler(
          { msg: "Could not process type. Did it bypass yup validation?" },
          res
        );
    }
  }
);

/**
 * Subscribes user to rocketship
 */
async function subscribeUser(req: RequestWithUser, res: NextApiResponse) {
  const rocketName = new RocketNameFromURI(req).getId();

  try {
    await addSubscriber(req.user.public_id, rocketName);

    return res
      .status(StatusCodes.CREATED)
      .json({ msg: `Subscribed to ${rocketName}` });
  } catch (error) {
    return errorHandler(error, res, {
      prisma: {
        msg: `You are already subscribed to the rocketship ${rocketName}`,
        statusCode: StatusCodes.CONFLICT,
      },
    });
  }
}

/**
 * Unsubscribe user from rocketship
 */
async function unsubscribeUser(req: RequestWithUser, res: NextApiResponse) {
  const rocketName = new RocketNameFromURI(req).getId();

  try {
    await removeSubscriber(req.user.public_id, rocketName);

    return res
      .status(StatusCodes.OK)
      .json({ msg: `Unsubscribed from ${rocketName}` });
  } catch (error) {
    return errorHandler(error, res, {
      prisma: {
        msg: `You were not previously subscribed to ${rocketName}`,
        statusCode: StatusCodes.NOT_FOUND,
      },
    });
  }
}

export default handler;

/**
 * Add subscriber to rocket
 * @param user_id user's public id
 * @param name the rocket name
 */
async function addSubscriber(public_id: bigint, name: string) {
  // a horrendeously inefficent 4 statement transaction
  return await prisma.subscribers
    .create({
      data: {
        rockets: {
          connect: {
            name,
          },
        },
        users: {
          connect: {
            public_id,
          },
        },
        auth_level: "sub",
      },
    })
    .catch(disconnectAndPropogateError);
}

/**
 * Remove subscriber from rocket
 * @param public_id user's public id
 * @param name the rocket name
 */
async function removeSubscriber(user_id: bigint, name: string) {
  const subscriptions = (await prisma.$queryRaw(
    ` DELETE
    FROM      subscribers
    WHERE     user_id = ${user_id}
    AND       rocket_id IN
              (
                     SELECT id
                     FROM   rockets
                     WHERE  name = $1 )
    returning * `,
    name
  )) as subscribers[];

  if (subscriptions.length === 0) {
    // subscription does not exist
    throw new DogeitError({
      msg: `You were not previously subscribed to ${name}`,
      statusCode: StatusCodes.NOT_FOUND,
    });
  }

  return subscriptions[0];
}
