// http next
import nc from "next-connect";
import { StatusCodes } from "http-status-codes";

// ts
import {
  NextApiRequestWithParams,
  RequestWithUser,
} from "../../../../types/requestTypes";
import { NextApiResponse } from "next";
import { onNoMatch } from "../../../../lib/utils";

// middleware
import csrfProtection from "../../../../middleware/csrfProtection";
import signedIn from "../../../../middleware/signedIn";

// lib
import { rocketShip } from "../../../../lib/server-const/routeNames";
import { errorHandler } from "../../../../lib/errors";
import { DogeitError } from "./../../../../lib/errors";
import { RocketNameFromURI } from "../../../../lib/attachId/rocketId";

// db
import prisma from "../../../../lib/prisma";

const handler = nc<NextApiRequestWithParams, NextApiResponse>({
  attachParams: true,
  onNoMatch: onNoMatch,
});

const ROUTE = `${rocketShip}/subscribed`;

handler.use(csrfProtection());

/**
 * Route: "api/rocket/:rocketId/subscribed"
 * Method: GET,
 * Desc: Tells if user is subscribed to a rocket or not
 */
handler.get(ROUTE, signedIn(), async (req, res) => {
  try {
    const rocketName = new RocketNameFromURI(req).getId();

    const subscriptions = await getSubscriber(req, rocketName);

    return res.status(StatusCodes.OK).json({ subscriptions });
  } catch (error) {
    return errorHandler(error, res);
  }
});

async function getSubscriber(req: RequestWithUser, name: string) {
  const subscribed = await prisma.rockets.findUnique({
    select: {
      name: true,
      subscribers: {
        select: { auth_level: true },
        where: { user_id: req.user.public_id },
      },
    },
    where: { name },
  });

  if (!subscribed) {
    throw new DogeitError({
      msg: `Rocket ${name} does not exist`,
      statusCode: StatusCodes.NOT_FOUND,
    });
  }

  return subscribed;
}

export default handler;
