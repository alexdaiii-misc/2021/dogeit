import { StatusCodes } from "http-status-codes";
import { subscribersQueryType } from "./../../../../types/schemas/subscribersSchemas.d";
// next http
import nc from "next-connect";

// middleware
import signedIn from "../../../../middleware/signedIn";
import validateQuery from "../../../../middleware/validateQuery";
import csrfProtection from "../../../../middleware/csrfProtection";

// lib
import { DogeitError, errorHandler } from "../../../../lib/errors";
import { rocketShip } from "../../../../lib/server-const/routeNames";
import { onNoMatch } from "../../../../lib/utils";
import { RocketNameFromURI } from "../../../../lib/attachId/rocketId";

// schema
import { subscribersSchema } from "../../../../schemas/subscribersSchemas";

// ts
import { NextApiResponse } from "next";
import { NextApiRequestWithParams } from "../../../../types/requestTypes";

// db
import prisma from "../../../../lib/prisma";
import dbErrors from "../../../../lib/queries/dbError/dbUserThrownErrors";

const handler = nc<NextApiRequestWithParams, NextApiResponse>({
  attachParams: true,
  onNoMatch: onNoMatch,
});

const ROUTE = `${rocketShip}/subscribers`;

handler.use(csrfProtection());

// TODO: cache results

/**
 * Route: "api/rocket/:rocketId/about/subscribers"
 * Method: GET,
 * Desc: Returns number of subscribers of a rocket
 */
handler.get(
  ROUTE,
  validateQuery(subscribersSchema, ROUTE),
  signedIn(),
  async (req, res) => {
    try {
      const authLevel = (req.query as subscribersQueryType).subscriber;

      const rocketName = new RocketNameFromURI(req).getId();

      switch (authLevel) {
        case "sub":
          const subscribers_count = await getSubscriberCountEst(rocketName);

          return res.status(StatusCodes.OK).json({ subscribers_count });

        case "mod":
          const mods = await getModeratorsOfRocket(rocketName);

          return res.status(StatusCodes.OK).json({ mods });

        default:
          throw new Error("authLevel can only be sub or mod");
      }
    } catch (error) {
      return errorHandler(error, res);
    }
  }
);

export default handler;

/**
 * Gets the usernames of the moderators of the rocket
 */
async function getModeratorsOfRocket(name: string) {
  const mods = await prisma.rockets.findUnique({
    select: {
      name: true,
      subscribers: {
        select: { users: { select: { username: true } } },
        where: { auth_level: "mod" },
      },
    },
    where: { name },
  });

  if (!mods) {
    throw new DogeitError({
      msg: `Rocket ${name} does not exist`,
      statusCode: StatusCodes.NOT_FOUND,
    });
  }

  return mods;
}

/**
 * Gets the estimated count of the number of subscribers of a rocket
 */
async function getSubscriberCountEst(name: string) {
  try {
    const estCount = (
      (await prisma.$queryRaw`SELECT * FROM subscribers_count(${name})`) as [
        { subscribers_count: number }
      ]
    )[0];

    return estCount;
  } catch (error) {
    throw await dbErrors(error);
  }
}
