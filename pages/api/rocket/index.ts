// next
import nc from "next-connect";
import { StatusCodes } from "http-status-codes";

// ts
import { NextApiRequest, NextApiResponse } from "next";
import { newRocketData } from "../../../types/schemas/rocketSchemas";

// lib
import { errorHandler } from "./../../../lib/errors";
import { onNoMatch } from "../../../lib/utils";
import RocketList from "../../../lib/paginatedClass/RocketList";

// middlewaer
import xxsSanatize from "../../../middleware/xxsSanatize";
import validateBody from "../../../middleware/validateBody";
import signedIn from "../../../middleware/signedIn";
import csrfProtection from "../../../middleware/csrfProtection";
import validateQuery from "../../../middleware/validateQuery";

// schemas
import { newRocketSchema } from "../../../schemas/rocketSchemas";
import { paginatedRocketList } from "./../../../schemas/paginatedQuerySchema";

// db
import prisma from "../../../lib/prisma";
import { disconnectAndPropogateError } from "../../../lib/queries/disconnect";
import { returnedRocketDetails } from "./../../../lib/queries/qryReturnVals";
import { paginatedResult } from "../../../lib/handlers/paginatedResult";

const handler = nc<NextApiRequest, NextApiResponse>({
  attachParams: true,
  onNoMatch,
});

handler.use(csrfProtection());

const ROUTE = "/api/rocket";

/**
 * Route: "api/rocket"
 * Method: POST,
 * Desc: Creates a new rocket. The user who created the rocket will become a moderator.
 *
 * Auth: must be signed in
 */
handler.post(
  validateBody(newRocketSchema),
  signedIn(),
  xxsSanatize,
  async (req, res) => {
    try {
      const rocket = await createNewRocket(req.body, req.user.public_id);

      return res.status(StatusCodes.CREATED).json({ rocket });
    } catch (error) {
      return errorHandler(error, res, {
        prisma: {
          msg: `Rocketship name ${req.body.name} already taken`,
          statusCode: StatusCodes.CONFLICT,
        },
      });
    }
  }
);

/**
 * Route: "api/rocket"
 * Method: POST,
 * Desc: Creates a new rocket. The user who created the rocket will become a moderator.
 *
 * Auth: must be signed in
 */
handler.get(
  ROUTE,
  validateQuery(paginatedRocketList(), ROUTE),
  async (req, res) => {
    return await paginatedResult(req, res, RocketList);
  }
);

export default handler;

/**
 * Creates a new rocket and subscribes the user to the rocket
 * @param data rocket data
 * @param user_id the public id of the user
 */
async function createNewRocket(data: newRocketData, user_id: bigint) {
  return await prisma.rockets
    .create({
      select: returnedRocketDetails,
      data: {
        ...data,
        subscribers: { create: [{ user_id, auth_level: "mod" }] },
      },
    })
    .catch(disconnectAndPropogateError);
}
