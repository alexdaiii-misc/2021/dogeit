// http next
import nc from "next-connect";
import { StatusCodes } from "http-status-codes";

// lib
import { onNoMatch } from "../../../../lib/utils";
import { userIndiv } from "../../../../lib/server-const/routeNames";
import { USER_PARAMS_TO_ENCODE } from "./../../../../lib/encodeConsts";
import { DogeitError, errorHandler } from "./../../../../lib/errors";
import { encode36 } from "../../../../lib/encode36";

// middleware
import csrfProtection from "../../../../middleware/csrfProtection";

// ts
import { NextApiRequestWithParams } from "../../../../types/requestTypes";
import { NextApiResponse } from "next";
import validateQuery from "../../../../middleware/validateQuery";

// schemas
import { paginatedQuery } from "./../../../../schemas/paginatedQuerySchema";
import { UsernameFromURI } from "../../../../lib/attachId/userId";

// db
import prisma from "../../../../lib/prisma";

const handler = nc<NextApiRequestWithParams, NextApiResponse>({
  attachParams: true,
  onNoMatch: onNoMatch,
});

handler.use(csrfProtection());

const ROUTE = `${userIndiv}`;

/**
 * Route: "api/user/:username"
 * Method: GET,
 * Desc: Get information about a user
 */
handler.get(ROUTE, validateQuery(paginatedQuery(), ROUTE), async (req, res) => {
  try {
    const username = new UsernameFromURI(req).getId();

    let user = await getUser(username);

    encode36(user, USER_PARAMS_TO_ENCODE);

    return res.status(StatusCodes.OK).json({ user });
  } catch (error) {
    return errorHandler(error, res);
  }
});

export default handler;

async function getUser(username: string) {
  const user = await prisma.users.findUnique({
    select: {
      public_id: true,
      username: true,
      name: true,
      image: true,
      created_at: true,
      site_admin: true,
    },
    where: { username },
  });

  if (!user) {
    throw new DogeitError({
      msg: `User ${username} cannot be found`,
      statusCode: StatusCodes.NOT_FOUND,
    });
  }

  return user;
}
