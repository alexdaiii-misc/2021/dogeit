// http next
import nc from "next-connect";

// lib
import { onNoMatch } from "../../../../lib/utils";
import { userIndiv } from "../../../../lib/server-const/routeNames";
import UsersPosts from "../../../../lib/paginatedClass/UsersPosts";
import { paginatedResult } from "../../../../lib/handlers/paginatedResult";

// middleware
import csrfProtection from "../../../../middleware/csrfProtection";

// ts
import { NextApiRequestWithParams } from "../../../../types/requestTypes";
import { NextApiResponse } from "next";
import validateQuery from "../../../../middleware/validateQuery";

// schemas
import { paginatedQuery } from "./../../../../schemas/paginatedQuerySchema";

const handler = nc<NextApiRequestWithParams, NextApiResponse>({
  attachParams: true,
  onNoMatch: onNoMatch,
});

handler.use(csrfProtection());

const ROUTE = `${userIndiv}/posts`;

handler.get(ROUTE, validateQuery(paginatedQuery(), ROUTE), async (req, res) => {
  return await paginatedResult(req, res, UsersPosts);
});

export default handler;
