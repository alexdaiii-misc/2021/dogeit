import { signIn, signOut, useSession } from "next-auth/client";
import React from "react";
import Profile from "../components/Profile";
import UpdateUser from "../components/UpdateUser";

export default function Page() {
  const [session] = useSession();

  if (session) {
    return (
      <>
        <Profile />
        <button onClick={() => signOut()}>Sign Out</button>
        <UpdateUser />
      </>
    );
  } else {
    return (
      <>
        Not signed in <br />
        <button onClick={() => signIn()}>Sign in</button>
      </>
    );
  }
}
