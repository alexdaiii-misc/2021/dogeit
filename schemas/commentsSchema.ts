import {
  MIN_SUBMISSION_LEN,
  ONLY_ALPHANUMERIC_REGEX,
} from "../lib/server-const/schemaConsts";
import { MAX_LENGTH_FOR_2_64_INBASE_36 } from "../lib/encodeConsts";
import { object, string } from "yup";

/**
 * Comment on a post
 */

export const newCommentSchema = object({
  post_id: string()
    .required()
    .min(2)
    .max(MAX_LENGTH_FOR_2_64_INBASE_36)
    .matches(ONLY_ALPHANUMERIC_REGEX),
  body: string().required().min(MIN_SUBMISSION_LEN),
  parent_id: string()
    .optional()
    .min(2)
    .max(MAX_LENGTH_FOR_2_64_INBASE_36)
    .matches(ONLY_ALPHANUMERIC_REGEX),
});
