import { object, string, number } from "yup";
import {
  MAX_ROCKET_NAME,
  MIN_POSTS_PER_PAGE,
  MIN_ROCKET_NAME,
  ONLY_ALPHANUMERIC_REGEX,
  MAX_POSTS_PER_PAGE,
  ROCKET_NAME_REGEX,
} from "../lib/server-const/schemaConsts";
import { MAX_LENGTH_FOR_2_64_INBASE_36 } from "../lib/encodeConsts";

/**
 * Query for getting the posts of a rocket
 * **max_id** - Will get all posts of a rocket before this id. The maximum id to get. Default 10.
 * **min_id** - Will get all posts of a rocket after this id. The minimum id to get. Default 25.
 * **count** - Maximum number of results to return
 */
export const paginatedQuery = (
  options: {
    min_count: number;
    max_count: number;
  } = {
    min_count: MIN_POSTS_PER_PAGE,
    max_count: MAX_POSTS_PER_PAGE,
  }
) => {
  return object({
    // This is the cursor for paginating
    max_id: string()
      .min(2)
      .max(MAX_LENGTH_FOR_2_64_INBASE_36)
      .matches(ONLY_ALPHANUMERIC_REGEX),
    // useful if, for example, a user refreshes the page
    since_id: string()
      .test(
        "notBothAtTheSameTime",
        "You cannot pass since_id at the same time as max_id",
        function (value) {
          const { max_id } = this.parent;
          if (max_id) return !(value && max_id);
          return true;
        }
      )
      .min(2)
      .max(MAX_LENGTH_FOR_2_64_INBASE_36)
      .matches(ONLY_ALPHANUMERIC_REGEX),
    count: number()
      .min(options.min_count)
      .max(options.max_count)
      .default(options.max_count),
  });
};

/**
 * Query for getting a list of rockets
 * **max_id** - Will get all rockets created after this rocket.
 * **since_id** - Will get all rockets created before this rocket.
 * **count** - Maximum number of results to return
 */
export const paginatedRocketList = (
  options: {
    min_count: number;
    max_count: number;
  } = {
    min_count: MIN_POSTS_PER_PAGE,
    max_count: MAX_POSTS_PER_PAGE,
  }
) => {
  return object({
    // This is the cursor for paginating
    max_id: string()
      .min(MIN_ROCKET_NAME)
      .max(MAX_ROCKET_NAME)
      .matches(ROCKET_NAME_REGEX),
    // useful if, for example, a user refreshes the page
    since_id: string()
      .test(
        "notBothAtTheSameTime",
        "You cannot pass since_id at the same time as max_id",
        function (value) {
          const { max_id } = this.parent;
          if (max_id) return !(value && max_id);
          return true;
        }
      )
      .min(MIN_ROCKET_NAME)
      .max(MAX_ROCKET_NAME)
      .matches(ROCKET_NAME_REGEX),
    count: number()
      .min(options.min_count)
      .max(options.max_count)
      .default(options.max_count),
  });
};
