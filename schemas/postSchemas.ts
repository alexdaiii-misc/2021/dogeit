import {
  MIN_SUBMISSION_LEN,
  POST_DELETION_ACTIONS,
} from "../lib/server-const/schemaConsts";
import {
  MIN_ROCKET_NAME,
  MAX_ROCKET_NAME,
  ROCKET_NAME_REGEX,
} from "../lib/server-const/schemaConsts";
import { object, string } from "yup";

// Add regex matcher for only:
// words with latin characters, numbers, and underscore
// no underscore at beginning
// no spaces
/**
 * Request body for when someone makes a post
 */
export const newPostSchema = object({
  rocket: string()
    .required()
    .min(MIN_ROCKET_NAME)
    .max(MAX_ROCKET_NAME)
    .matches(ROCKET_NAME_REGEX),
  title: string().required().min(MIN_SUBMISSION_LEN),
  body: string().required().min(MIN_SUBMISSION_LEN),
});

/**
 * Request body when deleting a post
 */
export const deletePostSchema = object({
  action: string()
    .required()
    .oneOf(POST_DELETION_ACTIONS as unknown as string[]),
});
