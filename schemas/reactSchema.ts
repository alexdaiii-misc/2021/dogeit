import {
  MAX_POSTS_PER_PAGE,
  ONLY_ALPHANUMERIC_REGEX,
  POST_REACTIONS,
} from "../lib/server-const/schemaConsts";
import { object, string, array } from "yup";
import { MAX_LENGTH_FOR_2_64_INBASE_36 } from "../lib/encodeConsts";

/**
 * Request body when reacting to a record
 */
export const reactToRecordSchema = object({
  post_id: string()
    .min(2)
    .max(MAX_LENGTH_FOR_2_64_INBASE_36)
    .matches(ONLY_ALPHANUMERIC_REGEX),
  comment_id: string()
    .min(2)
    .max(MAX_LENGTH_FOR_2_64_INBASE_36)
    .matches(ONLY_ALPHANUMERIC_REGEX)
    .test(
      "notBothAtTheSameTime",
      "You cannot pass post_id at the same time as comment_id",
      function (value) {
        const { post_id } = this.parent;
        if (post_id) return !(value && post_id);
        return true;
      }
    )
    .test(
      "notAllNullAtSameTime",
      "post_id, comment_id cannot all be null at same time",
      function () {
        const { post_id, comment_id } = this.parent;
        return !(!post_id && !comment_id);
      }
    ),
  reaction: string()
    .required()
    .oneOf(POST_REACTIONS as unknown as string[]),
});

/**
 * Request body when getting a reaciton to a record
 */
export const getReactionSchema = object({
  post_id: string()
    .min(2)
    .max(MAX_LENGTH_FOR_2_64_INBASE_36)
    .matches(ONLY_ALPHANUMERIC_REGEX),
  comment_id: string()
    .min(2)
    .max(MAX_LENGTH_FOR_2_64_INBASE_36)
    .matches(ONLY_ALPHANUMERIC_REGEX)
    .test(
      "notBothAtTheSameTime",
      "You cannot pass post_id at the same time as comment_id",
      function (value) {
        const { post_id } = this.parent;
        if (post_id) return !(value && post_id);
        return true;
      }
    )
    .test(
      "notAllNullAtSameTime",
      "post_id, comment_id cannot all be null at same time",
      function () {
        const { post_id, comment_id } = this.parent;
        return !(!post_id && !comment_id);
      }
    ),
});

export const getMultiReactionsSchema = object({
  post_id: array(
    string()
      .min(2)
      .max(MAX_LENGTH_FOR_2_64_INBASE_36)
      .matches(ONLY_ALPHANUMERIC_REGEX)
      .required()
  ).max(MAX_POSTS_PER_PAGE),
  comment_id: array(
    string()
      .min(2)
      .max(MAX_LENGTH_FOR_2_64_INBASE_36)
      .matches(ONLY_ALPHANUMERIC_REGEX)
      .required()
  )
    .max(MAX_POSTS_PER_PAGE)
    .test(
      "notBothAtTheSameTime",
      "You cannot pass post_id at the same time as comment_id",
      function (value) {
        const { post_id } = this.parent;
        if (post_id) return !(value && post_id);
        return true;
      }
    )
    .test(
      "notAllNullAtSameTime",
      "post_id, comment_id cannot all be null at same time",
      function () {
        const { post_id, comment_id } = this.parent;
        return !(!post_id && !comment_id);
      }
    ),
});
