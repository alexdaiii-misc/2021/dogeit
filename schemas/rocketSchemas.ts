import { googleFontsList } from "../lib/server-const/googleFontsList";
import {
  CSS_COLOR_REGEX,
  MAX_ABOUT_CHARS,
  MAX_ROCKET_NAME,
  MAX_USERNAME_LENGTH,
  MIN_ROCKET_NAME,
  MIN_USERNAME_LENGTH,
  ROCKET_NAME_REGEX,
  USER_USERNAME_REGEX,
} from "../lib/server-const/schemaConsts";
import { object, string, boolean } from "yup";

/**
 * Body for creating a new rocket ship
 * **name** - a string containing only latin characters, numbers, or underscores. No underscores in beginning
 * **about** - text containing information about the rocket ship
 * **rules** - text containing the rules of the rocket ship
 * **allow_discovery** - boolean to make rocketship public or not
 * **restrict_posting** - boolean to lock down rocketship posting to mods
 */
export const newRocketSchema = object({
  name: string()
    .required()
    .min(MIN_ROCKET_NAME)
    .max(MAX_ROCKET_NAME)
    .matches(ROCKET_NAME_REGEX),
  about: string().optional().max(MAX_ABOUT_CHARS),
  restrict_posting: boolean().optional(),
});

/**
 * Body for creating a new subscription or unsubscribing
 * - **action** - one of (sub, unsub)
 */
export const subRocketSchema = object({
  action: string().required().oneOf(["sub", "unsub"]),
});

/**
 * Body for creating an invitation
 * - **username** - username of person to invite
 * - **type** - one of (moderator_invite, subscriber_invite)
 */
export const inviteUserSchema = object({
  username: string()
    .required()
    .min(MIN_USERNAME_LENGTH)
    .max(MAX_USERNAME_LENGTH)
    .matches(USER_USERNAME_REGEX),
});

/**
 * Body for updating the rocketship information
 * **about** - text containing information about the rocket ship
 * **rules** - text containing the rules of the rocket ship
 */
export const updateRocketSchema = object({
  about: string().required().max(MAX_ABOUT_CHARS),
  restrict_posting: boolean()
    .optional()
    .when("about", {
      is: (about: any) => !about,
      then: boolean().required("about or restrict_posting is a required"),
    }),
});

/**
 * Body for updating the rocketship information
 * **allow_discovery** - boolean to make rocketship public or not
 * **restrict_posting** - boolean to lock down rocketship posting to mods
 */
export const updateRocketSettingsSchema = object({
  key_color_light: string().optional().matches(CSS_COLOR_REGEX),
  key_color_dark: string().optional().matches(CSS_COLOR_REGEX),
  title_font: string()
    .optional()
    .oneOf(googleFontsList)
    .test(
      "notAllNullAtSameTime",
      "title_font, key_color_dark, and key_color_light cannot all be null at same time",
      function () {
        const { title_font, key_color_dark, key_color_light } = this.parent;
        return !(!title_font && !key_color_dark && !key_color_light);
      }
    ),
  // image is set through another route
});
