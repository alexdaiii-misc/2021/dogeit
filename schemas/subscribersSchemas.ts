import { object, string } from "yup";
import { SUBSCRIBER_QUERY } from "../lib/server-const/schemaConsts";

/**
 * Body for updating a user prefrences
 * - **nightmode** - boolean. toggles darkmode on or off
 */
// export const patchPrefsSchema = object({
//   nightmode: boolean(),
// });
/**
 * Query for paginated api route
 */

export const subscribersSchema = object({
  subscriber: string()
    .required()
    .oneOf(SUBSCRIBER_QUERY as unknown as string[]),
});
