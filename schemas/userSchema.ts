import { object, string } from "yup";
import {
  MAX_NAME_LEN,
  MAX_USERNAME_LENGTH,
  MIN_USERNAME_LENGTH,
  USER_NAME_REGEX,
  USER_USERNAME_REGEX,
} from "../lib/server-const/schemaConsts";

// Reddit's equiavlent in Python is \A[\w-]+\Z
// matches any word (letters, numbers, underscore) and "-", but stops matching at end of word

/**
 * Body for updating a new user
 * - **username** - string. Must use only latin chracters, numbers, underscores or dashes
 * - **name** - string. Must use only latin characters or spaces
 * - **email** - string. Must be a valid email address
 */
export const patchUserSchema = object({
  username: string()
    .min(MIN_USERNAME_LENGTH)
    .max(MAX_USERNAME_LENGTH)
    .matches(USER_USERNAME_REGEX),
  name: string().max(MAX_NAME_LEN).matches(USER_NAME_REGEX),
  email: string().email(),
});
