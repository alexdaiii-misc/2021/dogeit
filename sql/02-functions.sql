-- Instagram's answer to creating Id's
-- Their problem also had needed to create unique ids across shards
-- However, this project only will have one instance of Postgres running
-- Their solution gives a 64 bit id (1/2 size of a uuid), sortable by time 
--  - The first 41 bits keep track of milliseconds since January 1st, 2011
--  - Next 13 bits keep track of the shardid
--  - Last 10 bits represent an auto incrementing sequence, mod 1024
-- This allows them to have 1024 unique ids per millisecond per shard
-- Also this seems very similar to twitter's solution with snowflake
-- Their soln can be found here: https://instagram-engineering.com/sharding-ids-at-instagram-1cf5a71e5a5c

CREATE OR REPLACE FUNCTION insta_id(sequence_name TEXT, OUT result bigint) AS $$
DECLARE
    our_epoch bigint := 1314220021721;
    seq_id bigint;
    now_millis bigint;
    shard_id int := 1;
BEGIN
    SELECT nextval(sequence_name) % 1024 INTO seq_id;    
    SELECT FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000) INTO now_millis;
    result := (now_millis - our_epoch) << 23;
    result := result | (shard_id <<10);
    result := result | (seq_id);
END;
    $$ LANGUAGE PLPGSQL;

CREATE SEQUENCE public.posts_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

CREATE SEQUENCE public.comments_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

CREATE SEQUENCE public.crypto_prices_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

CREATE SEQUENCE public.users_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

-- generate the default starting post reactions
-- CREATE OR REPLACE FUNCTION public.starting_post_reaction()
-- RETURNS JSON
-- AS $$
-- DECLARE
--   starting_reaction JSON;
-- BEGIN

--   starting_reaction := '{
--     "like": 0, 
--     "dislike": 0, 
--     "love": 0, 
--     "angry": 0, 
--     "rocket": 0
--   }'::jsonb;

--   RETURN starting_reaction;
-- END;
-- $$ LANGUAGE plpgsql;

-- Insert a default username 
CREATE OR REPLACE FUNCTION generate_username(tablename text DEFAULT 'users', col text DEFAULT 'username')
RETURNS VARCHAR(11)
LANGUAGE plpgsql
AS
$$
DECLARE
  gkey TEXT;
  key VARCHAR(11);
  found TEXT;
  idx INTEGER := 0;
  max_tries INTEGER := 10;
BEGIN

  LOOP
  
    -- Generator by Nathan Fritz (andyet.com); turbo (github.com/turbo)
    -- 8 bytes gives a collision p = .5 after 5.1 x 10^9 values
    gkey := encode(extensions.gen_random_bytes(8), 'base64');
    gkey := replace(gkey, '/', '_');  -- url safe replacement
    gkey := replace(gkey, '+', '-');  -- url safe replacement
    key := rtrim(gkey, '=');          -- cut off padding

    EXECUTE format('SELECT %2$I
      FROM %1$I
      WHERE %2$s = %3$L
    ', tablename, col, key) INTO found;

    IF found IS NULL THEN
      RETURN key;
    END IF;
	
	idx := idx + 1;
	
  -- Too many collisions, do not keep generating
	if idx >= max_tries THEN
		RETURN null;
	END IF;

  END LOOP;

  RETURN key;

END
$$;

-- Can't get prisma to play nicely with this - use adj list instead
-- For generating pathids
-- CREATE SEQUENCE public.comments_path_id_seq
--     INCREMENT 1
--     START 1
--     MINVALUE 1
--     MAXVALUE 2147483647
--     CACHE 1
--     CYCLE;
