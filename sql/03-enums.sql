CREATE TYPE reactions AS ENUM ('dislike', 'like', 'love', 'angry', 'rocket');

CREATE TYPE lvl AS ENUM ('sub', 'mod');