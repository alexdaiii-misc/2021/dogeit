CREATE EXTENSION IF NOT EXISTS citext;  
-- CREATE EXTENSION IF NOT EXISTS ltree;  

CREATE TABLE public.users (
  id SERIAL,
  public_id BIGINT UNIQUE NOT NULL DEFAULT insta_id('users_id_seq'),
  username citext UNIQUE NOT NULL DEFAULT generate_username(),
  name VARCHAR(255),
  email citext UNIQUE,
  email_verified TIMESTAMPTZ,
  image TEXT,
  created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  site_admin BOOLEAN NOT NULL DEFAULT false,

  CONSTRAINT users_username_min_len_check
    CHECK (char_length(username) >= 3),

  CONSTRAINT users_username_max_len_check
    CHECK (char_length(username) <= 20),

  CONSTRAINT users_email_max_len_check 
    CHECK (char_length(email::text) <= 255),

  PRIMARY KEY (id)

);

CREATE TABLE public.accounts (
  id SERIAL,
  compound_id VARCHAR(255) NOT NULL,
  user_id INTEGER NOT NULL,
  provider_type VARCHAR(255) NOT NULL,
  provider_id VARCHAR(255) NOT NULL,
  provider_account_id VARCHAR(255) NOT NULL,
  refresh_token TEXT,
  access_token TEXT,
  access_token_expires TIMESTAMPTZ,
  created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,

  CONSTRAINT accounts_user_id_fk
    FOREIGN KEY (user_id)
      REFERENCES public.users(id)
      ON DELETE CASCADE,
  
  PRIMARY KEY (id)
);

CREATE UNIQUE INDEX compound_id ON public.accounts(compound_id);
CREATE INDEX provider_account_id ON public.accounts(provider_account_id);
CREATE INDEX provider_id ON public.accounts(provider_id);
CREATE INDEX user_id ON public.accounts(user_id);

CREATE TABLE public.sessions (
  id SERIAL,
  user_id INTEGER NOT NULL,
  expires TIMESTAMPTZ NOT NULL,
  session_token VARCHAR(255) NOT NULL UNIQUE,
  access_token VARCHAR(255) NOT NULL UNIQUE,
  created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,

  CONSTRAINT sessions_user_id_fk
    FOREIGN KEY (user_id)
      REFERENCES public.users(id)
      ON DELETE CASCADE,

  PRIMARY KEY (id)
);

CREATE TABLE public.verification_requests (
  id SERIAL,
  identifier VARCHAR(255) NOT NULL,
  token VARCHAR(255) NOT NULL,
  expires TIMESTAMPTZ NOT NULL,
  created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (id)
);

CREATE UNIQUE INDEX token ON public.verification_requests(token);

-- My tables

CREATE TABLE IF NOT EXISTS public.rockets (
  id SERIAL,
  "name" citext NOT NULL UNIQUE,
  created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  about TEXT,
  -- there are not any methods of banning a user from a rocket
  restrict_posting BOOLEAN NOT NULL DEFAULT false,

  CONSTRAINT rocket_name_min_len_check
    CHECK (char_length("name") >= 3),

  CONSTRAINT rocket_name_max_len_check
    CHECK (char_length("name") <= 20),

  PRIMARY KEY (id)
);

-- 1 to 0 relationship w rocket
CREATE TABLE IF NOT EXISTS public.rocket_settings (
  id INTEGER,
  settings JSON,

  CONSTRAINT rocket_settings_id_fk
  FOREIGN KEY (id)
    REFERENCES public.rockets(id) MATCH FULL
    ON DELETE CASCADE,

  PRIMARY KEY (id)
);

-- will be a vvv big table irl
CREATE TABLE IF NOT EXISTS public.subscribers (
  rocket_id INTEGER,
  user_id BIGINT,
  auth_level LVL NOT NULL DEFAULT 'sub',
  subscribed_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,

  CONSTRAINT subscribers_user_id_fk
  FOREIGN KEY (user_id)
    REFERENCES public.users(public_id) MATCH FULL
    ON DELETE CASCADE,

  CONSTRAINT subscribers_rocket_id_fk
  FOREIGN KEY (rocket_id)
    REFERENCES public.rockets(id) MATCH FULL
    ON DELETE CASCADE,

  -- must be created with user_id as first value
  -- will allow query rockets subscribed to by user
  -- create a second index to get users of a rocket
  PRIMARY KEY (user_id, rocket_id)

);

-- should rarely be used - mostly used to get count(*) of subscribers of rocket
CREATE INDEX rocket_subs ON public.subscribers (rocket_id);

CREATE TABLE IF NOT EXISTS public.crypto_prices (
  id BIGINT DEFAULT insta_id('crypto_prices_id_seq'),
  total_volume BIGINT NOT NULL,
  market_cap_rank INTEGER NOT NULL,
  coin_id VARCHAR(255) NOT NULL,
  name VARCHAR(255) NOT NULL,
  current_price DECIMAL NOT NULL,
  high_24h DECIMAL NOT NULL,
  low_24h DECIMAL NOT NULL,
  price_change_24h_in_currency DECIMAL NOT NULL,
  price_change_percentage_24h DECIMAL NOT NULL,
  circulating_supply DECIMAL NOT NULL,

  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.posts (
  id BIGINT DEFAULT insta_id('posts_id_seq'),
  crypto_prices_id BIGINT NOT NULL,
  rocket_id INTEGER NOT NULL,
  user_id BIGINT,
  created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  -- restrict_commenting BOOLEAN NOT NULL DEFAULT false,
  title VARCHAR(255) NOT NULL,
  body TEXT NOT NULL,
  -- This will just be aggregrated by db and returned in api route with caching
  -- reaction_aggr JSON NOT NULL DEFAULT starting_post_reaction();

  CONSTRAINT posts_rocket_id_fk
  FOREIGN KEY (rocket_id)
    REFERENCES public.rockets(id) MATCH FULL
    ON DELETE CASCADE,

  -- Deleting a user will still show their past posts with username [deleted]
  CONSTRAINT posts_user_id_fk
  FOREIGN KEY (user_id)
    REFERENCES public.users(public_id) MATCH FULL
    ON DELETE SET NULL,

  -- Deleting the dogecoin price will delete the post
  -- rocketship can keep something like a queue of posts and delete old posts
  -- if we only keep a certian number of prices
  CONSTRAINT posts_crypto_prices_id_fk
  FOREIGN KEY (crypto_prices_id)
    REFERENCES public.crypto_prices(id) MATCH FULL
    ON DELETE CASCADE,

  PRIMARY KEY (id)
);

CREATE INDEX rocket_posts ON public.posts (rocket_id);
CREATE INDEX users_posts ON public.posts(user_id);

CREATE TABLE IF NOT EXISTS public.comments (
  id BIGINT DEFAULT insta_id('comments_id_seq'),
  post_id BIGINT NOT NULL,
  user_id BIGINT,
  -- path_id INTEGER NOT NULL DEFAULT nextval('comments_path_id_seq'),
  created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  body TEXT NOT NULL,
  parent_id BIGINT,
  root_id BIGINT NOT NULL DEFAULT -1,
  crypto_prices_id BIGINT NOT NULL,
  -- Can't get prisma to play nicely with this - use adj list instead
  -- Will just have parent id's and only a depth of 1 for comments
  -- path LTREE NOT NULL,

  CONSTRAINT comments_posts_id_fk
  FOREIGN KEY (post_id)
    REFERENCES public.posts(id)
    ON DELETE CASCADE,

  CONSTRAINT comments_parent_id_fk
  FOREIGN KEY (parent_id)
    REFERENCES public.comments(id)
    ON DELETE CASCADE,

  CONSTRAINT comments_root_id_fk
  FOREIGN KEY (root_id)
    REFERENCES public.comments(id)
    ON DELETE CASCADE,

  -- Deleting a user will still show their comments with username [deleted]
  CONSTRAINT comments_user_id_fk
  FOREIGN KEY (user_id)
    REFERENCES public.users(public_id)
    ON DELETE SET NULL,

  CONSTRAINT comments_crypto_prices_id_fk
  FOREIGN KEY (crypto_prices_id)
    REFERENCES public.crypto_prices(id) MATCH FULL
    ON DELETE CASCADE,

  PRIMARY KEY (id)
);

CREATE INDEX post_comments ON public.comments (post_id);
CREATE INDEX users_comments ON public.comments (user_id);
-- root id used for querying w pagination - must be btree or brin
CREATE INDEX root_comment ON public.comments (root_id);
-- CREATE INDEX comments_path ON comments USING GIST(path);

CREATE TABLE IF NOT EXISTS public.reaction (
  -- must have an id pk since user_id may be NULL
  id SERIAL,
  post_id BIGINT,
  comment_id BIGINT,
  user_id BIGINT,
  created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  reaction REACTIONS NOT NULL,

  UNIQUE(post_id, user_id),
  UNIQUE(comment_id, user_id),

  -- Deleting a user will still count the reactions
  CONSTRAINT reaction_user_id_fk
  FOREIGN KEY (user_id)
    REFERENCES public.users(public_id) MATCH FULL
    ON DELETE SET NULL,

  CONSTRAINT reaction_post_id_fk
  FOREIGN KEY (post_id)
    REFERENCES public.posts(id) MATCH FULL
    ON DELETE CASCADE,

  CONSTRAINT reaction_comment_id_fk
  FOREIGN KEY (comment_id)
    REFERENCES public.comments(id) MATCH FULL
    ON DELETE CASCADE,

  PRIMARY KEY (id)
);

CREATE INDEX post_reactions ON public.reaction (post_id);
CREATE INDEX comment_reactions ON public.reaction (comment_id);
CREATE INDEX user_reactions ON public.reaction (user_id);

-- blob of settings
-- disadvantage - harder to validate data in database
CREATE TABLE IF NOT EXISTS public.preferences (
  id BIGINT,
  social_media JSON,
  settings JSON,
  updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,

  CONSTRAINT preferences_user_id_fk
  FOREIGN KEY (id)
    REFERENCES public.users(public_id)
    ON DELETE CASCADE,

  PRIMARY KEY (id)

);

CREATE TABLE IF NOT EXISTS public.invitations (
  user_id BIGINT,
  rocket_id INTEGER,
  created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  inviter BIGINT NOT NULL,

  CONSTRAINT mod_invites_rocket_id_fk
  FOREIGN KEY (rocket_id)
    REFERENCES public.rockets(id)
    ON DELETE CASCADE,
  
  CONSTRAINT mod_invites_user_id_fk
  FOREIGN KEY (user_id)
    REFERENCES public.users(public_id)
    ON DELETE CASCADE,

  CONSTRAINT mod_invites_inviter_fk
  FOREIGN KEY (inviter)
    REFERENCES public.users(public_id)
    ON DELETE SET NULL,

  PRIMARY KEY (user_id, rocket_id)
);

-- From Next Auth