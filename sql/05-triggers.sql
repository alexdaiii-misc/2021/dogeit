-- Updates the updated at timestamp to now
CREATE OR REPLACE FUNCTION PUBLIC.update_timestamp_column()
RETURNS TRIGGER AS $$
BEGIN
   NEW.updated_at = now(); 
   RETURN NEW;
END;
$$ language 'plpgsql';


-- Trigger functions to 
CREATE TRIGGER update_users_timestamp BEFORE UPDATE
ON PUBLIC.users FOR EACH ROW EXECUTE PROCEDURE 
PUBLIC.update_timestamp_column();

CREATE TRIGGER update_rocket_timestamp BEFORE UPDATE
ON PUBLIC.rockets FOR EACH ROW EXECUTE PROCEDURE 
PUBLIC.update_timestamp_column();

CREATE TRIGGER update_posts_timestamp BEFORE UPDATE
ON PUBLIC.posts FOR EACH ROW EXECUTE PROCEDURE 
PUBLIC.update_timestamp_column();

CREATE TRIGGER update_reaction_timestamp BEFORE UPDATE
ON PUBLIC.reaction FOR EACH ROW EXECUTE PROCEDURE 
PUBLIC.update_timestamp_column();

CREATE TRIGGER update_comments_timestamp BEFORE UPDATE
ON PUBLIC.comments FOR EACH ROW EXECUTE PROCEDURE 
PUBLIC.update_timestamp_column();

CREATE TRIGGER update_preferences_timestamp BEFORE UPDATE
ON PUBLIC.preferences FOR EACH ROW EXECUTE PROCEDURE 
PUBLIC.update_timestamp_column();


CREATE OR REPLACE FUNCTION gen_root_node()
RETURNS TRIGGER LANGUAGE plpgsql AS $$
BEGIN

    IF new.root_id < 0 THEN
      new.root_id = new.id;
    END IF;

    return new;
END $$;

DROP TRIGGER IF EXISTS before_insert_on_comments_root ON comments;

CREATE TRIGGER before_insert_on_comments_root
BEFORE INSERT ON comments
FOR EACH ROW EXECUTE PROCEDURE gen_root_node();