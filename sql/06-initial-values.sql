-- This is for testing so you can set the cookie easily in something
-- like postman or insomnia

-- A siteadmin
INSERT INTO 
  public.users (username, name, email, site_admin)
VALUES
  ('doge', 'doge', 'doge@example.com', true);

INSERT INTO
  public.sessions (user_id, expires, session_token, access_token)
VALUES
  ((SELECT id
  FROM users
  WHERE username = 'doge'), to_timestamp('01-01-2030 09:00:00', 'dd-mm-yyyy hh24:mi:ss'), 'doge', 'coin');

-- A regular user
INSERT INTO 
  public.users (username, name, email)
VALUES
  ('shiba', 'shiba', 'shibainu@example.com');

INSERT INTO
  public.sessions (user_id, expires, session_token, access_token)
VALUES
  ((SELECT id
  FROM users
  WHERE username = 'shiba'), to_timestamp('01-01-2030 09:00:00', 'dd-mm-yyyy hh24:mi:ss'), 'shiba', 'inu');

-- Second regular user
INSERT INTO
  public.users (username, name, email)
VALUES
  ('banano', 'banano', 'banano@example.com');

INSERT INTO
  public.sessions (user_id, expires, session_token, access_token)
VALUES
  ((SELECT id
  FROM users
  WHERE username = 'banano'), to_timestamp('01-01-2030 09:00:00', 'dd-mm-yyyy hh24:mi:ss'), 'banano', 'banano');

-- Initial rocket ships
INSERT INTO
  rockets (name)
VALUES
  ('doge');

INSERT INTO
  rockets (name)
VALUES
  ('illuminati');


