-- 
-- Rocket FUNCTIONS
-- 

-- returns the moderator and rocket_id of the rocket the user is a moderator of
CREATE OR replace FUNCTION get_rocket_mod(
  rocket_name TEXT,
  usr_pid BIGINT,
  allow_site_admin BOOLEAN DEFAULT FALSE
)								   
returns table (
	rocket_id INT,
	moderator_id BIGINT
)
AS
  $func$
DECLARE
  rkt_id INT;
  moderator_id BIGINT;
BEGIN

-- 
-- 	check if rocket exists
	SELECT id
	FROM   rockets
	WHERE  name = rocket_name
	INTO rkt_id;
	
	IF rkt_id IS NULL THEN
		RAISE EXCEPTION '{"msg": "Rocket % does not exist", "statusCode": 404}', rocket_name;
	END IF;
	
--  allowing site admins to change rocket
	IF allow_site_admin THEN
-- 		check if the user is a siteadmin
		SELECT public_id
		FROM users
		WHERE public_id = usr_pid
		AND site_admin = TRUE
		INTO moderator_id;
		
-- 		Is a site admin - can return now
		IF moderator_id IS NOT NULL THEN
			RETURN QUERY
			(SELECT rkt_id AS rocket_id, moderator_id);
			RETURN;
		END IF;
		
	END IF;
	
--  check if user is a moderator
	SELECT user_id
	FROM subscribers
	WHERE subscribers.rocket_id = rkt_id
	AND auth_level = 'mod'
  AND user_id = usr_pid
	INTO moderator_id;
	
	IF moderator_id IS NULL THEN
		RAISE EXCEPTION '{"msg": "Not a moderator of %", "statusCode": 403}', rocket_name;
	END IF;
	
	RETURN QUERY 
	(SELECT rkt_id AS rocket_id, moderator_id);
	
END $func$
LANGUAGE plpgsql;

-- 
-- fn for updating rocket
CREATE OR replace FUNCTION update_rocket(r_about TEXT,
										 r_restrict_post BOOLEAN,
                                         rocket_name TEXT,
                                         usr_pid BIGINT) 
returns SETOF rockets
AS
  $$
DECLARE
	qry TEXT;
	prev_update BOOLEAN;
BEGIN

-- 	Strictly moderators can update rocket
    -- Get the rocket id that the inviter is moderator if
    PERFORM * 
    FROM get_rocket_mod(rocket_name, usr_pid);
	
	qry := 'UPDATE rockets SET';
	
	IF r_about IS NULL AND r_restrict_post IS NULL THEN
		RAISE EXCEPTION '{"msg": "Must update the rocket about and/or rocket restrict post", "statusCode": 400}';
	END IF;
	
	IF r_about IS NOT NULL THEN
-- 		WATCH OUT: here is probably where an sql injection may happen
		qry := format('%s about = %L', qry, r_about);
		prev_update := TRUE;
	END IF;
	
	IF r_restrict_post IS NOT NULL THEN
		IF prev_update THEN
			qry := qry || ',';
		END IF;
		
-- 		can prob just concat since r_restrict_post must be a boolean
		qry := qry || ' restrict_posting = ' || r_restrict_post;
	END IF;
	
	qry := format('%s WHERE name = %L '||
				  'RETURNING *;'
				  , qry, rocket_name);
	
	
	RETURN QUERY EXECUTE qry;
	
END $$ LANGUAGE plpgsql;

-- 
-- creates an invitation
CREATE OR replace FUNCTION add_invitation(rocket_name TEXT,
                                          invitee_name TEXT,
                                          inviter_pid BIGINT) returns setof invitations
AS
  $$
  DECLARE
    invited_usr_id bigint;
    rkt_id         bigint;
  BEGIN

    -- Get the rocket id that the inviter is moderator if
    SELECT rocket_id 
    -- make is so site-admins can also invite moderators
    FROM get_rocket_mod(rocket_name, inviter_pid, TRUE)
    INTO rkt_id;

-- last check if user inviting exists
    SELECT public_id
    FROM   users
    WHERE  username = invitee_name
    INTO   invited_usr_id;

    IF invited_usr_id IS NULL THEN
      RAISE EXCEPTION '{"msg": "User % does not exist", "statusCode": 404}', invitee_name;
    END IF;
    
    RETURN query
    INSERT INTO invitations
                (
                            user_id,
                            rocket_id,
                            inviter
                )
                VALUES
                (
                            invited_usr_id,
                            rkt_id,
                            inviter_pid
                )
    returning   *;
  
  END $$ LANGUAGE plpgsql;

-- 
-- Delete user invitation to a rocket
CREATE OR replace FUNCTION delete_user_invite(rocket_name TEXT,
                                              usr_pid BIGINT) returns setof invitations
AS
  $$
  DECLARE
    invitation_found bigint;
    rkt_id           INT;
  BEGIN
    SELECT id
    FROM   rockets
    WHERE  name = rocket_name
    INTO   rkt_id;
    
    -- raise exception since rocket not found
    IF rkt_id IS NULL THEN
      RAISE EXCEPTION '{"msg": "Rocket % not found", "statusCode": 404}', rocket_name;
    END IF;
    -- Check if invitation exists
    SELECT *
    FROM   invitations
    WHERE  rocket_id = rkt_id
    AND    user_id = usr_pid
    INTO   invitation_found;
    
    -- raise exception since invitaiton not found
    IF invitation_found IS NULL THEN
      RAISE
    EXCEPTION
      '{"msg": "Invitation not found", "statusCode": 404}';
    END IF;
    
    -- rocket exists and invitation exists
    -- delete user invite
    RETURN query
    DELETE
    FROM      invitations
    WHERE     rocket_id = rkt_id
    AND       user_id = usr_pid
    returning *;
  
  END $$ LANGUAGE plpgsql; 

-- 
-- Accepts the moderator invite
-- makes deletes the user invitation and makes the user a moderator 
CREATE OR replace FUNCTION accept_invite(rocket_name TEXT,
                                         usr_pid BIGINT) returns setof subscribers
AS
  $$
  DECLARE
    rkt_id           bigint;
    invitation_found bigint;
  BEGIN


    SELECT rocket_id FROM delete_user_invite(rocket_name, usr_pid) INTO rkt_id;
    
    -- upsert user to rocket moderator
    RETURN query
    INSERT INTO subscribers
                (
                            rocket_id,
                            user_id,
                            auth_level
                )
                VALUES
                (
                            rkt_id,
                            usr_pid,
                            'mod'
                )
    ON conflict
                (
                            rocket_id,
                            user_id
                )
                DO
    UPDATE
    SET       auth_level = 'mod'
    returning * ;
  
  END $$ LANGUAGE plpgsql; 


-- 
-- fn for upserting the rocket settings
CREATE OR replace FUNCTION update_rkt_settings(rocket_name TEXT,
											   usr_pid BIGINT,
											   new_settings JSON
											  ) 
returns SETOF rocket_settings
AS
  $$
DECLARE
	rkt_id INT;
BEGIN

-- 	Strictly moderators can update rocket
    -- Get the rocket id that the inviter is moderator if
    SELECT rocket_id 
    FROM get_rocket_mod(rocket_name, usr_pid)
	INTO rkt_id;
	
	IF new_settings IS NULL THEN
		RAISE EXCEPTION '{"msg": "Rocket settings to insert cannot be null", "statusCode": 400}';
	END IF;
	
	RETURN QUERY
	INSERT INTO rocket_settings
		(
			id,
			settings
		)
		VALUES
		(
			rkt_id, 
			new_settings
		)
	ON CONFLICT 
		(
			id
		)
	DO
		UPDATE 
		SET settings = new_settings
	RETURNING *;
		
END $$ LANGUAGE plpgsql;