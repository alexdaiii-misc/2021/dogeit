-- 
-- Trigger to prevent insert into posts if restrict posting is true and user is not a moderator
CREATE OR REPLACE FUNCTION trg_rocket_does_not_restrict_posting()
  RETURNS trigger AS
$func$
DECLARE
  rocket_name TEXT;
  restricted BOOLEAN;
  mod_id BIGINT;
BEGIN

  SELECT name, restrict_posting
  FROM rockets
  WHERE id = NEW.rocket_id
  INTO rocket_name, restricted;

  IF restricted IS NOT FALSE THEN

    -- check if moderator
    PERFORM * FROM get_rocket_mod(rocket_name, NEW.user_id, TRUE);

  END IF;

  RETURN NEW;
END
$func$  
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS before_insert_posts_check_rocket_restrict_posting ON posts;

CREATE TRIGGER before_insert_posts_check_rocket_restrict_posting
BEFORE INSERT ON posts
FOR EACH ROW EXECUTE PROCEDURE trg_rocket_does_not_restrict_posting();

-- 
-- checks if rocket moderator using the rocket id. Returns the moderator id
-- NOTE: rocket know that the exists before calling fn
-- Can return null or the moderator_id
CREATE OR replace FUNCTION check_rocket_mod_with_id(
  rkt_id INT,
  usr_pid BIGINT,
  allow_site_admin BOOLEAN DEFAULT FALSE
)
RETURNS BIGINT
AS
  $func$
DECLARE
  moderator_id BIGINT;
BEGIN
	
--  allowing site admins to change rocket
	IF allow_site_admin IS TRUE THEN
-- 		check if the user is a siteadmin
		SELECT public_id
		FROM users
		WHERE public_id = usr_pid
		AND site_admin = TRUE
		INTO moderator_id;
		
-- 		Is a site admin - can return now
		IF moderator_id IS NOT NULL THEN
			RETURN moderator_id;
		END IF;
		
	END IF;
	
--  check if user is a moderator
	SELECT user_id
	FROM subscribers
	WHERE subscribers.rocket_id = rkt_id
	AND auth_level = 'mod'
	AND user_id = usr_pid
	INTO moderator_id;
	
	-- IF moderator_id IS NULL THEN
	-- 	RAISE EXCEPTION '{"msg": "Not a moderator", "statusCode": 403}';
	-- END IF;
	
	RETURN moderator_id;
	
END $func$
LANGUAGE plpgsql;

-- 
-- Nulls out the post - permenant - user must be moderator (OR siteadmin)
CREATE OR REPLACE FUNCTION nullify_post(
  post_id BIGINT,
  usr_pid BIGINT
)
returns table (
  post_id_astring TEXT,
  user_id BIGINT,
  crypto_prices_id TEXT,
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE,
  -- restrict_commenting BOOLEAN,
  title VARCHAR(255),
  body TEXT
)
AS $func$
DECLARE
  nulled_post posts%ROWTYPE;
  temp_bool BOOLEAN;
BEGIN

  SELECT *
  FROM posts
  WHERE id = post_id
  INTO nulled_post;

  IF nulled_post IS NULL THEN
    RAISE EXCEPTION '{"msg": "Record does not exist", "statusCode": 404}';
  END IF;
    
  temp_bool := usr_pid != nulled_post.user_id;
  
  -- check if they are the owner of the post
  IF (usr_pid <> nulled_post.user_id) OR nulled_post.user_id IS NULL THEN
  
  	RAISE NOTICE 'Not the owner of the post';
	
    -- check that the user is a moderator of the rocket
    -- check_rocket_mod_with_id(rkt_id INT, usr_pid BIGINT)
    -- NOTE: must be here bc postgres does not short circuit expressions
    IF (SELECT check_rocket_mod_with_id(nulled_post.rocket_id, usr_pid)) IS NULL THEN
      RAISE EXCEPTION '{"msg": "Not a moderator", "statusCode": 403}';
    END IF;
	
	RAISE NOTICE 'Is a moderator. Continuing.';

  END IF;

  -- Update the post to be deleted
  UPDATE posts
  SET user_id = NULL,
  title = '',
  body = ''
  WHERE id = post_id
  RETURNING *
  INTO nulled_post;
  
  -- casted the bigints into strings bc prisma can't handle bigints
  RETURN QUERY
  (SELECT 
    post_id::TEXT AS post_id_astring,
    nulled_post.user_id AS user_id,
    nulled_post.crypto_prices_id::TEXT AS crypto_prices_id,
    nulled_post.created_at AS created_at,
    nulled_post.updated_at AS updated_at,
    -- nulled_post.restrict_commenting AS restrict_commenting,
    nulled_post.title AS title,
    nulled_post.body AS body
  );

END $func$
LANGUAGE plpgsql;