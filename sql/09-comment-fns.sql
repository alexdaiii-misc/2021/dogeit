-- 
-- Trigger to prevent insert into comments if restrict posting is true and user is not a mod
CREATE OR REPLACE FUNCTION trg_rocket_does_not_restrict_commenting()
  RETURNS trigger AS
$func$
DECLARE
  rkt_id INTEGER;
  rocket_name TEXT;
  restricted BOOLEAN;
  mod_id BIGINT;
BEGIN

  SELECT rocket_id
  FROM posts
  WHERE id = NEW.post_id
  INTO rkt_id;

  SELECT name, restrict_posting
  FROM rockets
  WHERE id = rkt_id
  INTO rocket_name, restricted;

  IF restricted IS NOT FALSE THEN

    -- check if moderator
    PERFORM * FROM get_rocket_mod(rocket_name, NEW.user_id, TRUE);

  END IF;

  RETURN NEW;
END
$func$  
LANGUAGE plpgsql;


DROP TRIGGER IF EXISTS before_insert_comments_check_rocket_restrict_commenting ON comments;

CREATE TRIGGER before_insert_comments_check_rocket_restrict_commenting
BEFORE INSERT ON comments
FOR EACH ROW EXECUTE PROCEDURE trg_rocket_does_not_restrict_commenting();


-- 
-- Trigger to prevent insert into comments if parent id does not match comment parent id
CREATE OR REPLACE FUNCTION trg_check_parent_child_same_post_id()
  RETURNS trigger AS
$func$
DECLARE
  parent_post_id BIGINT;
BEGIN

  -- is a root comment
  IF NEW.parent_id IS NULL THEN
    RETURN NEW;
  END IF;

  SELECT post_id
  FROM comments
  WHERE id = NEW.parent_id 
  INTO parent_post_id;

  IF parent_post_id IS NULL THEN
    RAISE EXCEPTION 'Parent post does not exist';
  END IF;
  
  IF (NEW.post_id != parent_post_id) THEN
    RAISE EXCEPTION 'Child post must be the same as parent post';
  END IF;

  RETURN NEW;
END
$func$  
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS before_insert_comments_check_parent_child_same_post_id ON comments;

CREATE TRIGGER before_insert_comments_check_parent_child_same_post_id
BEFORE INSERT ON comments
FOR EACH ROW EXECUTE PROCEDURE trg_check_parent_child_same_post_id();

-- 
-- Nulls out the comment - permenant - user must be moderator (OR siteadmin)
CREATE OR REPLACE FUNCTION nullify_comment(
  comment_id BIGINT,
  usr_pid BIGINT
)
returns table (
  comment_id_astring TEXT,
  post_id TEXT,
  -- will be nulled
  user_id BIGINT,
  crypto_prices_id TEXT,
  parent_id TEXT,
  root_id TEXT,
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE,
  body TEXT
)
AS $func$
DECLARE
  nulled_comment comments%ROWTYPE;
  rkt_id INTEGER;
BEGIN

  SELECT *
  FROM comments
  WHERE id = comment_id
  INTO nulled_comment;

  IF nulled_comment IS NULL THEN
    RAISE EXCEPTION '{"msg": "Comment does not exist", "statusCode": 404}';
  END IF;
    
  -- check if they are the owner of the post
  IF (usr_pid <> nulled_comment.user_id) OR nulled_comment.user_id IS NULL THEN
  
  	RAISE NOTICE 'Not the owner of the comment';

    SELECT rocket_id
    FROM posts
    WHERE id = nulled_comment.post_id
    INTO rkt_id;

    -- check that the user is a moderator of the rocket
    -- check_rocket_mod_with_id(rkt_id INT, usr_pid BIGINT)
    -- NOTE: must be here bc postgres does not short circuit expressions
    IF (SELECT check_rocket_mod_with_id(rkt_id, usr_pid)) IS NULL THEN
      RAISE EXCEPTION '{"msg": "Not a moderator", "statusCode": 403}';
    END IF;
	
	  RAISE NOTICE 'Is a moderator. Continuing.';

  END IF;

  -- Update the post to be deleted
  UPDATE comments
  SET user_id = NULL,
  body = ''
  WHERE id = comment_id
  RETURNING *
  INTO nulled_comment;
  
  -- casted the bigints into strings bc prisma can't handle bigints
  RETURN QUERY
  (SELECT 
    comment_id::TEXT AS comment_id_astring,
    nulled_comment.post_id::TEXT AS post_id,
    nulled_comment.user_id AS user_id,
    nulled_comment.crypto_prices_id::TEXT AS crypto_prices_id,
    nulled_comment.parent_id::TEXT AS parent_id,
    nulled_comment.root_id::TEXT AS root_id,
    nulled_comment.created_at AS created_at,
    nulled_comment.updated_at AS updated_at,
    -- nulled_post.restrict_commenting AS restrict_commenting,
    nulled_comment.body AS body
  );

END $func$
LANGUAGE plpgsql;

-- 
-- Deletes a comment
-- Server perfroms check
-- returns prismasafe comment
CREATE OR REPLACE FUNCTION admin_delete_comment(
  comment_id BIGINT
)
returns table (
  comment_id_astring TEXT,
  post_id TEXT,
  -- will be nulled
  user_id BIGINT,
  crypto_prices_id TEXT,
  parent_id TEXT,
  root_id TEXT,
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE,
  body TEXT
)
AS $func$
DECLARE
  nulled_comment comments%ROWTYPE;
BEGIN

  SELECT *
  FROM comments
  WHERE id = comment_id
  INTO nulled_comment;

  IF nulled_comment IS NULL THEN
    RAISE EXCEPTION '{"msg": "Comment does not exist", "statusCode": 404}';
  END IF;

  DELETE FROM comments
  WHERE id = comment_id
  RETURNING *
  INTO nulled_comment;
  
  -- casted the bigints into strings bc prisma can't handle bigints
  RETURN QUERY
  (SELECT 
    comment_id::TEXT AS comment_id_astring,
    nulled_comment.post_id::TEXT AS post_id,
    nulled_comment.user_id AS user_id,
    nulled_comment.crypto_prices_id::TEXT AS crypto_prices_id,
    nulled_comment.parent_id::TEXT AS parent_id,
    nulled_comment.root_id::TEXT AS root_id,
    nulled_comment.created_at AS created_at,
    nulled_comment.updated_at AS updated_at,
    nulled_comment.body AS body
  );

END $func$
LANGUAGE plpgsql;

-- 
-- Gets comments paginated
-- returns prismasafe comment
CREATE OR REPLACE FUNCTION get_paginated_comments(
  post_selected_id BIGINT,
  root_id_cursor BIGINT,
  count_limit INTEGER,
  is_refresh BOOLEAN = False
)
returns table(
	comment_id_astring TEXT,
	-- converted to string for prisma safe
	post_id TEXT,
	username CITEXT,
	-- converted to string for prisma safe
	crypto_prices_id TEXT,
	-- converted to string for prisma safe
	parent_id TEXT,
	-- converted to string for prisma safe
	root_id TEXT,
	created_at TIMESTAMP WITH TIME ZONE,
	updated_at TIMESTAMP WITH TIME ZONE,
	body TEXT
)
AS $func$
DECLARE
  qry TEXT;
BEGIN

	qry := 'SELECT 		
		comments.id::TEXT AS comment_id_astring,
		post_id::TEXT,
		username,
		crypto_prices_id::TEXT,
		parent_id::TEXT,
		root_id::TEXT,
		comments.created_at,
		comments.updated_at,
		body
	FROM comments
	JOIN users ON
		comments.user_id = users.public_id
	WHERE root_id IN (
		SELECT
			root_id 
		FROM
			comments 
		WHERE
			post_id = ' || post_selected_id || '
		AND root_id ';
	
	IF is_refresh THEN
		qry := qry || '>= ' || root_id_cursor || '
		';
	ELSE
		qry := qry || '< ' || root_id_cursor || '
		';
	END IF;
	
	qry := qry || 'GROUP BY
			root_id
		ORDER BY
			root_id DESC
		' || 'LIMIT ' || count_limit || '
	)
	ORDER BY
		root_id DESC,
		comments.id DESC';	
	
	-- casted the bigints into strings bc prisma can't handle bigints
	RETURN QUERY EXECUTE qry;

END $func$
LANGUAGE plpgsql;

