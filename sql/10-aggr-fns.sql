-- 
-- Get estimated count of rows returned by query
CREATE OR REPLACE FUNCTION count_estimate(query text) RETURNS integer AS $$
DECLARE
  rec   record;
  rows  integer;
BEGIN
  FOR rec IN EXECUTE 'EXPLAIN ' || query LOOP
    rows := substring(rec."QUERY PLAN" FROM ' rows=([[:digit:]]+)');
    EXIT WHEN rows IS NOT NULL;
  END LOOP;
  RETURN rows;
END;
$$ LANGUAGE plpgsql VOLATILE STRICT;


-- 
-- Counts the estimated number of subscribers in a rocket
CREATE OR REPLACE FUNCTION subscribers_count(
  rocket_name TEXT
)
returns INT
AS $func$
DECLARE
  rkt_id INTEGER;
  sub_count INTEGER;
  qry TEXT;
BEGIN

  SELECT id
  FROM rockets
  WHERE name = rocket_name
  INTO rkt_id;

  IF rkt_id IS NULL THEN
    RAISE EXCEPTION '{"msg": "Rocket % does not exist", "statusCode": 404}', rocket_name;
  END IF;
  
   RAISE NOTICE 'VALUE: %', rkt_id;
   
   qry := 'SELECT * FROM subscribers WHERE rocket_id = ' || rkt_id;
   
   RAISE NOTICE 'VALUE: %', qry;
   
   SELECT *
   FROM count_estimate(qry)
   INTO sub_count;

  RETURN sub_count;

END $func$
LANGUAGE plpgsql;


-- 
-- materialized view to store the avaliable reactions
CREATE materialized VIEW avaliable_reactions
AS SELECT e.enumlabel AS enum_value
   FROM   pg_type t
          join pg_enum e
            ON t.oid = e.enumtypid
          join pg_catalog.pg_namespace n
            ON n.oid = t.typnamespace
   WHERE  t.typname = 'reactions';


-- 
-- Counts the number of reaction to a post or comment
CREATE OR replace FUNCTION reaction_count( post_or_cmt_id BIGINT,
                                          tgt TEXT ) 
returns TABLE( reaction text,
               count_estimate INTEGER )
AS
  $func$
  DECLARE
    qry text;
    react RECORD;
  BEGIN
    IF lower(tgt) = 'post_id' THEN
      IF
          (
          SELECT id
          FROM   posts
          WHERE  id = post_or_cmt_id) IS NULL THEN
        RAISE
      EXCEPTION
        '{"msg": "Post does not exist", "statusCode": 404}';
      END IF;
    ELSIF lower(tgt) = 'comment_id' THEN
      IF
          (
          SELECT id
          FROM   posts
          WHERE  id = post_or_cmt_id) IS NULL THEN
        RAISE
      EXCEPTION
        '{"msg": "Post does not exist", "statusCode": 404}';
      END IF;
    ELSE
      RAISE
    EXCEPTION
      '{"msg": "Tgt cannot be %. Tgt can only be post_id or comment_id", "statusCode": 500}', tgt;
    END IF;
    qry := '';
    FOR react IN
    SELECT     *
    FROM   avaliable_reactions LOOP qry := qry
                  || 'SELECT '''
                  || react.enum_value
                  || ''' AS reaction, * FROM count_estimate( ''SELECT * FROM reaction WHERE '
                  || tgt
                  || ' = '
                  || post_or_cmt_id
                  || ' AND reaction = '''''
                  || react.enum_value
                  || ''''''') UNION ';
  
  END LOOP;
  
  SELECT trim(trailing 'UNION   ' FROM qry)
  INTO   qry;
  
  RETURN query EXECUTE qry;
END $func$ LANGUAGE plpgsql;