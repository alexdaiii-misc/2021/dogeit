CREATE EXTENSION IF NOT EXISTS "pgcrypto";

-- From Nathan Fritz (andyet.com); turbo (github.com/turbo)
-- can't query pg_type because type might exist in other schemas
-- no IF NOT EXISTS for CREATE DOMAIN, need to catch exception
DO $$ BEGIN
  CREATE DOMAIN SHORTKEY as varchar(11);
EXCEPTION
  WHEN duplicate_object THEN null;
END $$;

CREATE OR REPLACE FUNCTION generate_shortkey(tablename text, col text DEFAULT 'public_id')
RETURNS SHORTKEY
LANGUAGE plpgsql
AS
$$
DECLARE
  gkey TEXT;
  key SHORTKEY;
  found TEXT;
  idx INTEGER;
BEGIN

  idx := 0;

  LOOP
  
    -- Generator by Nathan Fritz (andyet.com); turbo (github.com/turbo)
    -- 8 bytes gives a collision p = .5 after 5.1 x 10^9 values
    gkey := encode(extensions.gen_random_bytes(8), 'base64');
    gkey := replace(gkey, '/', '_');  -- url safe replacement
    gkey := replace(gkey, '+', '-');  -- url safe replacement
    key := rtrim(gkey, '=');          -- cut off padding

    EXECUTE format('SELECT %2$I
      FROM %1$I
      WHERE %2$s = %3$L
    ', tablename, col, key) INTO found;

    IF found IS NULL THEN
      RETURN key;
    END IF;
	
	idx := idx + 1;
	
  -- Too many collisions, do not keep generating
	if idx = 5 THEN
		RETURN null;
	END IF;

  END LOOP;

  RETURN key;

END
$$;