delete from subscribers where rocket_id in (
	select id from rockets where name = 'doge'
);

select * from invitations;

insert into invitations (user_id, rocket_id, inviter)
values ((
	select public_id
	from users
	where username = 'shiba'
),(
	select id
	from rockets
	where name = 'doge'
), (
	select public_id
	from users
	where username = 'doge'
)) ON CONFLICT DO NOTHING;