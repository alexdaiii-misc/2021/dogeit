create table tree(
    id serial primary key,
    letter char,
    path ltree
);
create index tree_path_idx on tree using gist (path);

insert into tree (letter, path) values ('A', 'A');
insert into tree (letter, path) values ('B', 'A.B');
insert into tree (letter, path) values ('C', 'A.C');
insert into tree (letter, path) values ('D', 'A.C.D');
insert into tree (letter, path) values ('E', 'A.C.E');
insert into tree (letter, path) values ('F', 'A.C.F');
insert into tree (letter, path) values ('G', 'A.B.G');

-- recursively count the total number of nodes under any given parent
-- @>, the “ancestor” operator. It tests whether one path is an ancestor of another.
-- select count(*) from tree where PARENT-PATH @> path;
select count(*) from tree where 'A' @> path;

-- Trigger to modify path before each insert
CREATE OR REPLACE FUNCTION before_insert_on_tree()

RETURNS TRIGGER LANGUAGE plpgsql AS $$
BEGIN
    new.path := new.path ||  new.id::text;
    return new;
END $$;

DROP TRIGGER IF EXISTS before_insert_on_tree ON tree;
CREATE TRIGGER before_insert_on_tree
BEFORE INSERT ON tree
FOR EACH ROW EXECUTE PROCEDURE before_insert_on_tree();

-- root node
insert into tree (letter, path) values ('A', '');
-- child
insert into tree (letter, path) values ('B', (SELECT path FROM fragment WHERE letter='A'));
insert into tree (letter, path) values ('C', (SELECT path FROM fragment WHERE letter='A'));
insert into tree (letter, path) values ('D', (SELECT path FROM fragment WHERE letter='C'));
insert into tree (letter, path) values ('E', (SELECT path FROM fragment WHERE letter='C'));
insert into tree (letter, path) values ('F', (SELECT path FROM fragment WHERE letter='C'));
insert into tree (letter, path) values ('G', (SELECT path FROM fragment WHERE letter='B'));