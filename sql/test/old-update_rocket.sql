-- 
-- fn for updating rocket
CREATE OR replace FUNCTION update_rocket(r_about TEXT,
										 r_restrict_post BOOLEAN,
                                         rocket_name TEXT,
                                         usr_pid BIGINT) 
returns SETOF rockets
AS
  $$
DECLARE
	qry TEXT;
	prev_update BOOLEAN;
BEGIN

-- 	Strictly moderators can update rocket
    -- Get the rocket id that the inviter is moderator if
    PERFORM * 
    FROM get_rocket_mod(rocket_name, usr_pid);
	
	qry := 'UPDATE rockets SET';
	
	IF r_about IS NULL AND r_restrict_post IS NULL THEN
		RAISE EXCEPTION '{"msg": "Must update the rocket about and/or rocket restrict post", "statusCode": 400}';
	END IF;
	
	IF r_about IS NOT NULL THEN
-- 		WATCH OUT: here is probably where an sql injection may happen
		qry := format('%s about = %L', qry, r_about);
		prev_update := TRUE;
	END IF;
	
	IF r_restrict_post IS NOT NULL THEN
		IF prev_update THEN
			qry := qry || ',';
		END IF;
		
-- 		can prob just concat since r_restrict_post must be a boolean
		qry := qry || ' restrict_posting = ' || r_restrict_post;
	END IF;
	
	qry := format('%s WHERE name = %L '||
				  'RETURNING *;'
				  , qry, rocket_name);
	
	
	RETURN QUERY EXECUTE qry;
	
END $$ LANGUAGE plpgsql;

-- only update about
SELECT * FROM update_rocket('SQL INJECTION'' UNION SELECT * FROM USERS; --', NULL, 'dogelore', 
						   (SELECT public_id
						   FROM users
						   WHERE username = 'shiba')
						   )

-- only update restrict posting
SELECT * FROM update_rocket(NULL, TRUE, 'dogelore', 
						   (SELECT public_id
						   FROM users
						   WHERE username = 'shiba')
						   )

-- update both
SELECT * FROM update_rocket('SQL INJECTION'' UNION SELECT * FROM USERS; --', TRUE, 'dogelore', 
						   (SELECT public_id
						   FROM users
						   WHERE username = 'shiba')
						   )


-- update none - should fail
SELECT * FROM update_rocket(NONE, TRUE, 'dogelore', 
						   (SELECT public_id
						   FROM users
						   WHERE username = 'shiba')
						   )