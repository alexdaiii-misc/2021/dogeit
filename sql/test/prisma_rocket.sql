
-- This is the prisma query for getRocketFromComment()
explain SELECT 
"public"."rocket"."id", "public"."rocket"."name", 
"public"."rocket"."created_at", "public"."rocket"."updated_at", 
"public"."rocket"."about", "public"."rocket"."rules", 
"public"."rocket"."allow_discovery", "public"."rocket"."restrict_posting" 
FROM 
"public"."rocket" 
WHERE 
("public"."rocket"."id") IN 
	(SELECT "t0"."id" 
	 FROM "public"."rocket" AS "t0" 
	 INNER JOIN 
	 "public"."posts" AS "j0" ON ("j0"."rocket_id") = ("t0"."id") 
	 WHERE (("j0"."id") IN 
			(SELECT "t1"."id" 
			 FROM "public"."posts" AS "t1" 
			 INNER JOIN 
			 "public"."comments" AS "j1" ON ("j1"."post_id") = ("t1"."id") 
			 WHERE 
			 ("j1"."id" = 2597667639106995210 AND "t1"."id" IS NOT NULL))
			AND "t0"."id" IS NOT NULL)) LIMIT 1 OFFSET 0

-- My query:
explain
select 
"public"."rocket"."id", "public"."rocket"."name", 
"public"."rocket"."created_at", "public"."rocket"."updated_at", 
"public"."rocket"."about", "public"."rocket"."rules", 
"public"."rocket"."allow_discovery", "public"."rocket"."restrict_posting" 
from
rocket
inner join
posts
on posts.rocket_id = rocket.id
inner join
comments
on comments.post_id = posts.id
where
comments.id = 2597667639106995210
limit 1