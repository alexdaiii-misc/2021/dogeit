-- 
-- Insert a new post with checks
CREATE OR REPLACE FUNCTION insert_new_post
(
	rocket_name TEXT,
	new_title TEXT,
	new_body TEXT,
	latest_crypto_id BIGINT,
	usr_pid BIGINT
)
	RETURNS SETOF posts AS
$func$
DECLARE
	restricted BOOLEAN;
	rkt_id INTEGER;
BEGIN

	SELECT 
	id, restrict_posting
	FROM   rockets
	WHERE  name = rocket_name
	INTO rkt_id, restricted;
  
	IF restricted IS NOT FALSE THEN
	-- check if moderator OR siteadmin
		PERFORM rocket_id FROM get_rocket_mod(rocket_name, usr_pid, TRUE);
	END IF;

	RETURN QUERY
	INSERT INTO posts
	(
		crypto_prices_id,
		rocket_id,
		user_id,
		title,
		body
	)
	VALUES
	(
		latest_crypto_id,
		rkt_id,
		usr_pid,
		new_title,
		new_body
	)
	RETURNING *;
  
END
$func$  LANGUAGE plpgsql;
