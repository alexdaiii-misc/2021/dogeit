export type APIExternGeckoCoinRes = {
  id: string;
  coin_id: string;
  name: string;
  market_cap_rank: number;
  current_price: string;
  total_volume: string;
  high_24h: string;
  low_24h: string;
  price_change_24h_in_currency: string;
  price_change_percentage_24h: string;
  circulating_supply: string;
};
