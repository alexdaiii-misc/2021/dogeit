export interface geckoResponse extends AxiosResponse<any> {
  data: geckoResponseData;
}

export interface geckoResponseData {
  id: string;
  symbol: string;
  name: string;
  asset_platform_id: any;
  platforms: Platforms;
  block_time_in_minutes: number;
  hashing_algorithm: string;
  categories: string[];
  public_notice: any;
  additional_notices: any[];
  description: Description;
  links: Links;
  image: Image;
  country_origin: string;
  genesis_date: string;
  sentiment_votes_up_percentage: number;
  sentiment_votes_down_percentage: number;
  market_cap_rank: number;
  coingecko_rank: number;
  coingecko_score: number;
  developer_score: number;
  community_score: number;
  liquidity_score: number;
  public_interest_score: number;
  market_data: MarketData;
  public_interest_stats: PublicInterestStats;
  status_updates: any[];
  last_updated: string;
}

export interface Platforms {
  "": string;
  "binance-smart-chain": string;
}

export interface Description {
  en: string;
}

export interface Links {
  homepage: string[];
  blockchain_site: string[];
  official_forum_url: string[];
  chat_url: string[];
  announcement_url: string[];
  twitter_screen_name: string;
  facebook_username: string;
  bitcointalk_thread_identifier: number;
  telegram_channel_identifier: string;
  subreddit_url: string;
  repos_url: ReposUrl;
}

export interface ReposUrl {
  github: string[];
  bitbucket: any[];
}

export interface Image {
  thumb: string;
  small: string;
  large: string;
}

export interface MarketData {
  current_price: Prices;
  total_value_locked: any;
  mcap_to_tvl_ratio: any;
  fdv_to_tvl_ratio: any;
  roi: any;
  ath: Prices;
  ath_change_percentage: Prices;
  ath_date: Prices;
  atl: Prices;
  atl_change_percentage: Prices;
  atl_date: Prices;
  market_cap: Prices;
  market_cap_rank: number;
  fully_diluted_valuation: FullyDilutedValuation;
  total_volume: Prices;
  high_24h: Prices;
  low_24h: Prices;
  price_change_24h: number;
  price_change_percentage_24h: number;
  price_change_percentage_7d: number;
  price_change_percentage_14d: number;
  price_change_percentage_30d: number;
  price_change_percentage_60d: number;
  price_change_percentage_200d: number;
  price_change_percentage_1y: number;
  market_cap_change_24h: number;
  market_cap_change_percentage_24h: number;
  price_change_24h_in_currency: Prices;
  price_change_percentage_1h_in_currency: Prices;
  price_change_percentage_24h_in_currency: Prices;
  price_change_percentage_7d_in_currency: Prices;
  price_change_percentage_14d_in_currency: Prices;
  price_change_percentage_30d_in_currency: Prices;
  price_change_percentage_60d_in_currency: Prices;
  price_change_percentage_200d_in_currency: Prices;
  price_change_percentage_1y_in_currency: Prices;
  market_cap_change_24h_in_currency: Prices;
  market_cap_change_percentage_24h_in_currency: Prices;
  total_supply: any;
  max_supply: any;
  circulating_supply: number;
  last_updated: string;
}

export interface Prices {
  aed: number;
  ars: number;
  aud: number;
  bch: number;
  bdt: number;
  bhd: number;
  bmd: number;
  bnb: number;
  brl: number;
  btc: number;
  cad: number;
  chf: number;
  clp: number;
  cny: number;
  czk: number;
  dkk: number;
  dot: number;
  eos: number;
  eth: number;
  eur: number;
  gbp: number;
  hkd: number;
  huf: number;
  idr: number;
  ils: number;
  inr: number;
  jpy: number;
  krw: number;
  kwd: number;
  lkr: number;
  ltc: number;
  mmk: number;
  mxn: number;
  myr: number;
  ngn: number;
  nok: number;
  nzd: number;
  php: number;
  pkr: number;
  pln: number;
  rub: number;
  sar: number;
  sek: number;
  sgd: number;
  thb: number;
  try: number;
  twd: number;
  uah: number;
  usd: number;
  vef: number;
  vnd: number;
  xag: number;
  xau: number;
  xdr: number;
  xlm: number;
  xrp: number;
  yfi: number;
  zar: number;
  bits: number;
  link: number;
  sats: number;
}

export interface FullyDilutedValuation {}

export interface PublicInterestStats {
  alexa_rank: number;
  bing_matches: any;
}
