import { returnedCommentDetails } from "./../../lib/queries/qryReturnVals";
import { Prisma } from "@prisma/client";

const commentSelectedDetails = Prisma.validator<Prisma.commentsArgs>()({
  select: returnedCommentDetails,
});

export type commentSelected = Prisma.commentsGetPayload<
  typeof returnedCommentDetails
>;

export type prismaSafeComment = {
  comment_id_astring?: string;
  id?: string;
  post_id: string;
  user_id: string | null;
  parent_id: string | null;
  root_id: string;
  crypto_prices_id: string;
  created_at: string;
  updated_at: string;
  body: string;
};
