import { returnedPostDetails } from "./../../../lib/queries/qryReturnVals";
import { Prisma } from "@prisma/client";

export type PostCommentsPaginated = {
  id: bigint;
  post_id: bigint;
  username: string;
  parent_id: bigint | null;
  root_id: bigint;
  crypto_prices_id: bigint;
  created_at: string;
  updated_at: string;
  body: string;
};

export type PostCommentsPaginatedSearlized = {
  id: string;
  post_id: string;
  username: string;
  parent_id: string | null;
  root_id: string;
  crypto_prices_id: string;
  created_at: string;
  updated_at: string;
  body: string;
};

export type prismaSafePaginatedComment = {
  comment_id_astring?: string;
  id?: string;
  post_id: string;
  username: string;
  parent_id: string | null;
  root_id: string;
  crypto_prices_id: string;
  created_at: string;
  updated_at: string;
  body: string;
};
