import { returnedPostDetails } from "../../../lib/queries/qryReturnVals";
import { Prisma } from "@prisma/client";

export type RocketPosts = Prisma.postsGetPayload<{
  select: {
    users: { select: { username: true } };
    id;
    crypto_prices_id;
    user_id;
    created_at;
    updated_at;
    title;
    body;
  };
}>;
