import { returnedPostDetails } from "../../lib/queries/qryReturnVals";
import { Prisma } from "@prisma/client";

export type prismaSafePost = {
  post_id_astring?: string;
  id?: string;
  user_id: string | null;
  crypto_prices_id: string;
  created_at: string;
  updated_at: string;
  title: string;
  body: string;
};

const postsSelectedDetailes = Prisma.validator<Prisma.postsArgs>()({
  select: returnedPostDetails,
});

export type postSelected = Prisma.postsGetPayload<typeof postsSelectedDetailes>;
