import { comments, crypto_prices, rockets } from "@prisma/client";
import { Session } from "next-auth";
import { NextApiRequest } from "next";
import { getRocketByRocketName } from "../lib/database/RocketQueries";
import { UnwrapPromise } from ".prisma/client";
import { getComment, getPost } from "../lib/database/PostQueries";
import { getUserFromSessionToken } from "../lib/queries/userSession";

export type rocketReq = rockets;

export type postReq = Exclude<UnwrapPromise<ReturnType<typeof getPost>>, null>;

export type userReq = Omit<
  Exclude<UnwrapPromise<ReturnType<typeof getUserFromSessionToken>>, null>,
  "sessions"
>;

export type commentReq = Exclude<
  UnwrapPromise<ReturnType<typeof getComment>>,
  null
>;

interface NextApiRequestWithParams extends NextApiRequest {
  params: any;
}

// Base
interface RequestWithUser extends NextApiRequestWithParams {
  user: userReq;
  signInMethod: "jwt" | "session";
}

interface RequestWithSession extends NextApiRequestWithParams {
  user: userReq;
  session: Session | null;
  signInMethod: "jwt" | "session";
}

interface RequestWithRocket extends NextApiRequestWithParams {
  rocket: rocketReq;
}

interface RequestWithPost extends NextApiRequestWithParams {
  post: postReq;
}

interface RequestWithCrypto extends NextApiRequestWithParams {
  crypto_price: crypto_prices;
}

interface RequestWithComment extends NextApiRequestWithParams {
  comment: commentReq;
}

// Base with maybes
interface RequestWithMaybeRocket extends NextApiRequestWithParams {
  rocket?: rocketReq;
}

interface RequestWithMaybePost extends NextApiRequestWithParams {
  post?: postReq;
}
