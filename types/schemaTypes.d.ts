import { inviteUserSchema } from "../schemas/rocketSchemas";
import { InferType } from "yup";

export type inviteUserType = InferType<typeof inviteUserSchema>;
