import { InferType } from "yup";
import { newCommentSchema } from "../../schemas/commentsSchema";

export type newCommentType = InferType<typeof newCommentSchema>;
