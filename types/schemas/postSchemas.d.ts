import { deletePostSchema, newPostSchema } from "../../schemas/postSchemas";
import { InferType } from "yup";
import { POST_DELETION_ACTIONS } from "../../lib/server-const/schemaConsts";

export type newPostSchemaType = InferType<typeof newPostSchema>;

export type postDeletionActions = typeof POST_DELETION_ACTIONS[number];

export type deletePostSchemaType = {
  action: postDeletionActions;
};
