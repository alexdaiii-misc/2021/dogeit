import { InferType } from "yup";
import {
  reactToRecordSchema,
  getReactionSchema,
  getMultiReactionsSchema,
} from "./../../schemas/reactSchema";

export type reactToRecordSchema = InferType<typeof reactToRecordSchema>;

export type getReactionSchemaType = InferType<typeof getReactionSchema>;

export type getMultiReactionsSchemaType = InferType<
  typeof getMultiReactionsSchema
>;
