import { InferType } from "yup";
import {
  newRocketSchema,
  updateRocketSchema,
  updateRocketSettingsSchema,
} from "../../schemas/rocketSchemas";

type newRocketData = InferType<typeof newRocketSchema>;

type rocketUpdateInfo = InferType<typeof updateRocketSchema>;

type rocketSettings = InferType<typeof updateRocketSettingsSchema>;
