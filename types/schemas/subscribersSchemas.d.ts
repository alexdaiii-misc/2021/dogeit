import { SUBSCRIBER_QUERY } from "../../lib/server-const/schemaConsts";
import { subscribersSchema } from "./../../schemas/subscribersSchemas";
import { InferType } from "yup";

export type subscribersQueryType = {
  subscriber: typeof SUBSCRIBER_QUERY[number];
};
